// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Git hosting service traits
//!
//! The traits in this module are meant to help keep service-specific knowledge away from the
//! workflow implementation.
//!
//! These traits might be split into a separate library in the future.

use crates::chrono::{DateTime, Utc};
use crates::git_workarea::{CommitId, GitContext, Identity};

use std::fmt::{self, Debug};
use std::sync::Arc;

#[derive(Debug, Clone)]
/// A commit status created by a `Commit` or `MergeRequest`.
pub struct PendingCommitStatus<'a> {
    /// The commit the status applies to.
    pub commit: &'a Commit,
    /// The state of the commit status.
    pub state: CommitStatusState,
    /// The refname the commit status is intended for.
    pub refname: Option<&'a str>,
    /// The name of the status check.
    pub name: &'a str,
    /// A description for the status.
    pub description: &'a str,
}

#[derive(Debug, Clone)]
/// A commit hosted on the service provider.
pub struct Commit {
    /// The repository where the commit lives.
    pub repo: Repo,
    /// The refname for the commit (if available).
    pub refname: Option<String>,
    /// The object id of the commit.
    pub id: CommitId,
}

impl Commit {
    /// Create a commit status for the commit.
    pub fn create_commit_status<'a>(&'a self, state: CommitStatusState, name: &'a str,
                                    description: &'a str)
                                    -> PendingCommitStatus<'a> {
        PendingCommitStatus {
            commit: self,
            state: state,
            refname: self.refname.as_ref().map(String::as_str),
            name: name,
            description: description,
        }
    }
}

#[derive(Debug, Clone)]
/// A repository hosted on the service.
pub struct Repo {
    /// The name of the project.
    pub name: String,

    /// The URL which should be used to fetch from the repository.
    ///
    /// Whether this uses HTTPS or SSH is dependent on the service, but it should not require
    /// interaction in order to use (whether through an SSH key or administrator privileges).
    pub url: String,

    /// The ID of the repository.
    pub id: u64,

    /// The repository which this project was forked from.
    pub forked_from: Option<Box<Repo>>,
}

impl Repo {
    /// The root of the fork tree for the repository.
    pub fn fork_root(&self) -> &Self {
        self.forked_from
            .as_ref()
            .map_or(self, |parent| parent.fork_root())
    }
}

#[derive(Debug, Clone)]
/// An issue on the service.
pub struct Issue {
    /// The source repository for the issue.
    pub repo: Repo,
    /// The internal ID of the issue.
    ///
    /// Service-specific.
    pub id: u64,
    /// The URL of the issue.
    pub url: String,
    /// The description for the issue.
    pub description: String,
    /// The labels for the issue.
    pub labels: Vec<String>,
    /// The milestone for the issue.
    pub milestone: Option<String>,
    /// The username of the assignee.
    pub assignee: Option<String>,
    /// A string which may be used in a comment to refer to the issue.
    pub reference: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// The status of the hooks having been run on a merge request.
pub enum CheckStatus {
    /// The checks passed.
    Pass,
    /// The checks failed.
    Fail,
    /// The checks have not been run.
    Unchecked,
}

impl CheckStatus {
    /// Whether the checks have been run or not.
    pub fn is_checked(&self) -> bool {
        match *self {
            CheckStatus::Pass | CheckStatus::Fail => true,
            CheckStatus::Unchecked => false,
        }
    }

    /// Whether the checks need to be run or not.
    pub fn is_ok(&self) -> bool {
        match *self {
            CheckStatus::Pass => true,
            CheckStatus::Fail | CheckStatus::Unchecked => false,
        }
    }
}

#[derive(Debug, Clone)]
/// A merge request on the service.
pub struct MergeRequest {
    /// The source repository for the merge request.
    pub source_repo: Repo,
    /// The name of the branch requested for merging.
    pub source_branch: String,
    /// The repository the merge request will be merged into.
    pub target_repo: Repo,
    /// The target branch for the request.
    pub target_branch: String,
    /// The internal ID of the merge request.
    ///
    /// Service-specific.
    pub id: u64,
    /// The URL of the merge request.
    pub url: String,
    /// Whether the merge request is a "work-in-progress" or not.
    pub work_in_progress: bool,
    /// The description for the merge request.
    pub description: String,
    /// The previous commit of the merge request (if available).
    ///
    /// This is particularly important for the stage action. Not so important otherwise.
    pub old_commit: Option<Commit>,
    /// The commit which has been requested for merging.
    pub commit: Commit,
    /// The author of the merge request.
    pub author: User,
    /// A string which may be used in a comment to refer to the merge request.
    pub reference: String,
    /// Whether the source branch should be removed when merging.
    pub remove_source_branch: bool,
}

impl MergeRequest {
    /// Create a commit status for the merge request.
    pub fn create_commit_status<'a>(&'a self, state: CommitStatusState, name: &'a str,
                                    description: &'a str)
                                    -> PendingCommitStatus<'a> {
        PendingCommitStatus {
            commit: &self.commit,
            state: state,
            refname: Some(&self.source_branch),
            name: name,
            description: description,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// States for a commit status.
pub enum CommitStatusState {
    /// The check is expected, but has not started yet.
    Pending,
    /// The check is currently running.
    Running,
    /// The check is a success.
    Success,
    /// The check is a failure.
    Failed,
}

impl From<CommitStatusState> for CheckStatus {
    fn from(state: CommitStatusState) -> Self {
        match state {
            CommitStatusState::Success => CheckStatus::Pass,
            CommitStatusState::Failed => CheckStatus::Fail,
            CommitStatusState::Pending |
            CommitStatusState::Running => CheckStatus::Unchecked,
        }
    }
}

#[derive(Debug, Clone)]
/// A commit status for a specific commit.
pub struct CommitStatus {
    /// The state of the commit status.
    pub state: CommitStatusState,
    /// The author of the commit status.
    pub author: User,
    /// The refname of the commit (if applicable).
    pub refname: Option<String>,
    /// The name of the check being performed.
    pub name: String,
    /// A description of the check.
    pub description: String,
}

#[derive(Debug, Clone)]
/// A user on the service.
pub struct User {
    /// The internal ID of the user.
    ///
    /// Service-specific.
    pub id: u64,
    /// The username on the service of the user (used for mentioning the user).
    pub handle: String,
    /// The real name of the user.
    pub name: String,
    /// The email address of the user.
    pub email: String,
}

impl User {
    /// Convenience method for getting an identity for the user.
    pub fn identity(&self) -> Identity {
        Identity::new(&self.name, &self.email)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
/// Access level to a project.
pub enum AccessLevel {
    /// A user external to the project.
    Contributor,
    /// A user with permission to develop on the project directly.
    Developer,
    /// A user with permission to maintain integration branches on the project.
    Maintainer,
    /// A user with permission to administrate the project.
    Owner,
}

#[derive(Debug, Clone)]
/// A membership of a user to a project, team, or group.
pub struct Membership {
    /// The user with access.
    pub user: User,

    /// The access level of the user.
    pub access_level: AccessLevel,

    /// When the membership expires (if at all).
    pub expiration: Option<DateTime<Utc>>,
}

#[derive(Debug, Clone)]
/// A comment on the service.
pub struct Comment {
    /// The ID of the note.
    pub id: u64,
    /// Indicates whether the comment is autogenerated (via activity or mentions) or not.
    pub is_system: bool,
    /// Indicates whether the comment indicates a branch update or not.
    ///
    /// This is used to separate the comment stream into before and after for an update to its
    /// source topic.
    pub is_branch_update: bool,
    /// When the comment was created.
    pub created_at: DateTime<Utc>,
    /// The author of the comment.
    pub author: User,
    /// The content of the comment.
    pub content: String,
}

#[derive(Debug, Clone)]
/// An award on the service.
pub struct Award {
    /// The name of the award.
    pub name: String,
    /// The author of the award.
    pub author: User,
}

error_chain! {
    errors {
        /// An error occurred when communicating with the host.
        Host {
            description("service error")
        }

        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

#[derive(Clone)]
/// A project hosted on a service.
pub struct HostedProject {
    /// The name of the project.
    pub name: String,
    /// The service the project is hosted on.
    pub service: Arc<HostingService>,
}

impl HostedProject {
    /// Add a member to the project.
    pub fn add_member(&self, user: &User, level: AccessLevel) -> Result<()> {
        self.service.add_member(&self.name, user, level)
    }

    /// Get the membership list of the project.
    pub fn members(&self) -> Result<Vec<Membership>> {
        self.service.members(&self.name)
    }

    /// Add a wehhook to the project.
    pub fn add_hook(&self, url: &str) -> Result<()> {
        self.service.add_hook(&self.name, url)
    }

    /// Get a commit for a project.
    pub fn commit(&self, commit: &CommitId) -> Result<Commit> {
        self.service.commit(&self.name, commit)
    }

    /// Get a merge request on a project.
    pub fn merge_request(&self, id: u64) -> Result<MergeRequest> {
        self.service.merge_request(&self.name, id)
    }
}

impl Debug for HostedProject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("HostedProject")
            .field("name", &self.name)
            .finish()
    }
}

/// A hosting service.
pub trait HostingService: Send + Sync {
    /// Fetch a commit into a given git context.
    ///
    /// The default implementation requires that the commit have a valid refname, otherwise the
    /// fetch will fail.
    fn fetch_commit(&self, git: &GitContext, commit: &Commit) -> Result<()> {
        if let Some(ref refname) = commit.refname {
            git.fetch(&commit.repo.url, &[refname])
                .chain_err(|| ErrorKind::Git("failed to fetch commit".to_string()))?;
        } else {
            bail!(ErrorKind::Msg("cannot fetch unnamed commits".to_string()));
        }

        Ok(())
    }
    /// Fetch a merge request into a given git context.
    fn fetch_mr(&self, git: &GitContext, mr: &MergeRequest) -> Result<()> {
        git.fetch(&mr.source_repo.url, &[&mr.source_branch])
            .chain_err(|| ErrorKind::Git("failed to fetch merge request".to_string()))?;

        Ok(())
    }

    /// The user the service is acting as.
    fn service_user(&self) -> &User;

    /// Add a user to a project with the given access level.
    fn add_member(&self, project: &str, user: &User, level: AccessLevel) -> Result<()>;

    /// Return the list of memberships to a project.
    ///
    /// This should include *all* memberships which provide a permission to the project above that
    /// of an anonymous user.
    fn members(&self, project: &str) -> Result<Vec<Membership>>;

    /// Register a webhook for a project.
    ///
    /// The webhook should listen for all events that matter for the workflow (e.g., pushes,
    /// updates to merge requests, etc.).
    fn add_hook(&self, project: &str, url: &str) -> Result<()>;

    /// Get a user by name.
    fn user(&self, user: &str) -> Result<User>;
    /// Get a commit for a project.
    fn commit(&self, project: &str, commit: &CommitId) -> Result<Commit>;
    /// Get an issue on a project.
    fn issue(&self, project: &str, id: u64) -> Result<Issue>;
    /// Get a merge request on a project.
    fn merge_request(&self, project: &str, id: u64) -> Result<MergeRequest>;
    /// Get a repository by name.
    fn repo(&self, project: &str) -> Result<Repo>;

    /// Get a user by ID.
    fn user_by_id(&self, user: u64) -> Result<User>;
    /// Get a commit for a project by ID.
    fn commit_by_id(&self, project: u64, commit: &CommitId) -> Result<Commit>;
    /// Get an issue on a project by ID.
    fn issue_by_id(&self, project: u64, id: u64) -> Result<Issue>;
    /// Get a merge request on a project by ID.
    fn merge_request_by_id(&self, project: u64, id: u64) -> Result<MergeRequest>;
    /// Get a repository by ID.
    fn repo_by_id(&self, project: u64) -> Result<Repo>;

    /// Get comments for an issue.
    ///
    /// Comments are ordered from oldest to newest.
    fn get_issue_comments(&self, issue: &Issue) -> Result<Vec<Comment>>;
    /// Add a comment to an issue.
    fn post_issue_comment(&self, issue: &Issue, content: &str) -> Result<()>;
    /// Get comments for a merge request.
    ///
    /// Comments are ordered from oldest to newest.
    fn get_mr_comments(&self, mr: &MergeRequest) -> Result<Vec<Comment>>;
    /// Add a comment to a merge request.
    fn post_mr_comment(&self, mr: &MergeRequest, content: &str) -> Result<()>;
    /// Add a comment to a commit.
    fn post_commit_comment(&self, commit: &Commit, content: &str) -> Result<()>;
    /// Get the latest commit statuses for a commit.
    fn get_commit_statuses(&self, commit: &Commit) -> Result<Vec<CommitStatus>>;
    /// Create a commit status.
    fn post_commit_status(&self, status: PendingCommitStatus) -> Result<()>;

    /// Get awards on a merge request.
    fn get_mr_awards(&self, mr: &MergeRequest) -> Result<Vec<Award>>;
    /// Get awards on a merge request comment.
    fn get_mr_comment_awards(&self, mr: &MergeRequest, comment: &Comment) -> Result<Vec<Award>>;
    /// Award an emoji to a merge request comment.
    fn award_mr_comment(&self, mr: &MergeRequest, comment: &Comment, award: &str) -> Result<()>;
    /// The award to give comments which were acted upon.
    fn comment_award_name(&self) -> &str;

    /// Get issues which are closed by a merge request.
    fn issues_closed_by_mr(&self, mr: &MergeRequest) -> Result<Vec<Issue>>;
    /// Add labels to an issue.
    fn add_issue_labels(&self, issue: &Issue, labels: &[&str]) -> Result<()>;
}
