// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Utilities related to merge requests.

extern crate git_workarea;
use self::git_workarea::{CommitId, GitContext};

use host::MergeRequest;

error_chain! {
    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

/// Check that a merge request contains a given commit compared to a target branch.
pub fn contains_commit(ctx: &GitContext, mr: &MergeRequest, commit: &CommitId, base: &CommitId)
                       -> Result<bool> {
    let rev_parse = ctx.git()
        .arg("rev-parse")
        .arg(commit.as_str())
        .output()
        .chain_err(|| "failed to construct rev-parse command")?;
    if !rev_parse.status.success() {
        bail!(ErrorKind::Git(format!("failed to parse the commit {}: {}",
                                     commit,
                                     String::from_utf8_lossy(&rev_parse.stderr))));
    }
    let commit_id = String::from_utf8_lossy(&rev_parse.stdout);

    let rev_list = ctx.git()
        .arg("rev-list")
        .arg(format!("^{}", base))
        .arg(mr.commit.id.as_str())
        .output()
        .chain_err(|| "failed to construct rev-list command")?;
    if !rev_list.status.success() {
        bail!(ErrorKind::Git(format!("failed to list commits on the merge request {}: {}",
                                     mr.url,
                                     String::from_utf8_lossy(&rev_list.stderr))));
    }
    let commits = String::from_utf8_lossy(&rev_list.stdout);

    Ok(commits.lines().any(|commit| commit == commit_id.trim()))
}
