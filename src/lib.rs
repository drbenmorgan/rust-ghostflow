// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![warn(missing_docs)]
#![recursion_limit="128"]

//! Ghostflow
//!
//! This crate implements actions which are part of ghostflow, the git-hosted development workflow.
//! This includes things such as applying hook checks to a commit, merging branches, handling
//! testing commands, and so on. See the documentation for the various actions for more
//! information.

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

mod crates {
    // public
    pub extern crate chrono;
    // pub extern crate error_chain;
    pub extern crate git_checks;
    pub extern crate git_topic_stage;
    pub extern crate git_workarea;
    pub extern crate serde_json;

    // private
    pub extern crate crypto;
    pub extern crate either;
    pub extern crate itertools;
    pub extern crate log;
    pub extern crate rand;
    pub extern crate rayon;
    pub extern crate regex;
    pub extern crate tempdir;
    pub extern crate topological_sort;
    pub extern crate wait_timeout;
}

pub mod utils;
pub mod actions;
pub mod host;
