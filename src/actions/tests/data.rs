// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_workarea::GitContext;
use crates::itertools::Itertools;

use actions::data::*;
use actions::tests::utils::test_workspace_dir;
use host::Repo;

use std::fs::{self, Permissions};
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process::{Command, Stdio};

struct TestRepo {
    pub ctx: GitContext,
    pub repo: Repo,
}

impl TestRepo {
    fn new(ctx: GitContext) -> Self {
        TestRepo {
            repo: Repo {
                name: "test_repo".to_string(),
                url: ctx.gitdir().to_string_lossy().into_owned(),
                id: 1000,
                forked_from: None,
            },
            ctx: ctx,
        }
    }
}

fn git_context(workspace_path: &Path) -> (TestRepo, GitContext) {
    // Here, we create two clones of the current repository: one to act as the remote and another
    // to be the repository the data action uses. The first is cloned from the source tree's
    // directory while the second is cloned from that first clone. This sets up the `origin` remote
    // properly for testing the `data` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(&origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("working clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    (TestRepo::new(GitContext::new(origindir)), GitContext::new(gitdir))
}

fn create_data<D>(git: &GitContext, destination: D) -> Data
    where D: AsRef<Path>,
{
    let mut data = Data::new(git.clone());
    data.add_destination(destination.as_ref().to_string_lossy());
    data
}

fn data_ref_exists(ctx: &GitContext, namespace: &str, algo: &str, hash: &str) -> bool {
    ctx.git()
        .arg("show-ref")
        .arg("--quiet")
        .arg("--verify")
        .arg(format!("refs/{}/{}/{}", namespace, algo, hash))
        .status()
        .unwrap()
        .success()
}

fn make_data_object(ctx: &GitContext, namespace: &str, contents: &[u8], algo: &str, hash: &str) {
    let mut hash_object = ctx.git()
        .arg("hash-object")
        .arg("-w")
        .arg("-t").arg("blob")
        .arg("--stdin")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .unwrap();
    hash_object.stdin.as_mut().unwrap().write_all(contents).unwrap();
    let hash_object = hash_object.wait_with_output().unwrap();
    if !hash_object.status.success() {
        panic!("hash object creation failed: {}",
               String::from_utf8_lossy(&hash_object.stderr));
    }
    let object = String::from_utf8_lossy(&hash_object.stdout);

    let refname = format!("refs/{}/{}/{}", namespace, algo, hash);
    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(&refname)
        .arg(object.trim())
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("updating the data ref {} failed: {}",
               refname,
               String::from_utf8_lossy(&update_ref.stderr));
    }
    assert!(data_ref_exists(ctx, namespace, algo, hash));
}

const VALID_DATA: &[(&str, &str)] = &[
    ("MD5", "eb733a00c0c9d336e65691a37ab54293"),
    ("SHA256", "916f0027a575074ce72a331777c3478d6513f786a591bd892da1a577bf2335f9"),
    ("SHA512", "0e1e21ecf105ec853d24d728867ad70613c21663a4693074b2a3619c1bd39d66\
                b588c33723bb466c72424e80e3ca63c249078ab347bab9428500e7ee43059d0d"),
];

const INVALID_DATA: &[(&str, &str)] = &[
    ("MD5", "notahash"),
    ("SHA256", "notahash"),
    ("SHA512", "notahash"),
];

fn create_data_refs(data: &[(&str, &str)], ctx: &GitContext, namespace: &str) {
    let contents = b"test data";

    data.iter()
        .foreach(|&(algo, hash)| make_data_object(ctx, namespace, contents, algo, hash));
}

fn check_data_refs(data: &[(&str, &str)], ctx: &GitContext, namespace: &str, keep_refs: bool) {
    data.iter()
        .foreach(|&(algo, hash)| {
            assert_eq!(data_ref_exists(ctx, namespace, algo, hash), keep_refs);
        });
}

fn check_data<D>(data: &[(&str, &str)], destination: D)
    where D: AsRef<Path>,
{
    let path = destination.as_ref();
    data.iter()
        .foreach(|&(algo, hash)| {
            let data_file = path.join(algo).join(hash);
            assert!(data_file.exists());
            assert!(fs::metadata(data_file).unwrap().permissions().readonly());
        });
}

#[test]
// Repositories without data should do nothing.
fn test_no_data() {
    let tempdir = test_workspace_dir("test_no_data");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let data = create_data(&ctx, destination);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::NoData);
}

#[test]
// No destination in the action should fail.
fn test_no_destinations() {
    let tempdir = test_workspace_dir("test_no_destinations");
    let (test_repo, ctx) = git_context(tempdir.path());
    let data = Data::new(ctx.clone());

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let err = data.fetch_data(&test_repo.repo).unwrap_err();

    if let ErrorKind::NoDestinations = *err.kind() {
        // OK
    } else {
        panic!("unexpected error: {:?}", err);
    }
}

#[test]
// No data shouldn't care if the destinations is empty.
fn test_no_destinations_no_data() {
    let tempdir = test_workspace_dir("test_no_destinations_no_data");
    let (test_repo, ctx) = git_context(tempdir.path());
    let data = Data::new(ctx.clone());

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::NoData);
}

#[test]
// Repositories with data should appear in the destination.
fn test_data_sync() {
    let tempdir = test_workspace_dir("test_data_sync");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let data = create_data(&ctx, &destination);

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(VALID_DATA, &ctx, "data", false);
}

#[test]
// Repositories with data should appear in multiple destinations.
fn test_data_sync_multiple() {
    let tempdir = test_workspace_dir("test_data_sync_multiple");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let destination2 = tempdir.path().join("destination2");
    let mut data = create_data(&ctx, &destination);

    data.add_destination(destination2.to_string_lossy());

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data(VALID_DATA, &destination2);
    check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(VALID_DATA, &ctx, "data", false);
}

#[test]
// Repositories with data should appear in the destination with the right namespace.
fn test_data_sync_namespace() {
    let tempdir = test_workspace_dir("test_data_sync_namespace");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    let namespace = "data-alternate";
    data.ref_namespace(namespace);

    create_data_refs(VALID_DATA, &test_repo.ctx, namespace);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, namespace, false);
    check_data_refs(VALID_DATA, &ctx, namespace, false);
}

#[test]
// Repositories with data should appear in the destination with the right namespace even if the
// namespace has a slash in it.
fn test_data_sync_namespace_subdir() {
    let tempdir = test_workspace_dir("test_data_sync_namespace_subdir");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    let namespace = "data-alternate/with/subdir";
    data.ref_namespace(namespace);

    create_data_refs(VALID_DATA, &test_repo.ctx, namespace);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, namespace, false);
    check_data_refs(VALID_DATA, &ctx, namespace, false);
}

#[test]
// Repositories with data should appear in the destination, but refs should be allowed to stay.
fn test_data_sync_keep_refs() {
    let tempdir = test_workspace_dir("test_data_sync_keep_refs");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    data.keep_refs();

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, "data", true);
    check_data_refs(VALID_DATA, &ctx, "data", true);
}

#[test]
// Repositories with data should appear in the destination, but refs should be allowed to stay.
fn test_data_sync_keep_refs_namespace() {
    let tempdir = test_workspace_dir("test_data_sync_keep_refs_namespace");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    let namespace = "data-alternate";
    data.keep_refs()
        .ref_namespace(namespace);

    create_data_refs(VALID_DATA, &test_repo.ctx, namespace);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, namespace, true);
    check_data_refs(VALID_DATA, &ctx, namespace, true);
}

#[test]
// Repositories with data should appear in the destination.
fn test_data_sync_hash_mismatch() {
    let tempdir = test_workspace_dir("test_data_sync_hash_mismatch");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let data = create_data(&ctx, &destination);

    create_data_refs(INVALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data_refs(INVALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(INVALID_DATA, &ctx, "data", false);
}

#[test]
// Invalid data should never be kept locally, but remote should be allowed to stay.
fn test_data_sync_hash_mismatch_keep_data() {
    let tempdir = test_workspace_dir("test_data_sync_hash_mismatch_keep_data");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    data.keep_refs();

    create_data_refs(INVALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data_refs(INVALID_DATA, &test_repo.ctx, "data", true);
    check_data_refs(INVALID_DATA, &ctx, "data", false);
}

#[test]
// Failure to sync should fail safely.
fn test_data_sync_fail() {
    let tempdir = test_workspace_dir("test_data_sync_fail");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination_parent = tempdir.path().join("destination");
    let destination = destination_parent.join("child");
    let data = create_data(&ctx, &destination);
    fs::create_dir_all(&destination_parent).unwrap();
    fs::set_permissions(&destination_parent, Permissions::from_mode(0o555)).unwrap();

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let err = data.fetch_data(&test_repo.repo).unwrap_err();

    if let ErrorKind::Rsync(ref msg) = *err.kind() {
        assert!(msg.starts_with("failed to sync data to"));
    } else {
        panic!("unexpected error: {:?}", err);
    }

    check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(VALID_DATA, &ctx, "data", true);
}

#[test]
// Failure to sync should fail safely even if a destination fails.
fn test_data_sync_fail_multiple() {
    let tempdir = test_workspace_dir("test_data_sync_fail_multiple");
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination_parent = tempdir.path().join("destination");
    let destination = destination_parent.join("child");
    let destination2 = tempdir.path().join("destination2");
    let mut data = create_data(&ctx, &destination);
    data.add_destination(destination2.to_string_lossy());
    fs::create_dir_all(&destination_parent).unwrap();
    fs::set_permissions(&destination_parent, Permissions::from_mode(0o555)).unwrap();

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let err = data.fetch_data(&test_repo.repo).unwrap_err();

    if let ErrorKind::Rsync(ref msg) = *err.kind() {
        assert!(msg.starts_with("failed to sync data to"));
    } else {
        panic!("unexpected error: {:?}", err);
    }

    check_data(VALID_DATA, &destination2);

    check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(VALID_DATA, &ctx, "data", true);
}
