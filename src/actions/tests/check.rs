// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_checks::GitCheckConfiguration;
use crates::git_checks::checks;
use crates::git_workarea::{CommitId, GitContext};

use actions::check::*;
use actions::tests::host::MockService;
use actions::tests::utils::test_workspace_dir;
use host::{CommitStatusState, HostingService};

use std::path::Path;
use std::process::Command;

mod test_checks {
    use crates::git_checks::impl_prelude::*;

    #[derive(Debug, Default, Clone, Copy)]
    pub struct Temporary;

    impl Check for Temporary {
        fn name(&self) -> &str {
            "temporary"
        }

        fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult> {
            let mut result = CheckResult::new();

            result.add_warning(format!("A temporary warning for commit {}.", commit.sha1))
                .make_temporary();

            Ok(result)
        }
    }

    #[derive(Debug, Default, Clone, Copy)]
    pub struct AlwaysWarn;

    impl Check for AlwaysWarn {
        fn name(&self) -> &str {
            "always-warn-commit"
        }

        fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult> {
            let mut result = CheckResult::new();

            result.add_warning(format!("This warning shows up for commit {}.", commit.sha1));

            Ok(result)
        }
    }

    impl BranchCheck for AlwaysWarn {
        fn name(&self) -> &str {
            "always-warn-branch"
        }

        fn check(&self, _: &CheckGitContext, _: &CommitId) -> Result<CheckResult> {
            let mut result = CheckResult::new();

            result.add_warning("This warning shows up for every branch.");

            Ok(result)
        }
    }
}

const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";

fn git_context(workspace_path: &Path) -> GitContext {
    let gitdir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    GitContext::new(gitdir)
}

fn admins() -> Vec<String> {
    vec![
        "admin".to_string(),
        "admin2".to_string(),
    ]
}

#[test]
// A successful check should not comment, but post a success status on the branch and each commit.
fn test_check_success() {
    let tempdir = test_workspace_dir("test_check_success");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 1).unwrap();
    let config = GitCheckConfiguration::new();

    service.step(2);
    let mr_update = service.merge_request("base", 1).unwrap();
    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let result = check.check_mr("test_check_success", &CommitId::new(BASE), &mr_update)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_commit_statuses_head = service.commit_statuses(&mr_update.commit.id);
    let mr_commit_statuses_previous = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses_previous.len(), 1);

    let status = &mr_commit_statuses_previous[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name,
               "ghostflow-branch-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd");
    assert_eq!(status.description,
               "overall branch status for the content checks against \
                58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
                \n\
                Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3");
}

#[test]
// A failed check should comment, and post a failure status on the branch and each failing commit;
// passing commits should still pass.
fn test_check_failure() {
    let tempdir = test_workspace_dir("test_check_failure");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 1).unwrap();

    // Add a check which rejects the first commit on the branch.
    let bad_commits = checks::BadCommits::new(&[mr.commit.id.as_str()]);

    let mut config = GitCheckConfiguration::new();
    config.add_check(&bad_commits);

    service.step(2);
    let mr_update = service.merge_request("base", 1).unwrap();
    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let result = check.check_mr("test_check_failure", &CommitId::new(BASE), &mr_update)
        .unwrap();
    assert_eq!(result, CheckStatus::Fail);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses_head = service.commit_statuses(&mr_update.commit.id);
    let mr_commit_statuses_previous = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               concat!("Errors:\n\n",
                       "  - commit 7189cf557ba2c7c61881ff8669158710b94d8df1 is a known-bad commit \
                        that was removed from the server.\n",
                       "\n",
                       "Alerts:\n\n",
                       "  - commit 7189cf557ba2c7c61881ff8669158710b94d8df1 was pushed to the \
                        server.\n",
                       "\n",
                       "Please rewrite commits to fix the errors listed above (adding fixup \
                        commits will not resolve the errors) and force-push the branch again to \
                        update the merge request.\n",
                       "\n",
                       "Alert: @admin @admin2."));

    let status = &mr_commit_statuses_previous[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    assert_eq!(mr_commit_statuses_previous.len(), 1);

    let status = &mr_commit_statuses_previous[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name,
               "ghostflow-branch-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd");
    assert_eq!(status.description,
               "overall branch status for the content checks against \
                58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
                \n\
                Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3");
}

#[test]
// Checks should checkout backported commits properly.
fn test_check_backport_commit() {
    let tempdir = test_workspace_dir("test_check_backport_commit");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 1).unwrap();

    let config = GitCheckConfiguration::new();

    service.step(2);

    let mr_update = service.merge_request("base", 1).unwrap();
    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let result = check.check_mr_with("test_check_backport_commit",
                                     &CommitId::new(BASE),
                                     &mr_update,
                                     &mr.commit.id)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let mr_update_commit_statuses = service.commit_statuses(&mr_update.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    assert_eq!(mr_update_commit_statuses.len(), 1);

    let status = &mr_update_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name,
               "ghostflow-branch-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd");
    assert_eq!(status.description,
               "overall branch status for the content checks against \
                58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
                \n\
                Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1");
}

#[test]
// Checks should bail out if a commit was requested to be checked that was not part of the merge
// request.
fn test_check_unrelated_commit() {
    let tempdir = test_workspace_dir("test_check_unrelated_commit");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 1).unwrap();

    let config = GitCheckConfiguration::new();

    service.step(3);

    let mr_update = service.merge_request("base", 1).unwrap();
    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let err = check.check_mr_with("test_check_unrelated_commit",
                                  &CommitId::new(BASE),
                                  &mr_update,
                                  &mr.commit.id)
        .unwrap_err();

    if let ErrorKind::UnrelatedCommit(ref commit) = *err.kind() {
        assert_eq!(commit, &mr.commit.id);
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Checks should run branch checks exactly once.
fn test_check_branch_checks_once() {
    let tempdir = test_workspace_dir("test_check_branch_checks_once");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 1).unwrap();

    let always_warn = test_checks::AlwaysWarn;

    let mut config = GitCheckConfiguration::new();
    config.add_check(&always_warn);
    config.add_branch_check(&always_warn);

    service.step(2);

    let mr_update = service.merge_request("base", 1).unwrap();
    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let result = check.check_mr("test_check_branch_checks_once",
                                &CommitId::new(BASE),
                                &mr_update)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let mr_commit_update_statuses = service.commit_statuses(&mr_update.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               concat!("Warnings:\n\n",
                       "  - This warning shows up for commit \
                        fe70f127605efb6032cacea0bd336428d67ed5a3.\n",
                       "  - This warning shows up for commit \
                        7189cf557ba2c7c61881ff8669158710b94d8df1.\n",
                       "  - This warning shows up for every branch.\n",
                       "\n",
                       "The warnings do not need to be fixed, but it is recommended to do so."));

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    assert_eq!(mr_commit_update_statuses.len(), 2);

    let status = &mr_commit_update_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    let status = &mr_commit_update_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr_update.source_branch.clone()));
    assert_eq!(status.name,
               "ghostflow-branch-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd");
    assert_eq!(status.description,
               "overall branch status for the content checks against \
                58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
                \n\
                Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3");
}

#[test]
// A successful check with warnings should comment, and post a success status on the branch and
// each commit.
fn test_check_warning() {
    let tempdir = test_workspace_dir("test_check_warning");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 2).unwrap();
    let config = GitCheckConfiguration::new();
    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let result = check.check_mr("test_check_warning", &CommitId::new(BASE), &mr).unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               concat!("Warnings:\n\n",
                       "  - the merge request is marked as a work-in-progress.\n",
                       "\n",
                       "The warnings do not need to be fixed, but it is recommended to do so."));

    assert_eq!(mr_commit_statuses.len(), 2);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    let status = &mr_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name,
               "ghostflow-branch-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd");
    assert_eq!(status.description,
               "overall branch status for the content checks against \
                58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
                \n\
                Branch-at: f6f8de8c7c5f1a081b14f5a47c7798268f383222");
}

#[test]
// A successful check with temporary warnings should comment, and post a success status on the
// branch and each commit.
fn test_check_warning_temporary() {
    let tempdir = test_workspace_dir("test_check_warning_temporary");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mr = service.merge_request("base", 1).unwrap();

    let temporary = test_checks::Temporary;

    let mut config = GitCheckConfiguration::new();
    config.add_check(&temporary);

    let admins = admins();
    let check = Check::new(ctx.clone(), service.clone(), config, &admins);

    let result = check.check_mr("test_check_warning_temporary", &CommitId::new(BASE), &mr).unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               concat!("Warnings:\n\n",
                       "  - A temporary warning for commit \
                        7189cf557ba2c7c61881ff8669158710b94d8df1.\n",
                       "\n",
                       "The warnings do not need to be fixed, but it is recommended to do so.\n",
                       "\n",
                       "Some messages may be temporary; please trigger the checks again if they \
                        have been resolved."));

    assert_eq!(mr_commit_statuses.len(), 2);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-commit-check");
    assert_eq!(status.description, "basic content checks");

    let status = &mr_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name,
               "ghostflow-branch-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd");
    assert_eq!(status.description,
               "overall branch status for the content checks against \
                58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
                \n\
                Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1");
}
