// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::serde_json::{self, Value};

use actions::test::jobs::*;
use actions::tests::host::MockService;
use actions::tests::utils::test_workspace_dir;
use host::{HostedProject, HostingService};

use std::fs::{self, File};
use std::path::Path;
use std::sync::Arc;

fn create_test(workspace_path: &Path, service: &Arc<MockService>) -> TestJobs {
    let project = HostedProject {
        name: "base".to_string(),
        service: Arc::clone(service) as Arc<HostingService>,
    };

    TestJobs::new(workspace_path.join("queue"), project).unwrap()
}

fn check_mr_job(path: &Path) {
    let queue = path.join("queue");

    for entry in fs::read_dir(queue).unwrap() {
        let entry = entry.unwrap();

        assert_eq!(entry.path().extension().unwrap(), "json");

        let job = File::open(entry.path()).unwrap();
        let data: Value = serde_json::from_reader(job).unwrap();

        assert_eq!(data, Value::Null);
    }
}

#[test]
// Testing an update should drop a job file in the queue.
fn test_test_updates() {
    let tempdir = test_workspace_dir("test_test_job_update");
    let service = MockService::test_service();
    let test = create_test(tempdir.path(), &service);

    test.test_update(Value::Null).unwrap();

    check_mr_job(tempdir.path());

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Testing an MR should drop a job file in the queue with the expected contents and comment.
fn test_test_mr() {
    let tempdir = test_workspace_dir("test_test_job_mr");
    let service = MockService::test_service();
    let test = create_test(tempdir.path(), &service);

    let mr = service.merge_request("base", 1).unwrap();
    test.test_mr(&mr, Value::Null).unwrap();

    check_mr_job(tempdir.path());

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "This topic has been queued for testing.");
}

#[test]
// Testing an MR should drop a job file in the queue with the expected contents.
fn test_test_mr_quiet() {
    let tempdir = test_workspace_dir("test_test_job_mr_quiet");
    let service = MockService::test_service();
    let mut test = create_test(tempdir.path(), &service);
    test.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    test.test_mr(&mr, Value::Null).unwrap();

    check_mr_job(tempdir.path());

    assert_eq!(service.remaining_data(), 0);
}
