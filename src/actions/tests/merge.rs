// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::{DateTime, Utc};
use crates::git_workarea::{CommitId, GitContext, Identity};
use crates::itertools::Itertools;

use actions::merge::*;
use actions::tests::host::MockService;
use actions::tests::utils::test_workspace_dir;
use host::{HostedProject, HostingService, User};
use utils::Trailer;

use std::collections::HashMap;
use std::iter;
use std::path::Path;
use std::process::Command;
use std::sync::Arc;

const BRANCH_NAME: &str = "test-merge";
const BACKPORT_BRANCH_NAME: &str = "test-merge-backport";
const BACKPORT_BRANCH_NAME2: &str = "test-merge-backport2";
const INTO_BRANCH_NAME: &str = "test-merge-into";
const INTO_BRANCH_NAME2: &str = "test-merge-into2";
const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";
const BASE_UPDATE: &str = "11bfbf44147015650afe6f95508ecfb1a77443cd";

lazy_static! {
    static ref MERGER_IDENT: Identity =
        Identity::new("merger", "merger@example.com");
    static ref AUTHOR_DATE: DateTime<Utc> = Utc::now();
}

fn git_context(workspace_path: &Path, commit: &str) -> (GitContext, GitContext) {
    // Here, we create two clones of the current repository: one to act as the remote and another
    // to be the repository the merge action uses. The first is cloned from the source tree's
    // directory while the second is cloned from that first clone. This sets up the `origin` remote
    // properly for the `merge` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg("--single-branch")
        // Clone as a "remote" URL to only get the relevant objects. If a local URL is used, packs
        // may be shared which affects the `--auto-abbrev` used in the merge logic.
        .arg(concat!("file://", env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    // Make the branches point to the expected place.
    let origin_ctx = GitContext::new(&origindir);
    let branches = &[
        BRANCH_NAME,
        BACKPORT_BRANCH_NAME,
        BACKPORT_BRANCH_NAME2,
        INTO_BRANCH_NAME,
        INTO_BRANCH_NAME2,
    ];
    for branch in branches {
        let update_ref = origin_ctx.git()
            .arg("update-ref")
            .arg(format!("refs/heads/{}", branch))
            .arg(commit)
            .output()
            .unwrap();
        if !update_ref.status.success() {
            panic!("update-ref failed: {}",
                   String::from_utf8_lossy(&update_ref.stderr));
        }
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg("--dissociate")
        .arg(origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("working clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    (origin_ctx, GitContext::new(gitdir))
}

fn make_commit(ctx: &GitContext, branch: &str) {
    let commit = ctx.git()
        .arg("commit-tree")
        .arg("-p").arg(branch)
        .arg("-m").arg("blocking commit")
        .arg(format!("{}^{{tree}}", branch))
        .output()
        .unwrap();
    if !commit.status.success() {
        panic!("blocking commit creation failed: {}",
               String::from_utf8_lossy(&commit.stderr));
    }
    let commit = String::from_utf8_lossy(&commit.stdout);
    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", branch))
        .arg(commit.trim())
        .status()
        .unwrap();
    assert!(update_ref.success());
}

#[derive(Debug)]
struct TestMergePolicy {
    trailers: Vec<Trailer>,
    needs_reviews_from: HashMap<u64, String>,
    rejections: Vec<String>,
}

impl Default for TestMergePolicy {
    fn default() -> Self {
        TestMergePolicy {
            trailers: vec![
                Trailer::new("Acked-by", "Ghostflow <ghostflow@example.com>"),
            ],
            needs_reviews_from: HashMap::new(),
            rejections: Vec::new(),
        }
    }
}

impl MergePolicyFilter for TestMergePolicy {
    fn process_trailer(&mut self, trailer: &Trailer, user: Option<&User>) {
        // Skip trailers which do not end in `-by`.
        if !trailer.token.ends_with("-by") {
            return;
        }

        // Ignore trailers by the `ignore` user.
        if let Some(user) = user {
            if user.handle == "ignore" {
                return;
            }
        }

        if trailer.token == "Require-review-by" {
            if let Some(user) = user {
                let msg = format!("review is required by @{}", user.handle);
                self.needs_reviews_from.insert(user.id, msg);
            }
        } else if trailer.token == "Rejected-by" {
            // Block if anyone else says `Rejected-by`.
            let reason = if let Some(user) = user {
                format!("rejected by @{}", user.handle)
            } else {
                format!("rejected by {}", trailer.value)
            };

            self.rejections.push(reason);
        } else {
            // Add the trailer.
            self.trailers.push(trailer.clone());
        }
    }

    fn result(self) -> ::std::result::Result<Vec<Trailer>, Vec<String>> {
        let reasons = self.rejections
            .into_iter()
            .chain(self.needs_reviews_from
                .into_iter()
                .map(|(_, value)| value))
            .collect::<Vec<_>>();

        if reasons.is_empty() {
            Ok(self.trailers)
        } else {
            Err(reasons)
        }
    }
}

fn create_merge(git: &GitContext, base: &str, service: &Arc<MockService>)
                -> Merge<TestMergePolicy> {
    let settings = MergeSettings::new(base, TestMergePolicy::default());
    create_merge_settings(git, service, settings)
}

fn create_merge_settings(git: &GitContext, service: &Arc<MockService>,
                         settings: MergeSettings<TestMergePolicy>)
                         -> Merge<TestMergePolicy> {
    let project = HostedProject {
        name: "base".to_string(),
        service: Arc::clone(service) as Arc<HostingService>,
    };
    Merge::new(git.clone(), project, settings)
}

fn create_merge_many(git: &GitContext, service: &Arc<MockService>) -> MergeMany {
    let project = HostedProject {
        name: "base".to_string(),
        service: Arc::clone(service) as Arc<HostingService>,
    };
    MergeMany::new(git.clone(), project)
}

fn check_mr_commit_message(ctx: &GitContext, branch: &str, expected: &str) {
    let cat_file = ctx.git()
        .arg("cat-file")
        .arg("-p")
        .arg(format!("refs/heads/{}", branch))
        .output()
        .unwrap();
    assert!(cat_file.status.success());
    let actual = String::from_utf8_lossy(&cat_file.stdout)
        .splitn(2, "\n\n")
        .skip(1)
        .join("\n\n");
    assert_eq!(actual, expected);
}

fn check_mr_commit(ctx: &GitContext, branch: &str, expected: &str) {
    check_mr_commit_message(ctx, branch, expected);

    let author_log = ctx.git()
        .arg("log")
        .arg("--pretty=%an%n%ae%n%ad")
        .arg("--max-count=1")
        .arg(format!("refs/heads/{}", branch))
        .output()
        .unwrap();
    assert!(author_log.status.success());
    let actual = String::from_utf8_lossy(&author_log.stdout);
    let author_log_lines = actual.lines().collect::<Vec<_>>();
    assert_eq!(author_log_lines[0], MERGER_IDENT.name);
    assert_eq!(author_log_lines[1], MERGER_IDENT.email);
    assert_eq!(author_log_lines[2],
               AUTHOR_DATE.format("%a %b %-d %H:%M:%S %Y %z").to_string());
}

fn check_noop_merge(ctx: &GitContext, branch: &str) {
    let diff = ctx.git()
        .arg("diff")
        .arg("--exit-code")
        .arg(format!("refs/heads/{}", branch))
        .arg(format!("refs/heads/{}~", branch))
        .output()
        .unwrap();
    if !diff.status.success() {
        panic!("expected no diff, found:\n{}",
               String::from_utf8_lossy(&diff.stdout))
    }
}

#[test]
// Merging should succeed with a different branch name.
fn test_merge_simple_branch_name() {
    let tempdir = test_workspace_dir("test_merge_simple_branch_name");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr_named(&mr, "topic-1-renamed", &MERGER_IDENT, *AUTHOR_DATE)
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1-renamed' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should succeed.
fn test_merge_simple() {
    let tempdir = test_workspace_dir("test_merge_simple");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should succeed with the right "into" branch name.
fn test_merge_simple_as_named() {
    let tempdir = test_workspace_dir("test_merge_simple_as_named");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.merge_branch_as(Some("custom-branch"));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into custom-branch\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should succeed without the "into" name for `master`.
fn test_merge_simple_as_master() {
    let tempdir = test_workspace_dir("test_merge_simple_as_master");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.merge_branch_as(Some("master"));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1'\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should not comment when quiet.
fn test_merge_simple_quiet() {
    let tempdir = test_workspace_dir("test_merge_simple_quiet");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.quiet();

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    assert_eq!(service.remaining_data(), 0);

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should create a log without elision if the limit is over the length.
fn test_merge_simple_log_limit_over() {
    let tempdir = test_workspace_dir("test_merge_simple_log_limit_over");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.log_limit(Some(1));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should not elide if the limit is exactly the topic length.
fn test_merge_simple_log_limit_exact() {
    let tempdir = test_workspace_dir("test_merge_simple_log_limit_exact");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.log_limit(Some(1));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging should elide if the topic is longer than the limit.
fn test_merge_simple_log_limit_elide() {
    let tempdir = test_workspace_dir("test_merge_simple_log_limit_elide");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.log_limit(Some(1));

    let merge = create_merge_settings(&ctx, &service, settings);

    service.step(2);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     fe70f12 topic-1: update\n\
                     ...\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging not log if the limit is zero.
fn test_merge_simple_log_limit_elide_zero() {
    let tempdir = test_workspace_dir("test_merge_simple_log_limit_elide_zero");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.log_limit(Some(0));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// Merging WIP branches should fail.
fn test_merge_wip() {
    let tempdir = test_workspace_dir("test_merge_wip");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", 2).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request is marked as a Work in Progress and may not be merged. Please \
                remove the Work in Progress state first.");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}

#[test]
// Test failure to push to the remote server.
fn test_merge_push_fail() {
    let tempdir = test_workspace_dir("test_merge_push_fail");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    // Make a change on remote the clone is not aware of.
    make_commit(&origin_ctx, BRANCH_NAME);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::PushFailed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "Automatic merge succeeded, but pushing to the remote failed.");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "blocking commit\n");
}

#[test]
// Test failure to push to the remote server.
fn test_merge_push_fail_quiet() {
    let tempdir = test_workspace_dir("test_merge_push_fail_quiet");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.quiet();

    let merge = create_merge_settings(&ctx, &service, settings);

    // Make a change on remote the clone is not aware of.
    make_commit(&origin_ctx, BRANCH_NAME);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::PushFailed);

    assert_eq!(service.remaining_data(), 0);

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "blocking commit\n");
}

#[test]
// Already merged branches should be ignored.
fn test_merge_already_merged() {
    let tempdir = test_workspace_dir("test_merge_already_merged");
    let (_, ctx) = git_context(tempdir.path(), BASE_UPDATE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", 5).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request may not be merged into `test-merge` because it has already been \
                merged.");
}

#[test]
// Branches which do not share history should not be allowed.
fn test_merge_no_common_history() {
    let tempdir = test_workspace_dir("test_merge_no_common_history");
    let (_, ctx) = git_context(tempdir.path(), BASE_UPDATE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    service.step(1);

    let mr = service.merge_request("base", 5).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request may not be merged into `test-merge` because there is no common \
                history.");
}

#[test]
// Conflicts should be reported.
fn test_merge_conflict() {
    let tempdir = test_workspace_dir("test_merge_conflict");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE_UPDATE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", 3).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request contains conflicts with `test-merge` in the following \
                paths:\n\n  \
                - `base`");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
}

#[test]
// Policies should be allowed to reject merge requests.
fn test_merge_policy_rejection() {
    let tempdir = test_workspace_dir("test_merge_policy_rejection");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge = create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request("base", 3).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request may not be merged into `test-merge` because:\n\n  \
                - rejected by @maint");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}

#[test]
// Duplicate target branches should fail.
fn test_merge_backport_duplicate_branches() {
    let tempdir = test_workspace_dir("test_merge_backport_duplicate_branches");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let err = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap_err();

    if let ErrorKind::DuplicateTargetBranch(ref branch) = *err.kind() {
        assert_eq!(branch, BRANCH_NAME);
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: add a base commit\n");
}

#[test]
// Trying to merge an unrelated commit should fail.
fn test_merge_backport_unrelated_commits() {
    let tempdir = test_workspace_dir("test_merge_backport_unrelated_commits");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, Some(CommitId::new(BASE_UPDATE))),
    ];
    let err = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap_err();

    if let ErrorKind::UnrelatedCommit(ref commit) = *err.kind() {
        assert_eq!(commit.as_str(), BASE_UPDATE);
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: add a base commit\n");
}

#[test]
// Conflicts should be reported.
fn test_merge_backport_conflict() {
    let tempdir = test_workspace_dir("test_merge_backport_conflict");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE_UPDATE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 3).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request contains conflicts with `test-merge` in the following \
                paths:\n\n  \
                - `base`");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: update base commit\n");
}

#[test]
// Conflicts should be reported in the right order.
fn test_merge_backport_conflict_order() {
    let tempdir = test_workspace_dir("test_merge_backport_conflict");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE_UPDATE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 3).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request contains conflicts with `test-merge-backport` in the following \
                paths:\n\n  \
                - `base`");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: update base commit\n");
}

#[test]
fn test_merge_backport_policy_rejection() {
    let tempdir = test_workspace_dir("test_merge_backport_policy_rejection");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 3).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request may not be merged into `test-merge` because:\n\n  \
                - rejected by @maint");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: add a base commit\n");
}

#[test]
fn test_merge_backport_policy_rejection_order() {
    let tempdir = test_workspace_dir("test_merge_backport_policy_rejection_order");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 3).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request may not be merged into `test-merge-backport` because:\n\n  \
                - rejected by @maint");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: add a base commit\n");
}

#[test]
fn test_merge_backport_atomic_push() {
    let tempdir = test_workspace_dir("test_merge_backport_atomic_push");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    // Make a change on remote the clone is not aware of.
    make_commit(&origin_ctx, BRANCH_NAME);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::PushFailed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "Automatic merge succeeded, but pushing to the remote failed.");

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "blocking commit\n");
    check_mr_commit_message(&origin_ctx, BACKPORT_BRANCH_NAME, "base: add a base commit\n");
}

#[test]
fn test_merge_backport() {
    let tempdir = test_workspace_dir("test_merge_backport");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge-backport\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
fn test_merge_backport_as_named() {
    let tempdir = test_workspace_dir("test_merge_backport_as_named");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    merge_settings.merge_branch_as(Some("master"));
    let mut merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    merge_settings2.merge_branch_as(Some("custom-branch"));
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1'\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into custom-branch\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
fn test_merge_backport_different_commits() {
    let tempdir = test_workspace_dir("test_merge_backport_as_named");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge = create_merge_many(&ctx, &service);

    let mr_old = service.merge_request("base", 1).unwrap();
    service.step(2);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, Some(mr_old.commit.id)),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     fe70f12 topic-1: update\n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge-backport\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
fn test_merge_backport_different_commits_as_named() {
    let tempdir = test_workspace_dir("test_merge_backport_as_named");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    merge_settings.merge_branch_as(Some("master"));
    let mut merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    merge_settings2.merge_branch_as(Some("custom-branch"));
    let merge = create_merge_many(&ctx, &service);

    let mr_old = service.merge_request("base", 1).unwrap();
    service.step(2);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, Some(mr_old.commit.id)),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1'\n\
                     \n\
                     A simple message\n\
                     \n\
                     fe70f12 topic-1: update\n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into custom-branch\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
fn test_merge_backport_many_branches() {
    let tempdir = test_workspace_dir("test_merge_backport_many_branches");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    let merge_settings3 = MergeSettings::new(BACKPORT_BRANCH_NAME2, TestMergePolicy::default());
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings3, None),
    ];
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge-backport\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME2,
                    "Merge topic 'topic-1' into test-merge-backport2\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
fn test_merge_backport_many_branches_as_named() {
    let tempdir = test_workspace_dir("test_merge_backport_many_branches_as_named");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    merge_settings.merge_branch_as(Some("master"));
    let mut merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    merge_settings2.merge_branch_as(Some("custom-branch"));
    let mut merge_settings3 = MergeSettings::new(BACKPORT_BRANCH_NAME2, TestMergePolicy::default());
    merge_settings3.merge_branch_as(Some("custom-branch2"));
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
        MergeBackport::new(&merge_settings3, None),
    ];
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1'\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into custom-branch\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME2,
                    "Merge topic 'topic-1' into custom-branch2\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// base -> into
fn test_merge_simple_into() {
    let tempdir = test_workspace_dir("test_merge_simple_into");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.add_into_branches(iter::once(IntoBranch::new(INTO_BRANCH_NAME)));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME,
                    "Merge branch 'test-merge' into test-merge-into");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
}

#[test]
// base -> base
fn test_merge_simple_into_cycle() {
    let tempdir = test_workspace_dir("test_merge_simple_into_cycle");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let err = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap_err();

    if let ErrorKind::CircularIntoBranches = *err.kind() {
        // OK
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);

    check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}

#[test]
// base -> into
//      -> into2
fn test_merge_simple_into_many() {
    let tempdir = test_workspace_dir("test_merge_simple_into_many");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    settings.add_into_branches(vec![
        IntoBranch::new(INTO_BRANCH_NAME),
        IntoBranch::new(INTO_BRANCH_NAME2),
    ]);

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME,
                    "Merge branch 'test-merge' into test-merge-into");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME2,
                    "Merge branch 'test-merge' into test-merge-into2");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME2);
}

#[test]
// base -> into -> into2
fn test_merge_simple_into_chain() {
    let tempdir = test_workspace_dir("test_merge_simple_into_chain");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(INTO_BRANCH_NAME);
    into_branch.chain_into(iter::once(IntoBranch::new(INTO_BRANCH_NAME2)));
    settings.add_into_branches(iter::once(into_branch));

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME,
                    "Merge branch 'test-merge' into test-merge-into");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME2,
                    "Merge branch 'test-merge-into' into test-merge-into2");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME2);
}

#[test]
// base -> into -> into2
//      -> into2
fn test_merge_simple_into_multi() {
    let tempdir = test_workspace_dir("test_merge_simple_into_chain_multi");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(INTO_BRANCH_NAME);
    let into_branch2 = IntoBranch::new(INTO_BRANCH_NAME2);
    into_branch.chain_into(iter::once(into_branch2.clone()));
    settings.add_into_branches(vec![
        into_branch,
        into_branch2,
    ]);

    let merge = create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request("base", 1).unwrap();
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME,
                    "Merge branch 'test-merge' into test-merge-into");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    INTO_BRANCH_NAME2,
                    "Merge branch 'test-merge-into' into test-merge-into2");
    check_noop_merge(&origin_ctx, INTO_BRANCH_NAME2);
    let parent = format!("{}~", INTO_BRANCH_NAME2);
    check_mr_commit(&origin_ctx,
                    &parent,
                    "Merge branch 'test-merge' into test-merge-into2");
    check_noop_merge(&origin_ctx, &parent);
}

#[test]
// backport -> branch (mr to both)
fn test_merge_backport_into() {
    let tempdir = test_workspace_dir("test_merge_backport_into");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let merge_settings = MergeSettings::new(BRANCH_NAME, TestMergePolicy::default());
    let mut merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    merge_settings2.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge branch 'test-merge-backport' into test-merge");
    check_noop_merge(&origin_ctx, BRANCH_NAME);
    let parent = format!("{}~", BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    &parent,
                    "Merge topic 'topic-1' into test-merge\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge topic 'topic-1' into test-merge-backport\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// backport2 -> backport -> branch (mr to just backports)
fn test_merge_backport_into_backports() {
    let tempdir = test_workspace_dir("test_merge_backport_into_backports");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut merge_settings = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    merge_settings.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));
    let mut merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME2, TestMergePolicy::default());
    merge_settings2.add_into_branches(iter::once(IntoBranch::new(BACKPORT_BRANCH_NAME)));
    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge branch 'test-merge-backport' into test-merge");
    check_noop_merge(&origin_ctx, BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge branch 'test-merge-backport2' into test-merge-backport");
    check_noop_merge(&origin_ctx, BACKPORT_BRANCH_NAME);
    let parent = format!("{}~", BACKPORT_BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    &parent,
                    "Merge topic 'topic-1' into test-merge-backport\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME2,
                    "Merge topic 'topic-1' into test-merge-backport2\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}

#[test]
// backport2 -> backport -> branch (mr to just backports)
fn test_merge_backport_into_backports_chain() {
    let tempdir = test_workspace_dir("test_merge_backport_into_backports_chain");
    let (origin_ctx, ctx) = git_context(tempdir.path(), BASE);
    let service = MockService::test_service();
    let mut merge_settings = MergeSettings::new(BACKPORT_BRANCH_NAME, TestMergePolicy::default());
    merge_settings.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));
    let mut merge_settings2 = MergeSettings::new(BACKPORT_BRANCH_NAME2, TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(BACKPORT_BRANCH_NAME);
    into_branch.chain_into(iter::once(IntoBranch::new(BRANCH_NAME)));
    merge_settings2.add_into_branches(iter::once(into_branch));

    let merge = create_merge_many(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let backports = vec![
        MergeBackport::new(&merge_settings, None),
        MergeBackport::new(&merge_settings2, None),
    ];
    let res = merge.merge_mr(&mr, &MERGER_IDENT, *AUTHOR_DATE, backports).unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    check_mr_commit(&origin_ctx,
                    BRANCH_NAME,
                    "Merge branch 'test-merge-backport' into test-merge");
    check_noop_merge(&origin_ctx, BRANCH_NAME);
    let parent = format!("{}~", BRANCH_NAME);
    check_mr_commit_message(&origin_ctx, &parent, "base: add a base commit\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME,
                    "Merge branch 'test-merge-backport2' into test-merge-backport");
    check_noop_merge(&origin_ctx, BACKPORT_BRANCH_NAME);
    let parent = format!("{}~", BACKPORT_BRANCH_NAME);
    check_mr_commit(&origin_ctx,
                    &parent,
                    "Merge topic 'topic-1' into test-merge-backport\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
    check_mr_commit(&origin_ctx,
                    BACKPORT_BRANCH_NAME2,
                    "Merge topic 'topic-1' into test-merge-backport2\n\
                     \n\
                     A simple message\n\
                     \n\
                     7189cf5 topic-1: make a change\n\
                     \n\
                     Acked-by: Ghostflow <ghostflow@example.com>\n\
                     Tested-by: other <other@example.com>\n\
                     Tested-by: maint <maint@example.com>\n\
                     Acked-by: other <other@example.com>\n\
                     Acked-by: maint <maint@example.com>\n\
                     Merge-request: !1\n");
}
