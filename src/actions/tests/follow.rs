// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_workarea::{CommitId, GitContext};

use actions::follow::*;
use actions::tests::utils::test_workspace_dir;

use std::path::Path;
use std::process::Command;

const BRANCH_NAME: &str = "master";
const BRANCH_REFNAME: &str = "refs/heads/master";

fn git_context(workspace_path: &Path) -> (GitContext, GitContext) {
    // Here, we create two clones of the current repository: one to act as the remote and one to be
    // the repository the action uses. The first is cloned from the source tree's directory while
    // the second is cloned from that first clone.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    // Ensure that there is a `master` branch on the origin.
    let originctx = GitContext::new(&origindir);
    let update_ref = originctx.git()
        .arg("update-ref")
        .arg(BRANCH_REFNAME)
        .arg("83d9837adde715c471d0e3292e53284098c748f4")
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("creating `master` failed: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("working clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    (originctx, GitContext::new(gitdir))
}

fn create_follow(git: &GitContext) -> Follow {
    Follow::new(git.clone(), BRANCH_NAME)
}

fn check_remote(git: &GitContext, commit: &CommitId) -> CommitId {
    let rev_parse = git.git()
        .arg("rev-parse")
        .arg(commit.as_str())
        .output()
        .unwrap();
    if !rev_parse.status.success() {
        panic!("failed to read the ref for the topic from the fork: {}",
               String::from_utf8_lossy(&rev_parse.stderr));
    }

    CommitId::new(String::from_utf8_lossy(&rev_parse.stdout).trim())
}

#[test]
// Creating a new ref should succeed.
fn test_follow_new() {
    let tempdir = test_workspace_dir("test_follow_new");
    let (origin_ctx, ctx) = git_context(tempdir.path());
    let follow = create_follow(&ctx);

    let cur_ref = check_remote(&origin_ctx, &CommitId::new(BRANCH_REFNAME));

    let name = "nightly";
    follow.update(name).unwrap();

    let refname = format!("refs/follow/{}/{}", BRANCH_NAME, name);
    let follow_ref = check_remote(&origin_ctx, &CommitId::new(refname));
    assert_eq!(follow_ref, cur_ref);
}

#[test]
// Updating a ref should succeed.
fn test_follow_update() {
    let tempdir = test_workspace_dir("test_follow_update");
    let (origin_ctx, ctx) = git_context(tempdir.path());
    let follow = create_follow(&ctx);

    let cur_ref = check_remote(&origin_ctx, &CommitId::new(BRANCH_REFNAME));
    let back_ref = check_remote(&ctx, &CommitId::new(format!("{}~", BRANCH_REFNAME)));

    // Push the branch back one commit.
    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(BRANCH_REFNAME)
        .arg(back_ref.as_str())
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    let name = "nightly";
    follow.update(name).unwrap();

    let refname = format!("refs/follow/{}/{}", BRANCH_NAME, name);
    let follow_ref = check_remote(&origin_ctx, &CommitId::new(&refname));
    assert_eq!(follow_ref, back_ref);

    // Put the branch back where it belongs.
    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(BRANCH_REFNAME)
        .arg(cur_ref.as_str())
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    follow.update(name).unwrap();

    let follow_update_ref = check_remote(&origin_ctx, &CommitId::new(refname));
    assert_eq!(follow_update_ref, cur_ref);
}

#[test]
// Custom ref namespace
fn test_follow_custom_ref_namespace() {
    let tempdir = test_workspace_dir("test_follow_custom_ref_namespace");
    let (origin_ctx, ctx) = git_context(tempdir.path());
    let mut follow = create_follow(&ctx);
    let namespace = "namespace";
    follow.ref_namespace(namespace);

    let cur_ref = check_remote(&origin_ctx, &CommitId::new(BRANCH_REFNAME));

    let name = "nightly";
    follow.update(name).unwrap();

    let refname = format!("refs/{}/{}/{}", namespace, BRANCH_NAME, name);
    let follow_ref = check_remote(&origin_ctx, &CommitId::new(refname));
    assert_eq!(follow_ref, cur_ref);
}

#[test]
// Following should force the push.
fn test_follow_update_force() {
    let tempdir = test_workspace_dir("test_follow_update_force");
    let (origin_ctx, ctx) = git_context(tempdir.path());
    let follow = create_follow(&ctx);

    let cur_ref = check_remote(&origin_ctx, &CommitId::new(BRANCH_REFNAME));
    let back_ref = check_remote(&ctx, &CommitId::new(format!("{}~", BRANCH_REFNAME)));

    let name = "nightly";
    follow.update(name).unwrap();

    let refname = format!("refs/follow/{}/{}", BRANCH_NAME, name);
    let follow_ref = check_remote(&origin_ctx, &CommitId::new(&refname));
    assert_eq!(follow_ref, cur_ref);

    // Push the branch back one commit.
    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(BRANCH_REFNAME)
        .arg(back_ref.as_str())
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    follow.update(name).unwrap();

    let follow_update_ref = check_remote(&origin_ctx, &CommitId::new(refname));
    assert_eq!(follow_update_ref, back_ref);
}
