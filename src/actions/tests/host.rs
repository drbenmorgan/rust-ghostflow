// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::Utc;
use crates::git_workarea::{CommitId, GitContext};
use crates::itertools::Itertools;

use host::*;

use std::cmp::min;
use std::collections::hash_map::HashMap;
use std::sync::{Arc, Mutex};

const LOCK_POISONED: &str = "mock service lock poisoned";

struct MockIssue {
    issue: Issue,
    comments: Vec<(Comment, Vec<Award>)>,
    reject_labels: bool,
}

struct MockMergeRequest {
    mr: MergeRequest,
    closes_issues: Vec<u64>,
    comments: Vec<(Comment, Vec<Award>)>,
    awards: Vec<Award>,
}

fn commit_status_from_pending(status: &PendingCommitStatus, author: User) -> CommitStatus {
    CommitStatus {
        state: status.state,
        author: author,
        refname: status.refname.map(ToString::to_string),
        name: status.name.to_string(),
        description: status.description.to_string(),
    }
}

#[derive(Default)]
struct State {
    step: usize,
    issue_comments: HashMap<u64, Vec<String>>,
    issue_labels: HashMap<u64, Vec<String>>,
    mr_comments: HashMap<u64, Vec<String>>,
    commit_comments: HashMap<String, Vec<String>>,
    commit_statuses: HashMap<String, Vec<CommitStatus>>,
    memberships: HashMap<String, Vec<Membership>>,
    hooks: HashMap<String, Vec<String>>,
}

impl State {
    fn reset(&mut self) {
        self.issue_comments.clear();
        self.issue_labels.clear();
        self.mr_comments.clear();
        self.commit_comments.clear();
        self.commit_statuses.clear();
        self.memberships.clear();
        self.hooks.clear();
    }

    fn remaining(&self) -> usize {
        0 +
            self.issue_comments.len() +
            self.issue_labels.len() +
            self.mr_comments.len() +
            self.commit_comments.len() +
            self.commit_statuses.len() +
            self.memberships.len() +
            self.hooks.len()
    }
}

pub struct MockService {
    projects: HashMap<String, Repo>,
    users: HashMap<String, User>,
    issues: HashMap<u64, Vec<MockIssue>>,
    merge_requests: HashMap<u64, Vec<MockMergeRequest>>,
    user: User,

    state: Mutex<State>,
}

impl MockService {
    fn new() -> Self {
        MockService {
            projects: HashMap::new(),
            users: HashMap::new(),
            issues: HashMap::new(),
            merge_requests: HashMap::new(),
            user: User {
                id: 100,
                handle: "root".to_string(),
                name: "root".to_string(),
                email: "admin@example.com".to_string(),
            },

            state: Default::default(),
        }
    }

    fn add_project(mut self, project: Repo) -> Self {
        self.projects.insert(project.name.clone(), project);

        self
    }

    fn add_user(mut self, user: User) -> Self {
        self.users.insert(user.name.clone(), user);

        self
    }

    fn add_issue(mut self, issue: MockIssue) -> Self {
        self.issues
            .entry(issue.issue.id)
            .or_insert_with(Vec::new)
            .push(issue);

        self
    }

    fn add_merge_request(mut self, mr: MockMergeRequest) -> Self {
        self.merge_requests
            .entry(mr.mr.id)
            .or_insert_with(Vec::new)
            .push(mr);

        self
    }

    fn find_user(&self, user: &str) -> Result<&User> {
        match self.users.get(user) {
            Some(p) => Ok(p),
            None => mock_err("invalid user"),
        }
    }

    fn find_user_by_id(&self, user_id: u64) -> Result<&User> {
        let user = self.users
            .iter()
            .find(|&(_, user)| user.id == user_id);

        match user {
            Some((_, p)) => Ok(p),
            None => mock_err("invalid user"),
        }
    }

    fn find_project(&self, project: &str) -> Result<&Repo> {
        match self.projects.get(project) {
            Some(p) => Ok(p),
            None => mock_err("invalid project"),
        }
    }

    fn find_project_by_id(&self, project_id: u64) -> Result<&Repo> {
        let project = self.projects
            .iter()
            .find(|&(_, project)| project.id == project_id);

        match project {
            Some((_, p)) => Ok(p),
            None => mock_err("invalid project"),
        }
    }

    fn add_membership(&self, project: &str, user: &User, level: AccessLevel) {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .memberships
            .entry(project.to_string())
            .or_insert_with(Vec::new)
            .push(Membership {
                user: user.clone(),
                access_level: level,
                expiration: None,
            });
    }

    fn find_memberships(&self, project: &str) -> Result<Vec<Membership>> {
        let state = self.state.lock().expect(LOCK_POISONED);
        match state.memberships.get(project) {
            Some(p) => Ok(p.clone()),
            None => mock_err("invalid project"),
        }
    }

    fn find_issue(&self, id: u64) -> Result<&MockIssue> {
        let state = self.state.lock().expect(LOCK_POISONED);
        match self.issues.get(&id) {
            Some(p) => {
                let issue_step = min(state.step, p.len() - 1);
                Ok(&p[issue_step])
            },
            None => mock_err("invalid issue"),
        }
    }

    fn find_merge_request(&self, id: u64) -> Result<&MockMergeRequest> {
        let state = self.state.lock().expect(LOCK_POISONED);
        match self.merge_requests.get(&id) {
            Some(p) => {
                let mr_step = min(state.step, p.len() - 1);
                Ok(&p[mr_step])
            },
            None => mock_err("invalid merge request"),
        }
    }

    pub fn test_service() -> Arc<Self> {
        let base_repo = Repo {
            name: "base".to_string(),
            url: "base".to_string(),
            id: 1,
            forked_from: None,
        };
        let fork_repo = Repo {
            name: "fork".to_string(),
            url: "fork".to_string(),
            id: 2,
            forked_from: None,
        };
        let self_repo = Repo {
            name: "self".to_string(),
            url: concat!(env!("CARGO_MANIFEST_DIR"), "/.git").to_string(),
            id: 3,
            forked_from: None,
        };
        let user_user = User {
            id: 0,
            handle: "user".to_string(),
            name: "user".to_string(),
            email: "user@example.com".to_string(),
        };
        let user_other = User {
            id: 1,
            handle: "other".to_string(),
            name: "other".to_string(),
            email: "other@example.com".to_string(),
        };
        let user_maint = User {
            id: 2,
            handle: "maint".to_string(),
            name: "maint".to_string(),
            email: "maint@example.com".to_string(),
        };
        let user_ignore = User {
            id: 3,
            handle: "ignore".to_string(),
            name: "ignore".to_string(),
            email: "ignore@example.com".to_string(),
        };

        let make_issue = |id: u64, desc: &str| {
            MockIssue {
                issue: Issue {
                    repo: base_repo.clone(),
                    id: id,
                    url: format!("issue{}", id),
                    description: desc.to_string(),
                    labels: Vec::new(),
                    milestone: None,
                    assignee: None,
                    reference: format!("#{}", id),
                },

                comments: Vec::new(),
                reject_labels: false,
            }
        };

        let issue1 = make_issue(1, "simple issue");
        let mut issue2 = make_issue(2, "simple issue that rejects labels");

        issue2.reject_labels = true;

        let make_mr = |old_commit: Option<&str>, commit: &str, id: u64, desc: &str| {
            MockMergeRequest {
                mr: MergeRequest {
                    source_repo: fork_repo.clone(),
                    source_branch: format!("topic-{}", id),
                    target_repo: base_repo.clone(),
                    target_branch: "master".to_string(),
                    id: id,
                    url: format!("mr{}", id),
                    work_in_progress: id % 2 == 0,
                    description: desc.to_string(),
                    old_commit: old_commit.map(|c| {
                        Commit {
                            repo: fork_repo.clone(),
                            id: CommitId::new(c),
                            refname: None,
                        }
                    }),
                    commit: Commit {
                        repo: fork_repo.clone(),
                        id: CommitId::new(commit),
                        refname: None,
                    },
                    author: user_user.clone(),

                    reference: format!("!{}", id),
                    remove_source_branch: false,
                },

                closes_issues: Vec::new(),
                comments: Vec::new(),
                awards: Vec::new(),
            }
        };
        let mr1 = |old_commit: Option<&str>, commit: &str| {
            make_mr(old_commit,
                    commit,
                    1,
                    "a simple topic\n```message\nA simple message\n```")
        };
        let mr2 = |old_commit: Option<&str>, commit: &str| {
            make_mr(old_commit, commit, 2, "a different, simple topic")
        };
        let mr3 = |old_commit: Option<&str>, commit: &str| {
            make_mr(old_commit,
                    commit,
                    3,
                    "a topic which conflicts with the update based\n```message\nA \
                     simple\nmultiline message\n```")
        };
        let mr4 = |old_commit: Option<&str>, commit: &str| {
            make_mr(old_commit, commit, 4, "a topic to test formatting")
        };
        let mr5 = |old_commit: Option<&str>, commit: &str| {
            make_mr(old_commit,
                    commit,
                    5,
                    "a topic with a commit which has already been merged")
        };
        let mr7 = |old_commit: Option<&str>, commit: &str| {
            make_mr(old_commit, commit, 7, "a topic which closes issues")
        };

        const TOPIC1: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";
        const TOPIC1_CONFLICT: &str = "755842266dcc5739c06d61433241f44b9306f24c";
        const TOPIC1_UPDATE: &str = "fe70f127605efb6032cacea0bd336428d67ed5a3";
        const TOPIC1_MISSED_UPDATE: &str = "1b340d2edcf19077ab3e27ddda7430a6612c2f62";

        const TOPIC2: &str = "f6f8de8c7c5f1a081b14f5a47c7798268f383222";

        const TOPIC3: &str = "7a28f8ea8759ff4f125ddbca825f976927a61310";

        const TOPIC4_NO_ATTRS: &str = "b9f9ba95441fc06f7e0c99032508b76fe5c8fa53";
        const TOPIC4_NO_CHANGE: &str = "7b5fcacdff4947cf501929dbc76d17654ca4343a";
        const TOPIC4_REWRITE: &str = "391992dbe37f2d4a3792430fe64874f744c4fbe6";
        const TOPIC4_REWRITE_MERGES: &str = "811418bb4004d1949d0057a832ee5e35ca01efca";
        const TOPIC4_REWRITE_FAIL: &str = "6137ad841169bdcd82dea88f7ff31ca8fdc4bc72";
        const TOPIC4_REWRITE_DELETE: &str = "d42d39e413d1944d4ad7394796c22d6aeb2818c4";
        const TOPIC4_REWRITE_UNTRACKED: &str = "d4159cc5fd9d1bfa8b8b8ad7a7ace2464f6a59f6";
        const TOPIC4_REWRITE_MANUAL: &str = "ab7200256e78d4ae8e0a4ac807d6e048623f1e10";
        const TOPIC4_EMPTY: &str = "f0a00a83187dc8c8f41675874f9e207290d9c24d";
        const TOPIC4_REWRITE_REMOVE: &str = "59764e7f7c8057c141a2f532437b5cf93d3ca257";
        const TOPIC4_REWRITE_MANUAL_REMOVE: &str = "67719629fc90b2d2a8350c10ebd789810b8c9479";
        const TOPIC4_TIMEOUT: &str = "b7d30c687a0272b36df44614de3438efa726278e";
        const TOPIC4_REWRITE_REPO_PARENT: &str = "7e09019e55e42bed1083bf0bea2240774cf66fe1";

        const TOPIC5: &str = "6d60de8086a5eaa0350a4a9d6c1897cfdc49f479";
        const TOPIC5_NO_HISTORY: &str = "1da7f0e3f3f5f6948835aa4f77a2ee2c2ae43854";

        const TOPIC7: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";

        let mut mr1_base = mr1(None, TOPIC1);
        let mr1_conflict = mr1(Some(TOPIC1), TOPIC1_CONFLICT);
        let mr1_update = mr1(Some(TOPIC1), TOPIC1_UPDATE);
        let mr1_missed_update = mr1(Some(TOPIC1_UPDATE), TOPIC1_MISSED_UPDATE);

        let mr2_base = mr2(None, TOPIC2);

        let mut mr3_base = mr3(None, TOPIC3);

        let mr4_no_attrs = mr4(None, TOPIC4_NO_ATTRS);
        let mr4_no_change = mr4(None, TOPIC4_NO_CHANGE);
        let mr4_rewrite = mr4(None, TOPIC4_REWRITE);
        let mr4_rewrite_merges = mr4(None, TOPIC4_REWRITE_MERGES);
        let mr4_rewrite_fail = mr4(None, TOPIC4_REWRITE_FAIL);
        let mr4_rewrite_delete = mr4(None, TOPIC4_REWRITE_DELETE);
        let mr4_rewrite_untracked = mr4(None, TOPIC4_REWRITE_UNTRACKED);
        let mr4_rewrite_manual = mr4(None, TOPIC4_REWRITE_MANUAL);
        let mr4_empty = mr4(None, TOPIC4_EMPTY);
        let mr4_rewrite_remove = mr4(None, TOPIC4_REWRITE_REMOVE);
        let mr4_rewrite_manual_remove = mr4(None, TOPIC4_REWRITE_MANUAL_REMOVE);
        let mr4_timeout = mr4(None, TOPIC4_TIMEOUT);
        let mr4_rewrite_repo_parent = mr4(None, TOPIC4_REWRITE_REPO_PARENT);

        let mr5_base = mr5(None, TOPIC5);
        let mr5_no_history = mr5(None, TOPIC5_NO_HISTORY);

        let mut mr7_base = mr7(None, TOPIC7);
        let mut mr7_bad_issue_id = mr7(None, TOPIC7);
        let mut mr7_bad_issue_label = mr7(None, TOPIC7);

        // Ignore unknown awards.
        mr1_base.awards.push(Award {
            name: "ignored".to_string(),
            author: user_other.clone(),
        });
        // Add trailers based on awards.
        mr1_base.awards.push(Award {
            name: "tada".to_string(),
            author: user_other.clone(),
        });
        // Test tone bits.
        mr1_base.awards.push(Award {
            name: "clap_tone2".to_string(),
            author: user_maint.clone(),
        });
        // Test user references.
        mr1_base.comments
            .push((Comment {
                       id: 0,
                       is_system: false,
                       is_branch_update: false,
                       created_at: Utc::now(),
                       author: user_other.clone(),
                       content: "Tested-by: me".to_string(),
                   },
                   Vec::new()));
        // Duplicate trailers should be collapsed.
        mr1_base.comments
            .push((Comment {
                       id: 1,
                       is_system: false,
                       is_branch_update: false,
                       created_at: Utc::now(),
                       author: user_other.clone(),
                       content: "Tested-by: @other".to_string(),
                   },
                   Vec::new()));
        mr1_base.comments.push((Comment {
                                    id: 2,
                                    is_system: false,
                                    is_branch_update: false,
                                    created_at: Utc::now(),
                                    author: user_other.clone(),
                                    content: "Tested-by: other <other@example.com>".to_string(),
                                },
                                Vec::new()));
        // +x comments should work.
        mr1_base.comments.push((Comment {
                                    id: 3,
                                    is_system: false,
                                    is_branch_update: false,
                                    created_at: Utc::now(),
                                    author: user_maint.clone(),
                                    content: "+3".to_string(),
                                },
                                Vec::new()));
        // Unrecognized user references should be ignored.
        mr1_base.comments
            .push((Comment {
                       id: 4,
                       is_system: false,
                       is_branch_update: false,
                       created_at: Utc::now(),
                       author: user_maint.clone(),
                       content: "Acked-by: @unknown".to_string(),
                   },
                   Vec::new()));
        // Allow policies to ignore certain users.
        mr1_base.comments
            .push((Comment {
                       id: 5,
                       is_system: false,
                       is_branch_update: false,
                       created_at: Utc::now(),
                       author: user_ignore.clone(),
                       content: "Acked-by: me".to_string(),
                   },
                   Vec::new()));
        // Policies may ignore trailers.
        mr1_base.comments
            .push((Comment {
                       id: 6,
                       is_system: false,
                       is_branch_update: false,
                       created_at: Utc::now(),
                       author: user_maint.clone(),
                       content: "Meaningless: trailer".to_string(),
                   },
                   Vec::new()));
        // System comments should be ignored.
        mr1_base.comments
            .push((Comment {
                       id: 4,
                       is_system: true,
                       is_branch_update: false,
                       created_at: Utc::now(),
                       author: user_maint.clone(),
                       content: "Rejected-by: @maint".to_string(),
                   },
                   Vec::new()));

        // Bogus awards should be ignored.
        mr3_base.awards.push(Award {
            name: "no_good".to_string(),
            author: user_maint.clone(),
        });

        // Test labeling closed issues.
        mr7_base.closes_issues.push(1);
        mr7_bad_issue_id.closes_issues.push(100);
        mr7_bad_issue_label.closes_issues.push(2);

        Arc::new(Self::new()
            .add_project(base_repo.clone())
            .add_project(fork_repo.clone())
            .add_project(self_repo.clone())
            .add_user(user_user.clone())
            .add_user(user_other.clone())
            .add_user(user_maint.clone())
            .add_user(user_ignore.clone())
            .add_issue(issue1)
            .add_issue(issue2)
            .add_merge_request(mr1_base)
            .add_merge_request(mr1_conflict)
            .add_merge_request(mr1_update)
            .add_merge_request(mr1_missed_update)
            .add_merge_request(mr2_base)
            .add_merge_request(mr3_base)
            .add_merge_request(mr4_no_attrs)
            .add_merge_request(mr4_no_change)
            .add_merge_request(mr4_rewrite)
            .add_merge_request(mr4_rewrite_merges)
            .add_merge_request(mr4_rewrite_fail)
            .add_merge_request(mr4_rewrite_delete)
            .add_merge_request(mr4_rewrite_untracked)
            .add_merge_request(mr4_rewrite_manual)
            .add_merge_request(mr4_empty)
            .add_merge_request(mr4_rewrite_remove)
            .add_merge_request(mr4_rewrite_manual_remove)
            .add_merge_request(mr4_timeout)
            .add_merge_request(mr4_rewrite_repo_parent)
            .add_merge_request(mr5_base)
            .add_merge_request(mr5_no_history)
            .add_merge_request(mr7_base)
            .add_merge_request(mr7_bad_issue_id)
            .add_merge_request(mr7_bad_issue_label))
    }

    pub fn step(&self, step: usize) {
        self.state.lock().expect(LOCK_POISONED).step = step;
    }

    pub fn reset_data(&self) {
        self.state.lock().expect(LOCK_POISONED).reset();
    }

    #[allow(dead_code)]
    pub fn issue_comments(&self, id: u64) -> Vec<String> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .issue_comments
            .remove(&id)
            .unwrap_or_else(Vec::new)
    }

    #[allow(dead_code)]
    pub fn issue_labels(&self, id: u64) -> Vec<String> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .issue_labels
            .remove(&id)
            .unwrap_or_else(Vec::new)
    }

    pub fn mr_comments(&self, id: u64) -> Vec<String> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .mr_comments
            .remove(&id)
            .unwrap_or_else(Vec::new)
    }

    #[allow(dead_code)]
    pub fn commit_comments(&self, commit: &str) -> Vec<String> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .commit_comments
            .remove(commit)
            .unwrap_or_else(Vec::new)
    }

    pub fn commit_statuses(&self, commit: &CommitId) -> Vec<CommitStatus> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .commit_statuses
            .remove(commit.as_str())
            .unwrap_or_else(Vec::new)
    }

    #[allow(dead_code)]
    pub fn memberships(&self, project: &str) -> Vec<Membership> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .memberships
            .remove(project)
            .unwrap_or_else(Vec::new)
    }

    #[allow(dead_code)]
    pub fn hooks(&self, project: &str) -> Vec<String> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .hooks
            .remove(project)
            .unwrap_or_else(Vec::new)
    }

    pub fn remaining_data(&self) -> usize {
        self.state.lock().expect(LOCK_POISONED).remaining()
    }
}

error_chain! {
    types {
        MockError, MockErrorKind, MockResultExt, MockResult;
    }
}

fn mock_err_inner<T, M: AsRef<str>>(msg: M) -> MockResult<T> {
    bail!(msg.as_ref())
}

fn mock_err<T, M: AsRef<str>>(msg: M) -> Result<T> {
    ResultExt::chain_err(mock_err_inner(msg), || ErrorKind::Host)
}

impl HostingService for MockService {
    fn service_user(&self) -> &User {
        &self.user
    }

    fn fetch_commit(&self, _: &GitContext, _: &Commit) -> Result<()> {
        Ok(())
    }

    fn fetch_mr(&self, _: &GitContext, _: &MergeRequest) -> Result<()> {
        Ok(())
    }

    fn add_member(&self, project: &str, user: &User, level: AccessLevel) -> Result<()> {
        let _ = self.find_project(project)?;

        Ok(self.add_membership(project, user, level))
    }

    fn members(&self, project: &str) -> Result<Vec<Membership>> {
        let _ = self.find_project(project)?;

        self.find_memberships(project)
    }

    fn add_hook(&self, project: &str, url: &str) -> Result<()> {
        let _ = self.find_project(project)?;

        let mut state = self.state.lock().expect(LOCK_POISONED);
        let hooks = state.hooks.entry(project.to_string()).or_insert_with(Vec::new);

        info!(target: "test/service",
              "adding a hook to {}",
              project);

        Ok(hooks.push(url.to_string()))
    }

    fn user(&self, user: &str) -> Result<User> {
        Ok(self.find_user(user)?.clone())
    }

    fn commit(&self, project: &str, commit: &CommitId) -> Result<Commit> {
        let project = self.find_project(project)?;

        Ok(Commit {
            repo: project.clone(),
            refname: None,
            // Just act as if the commit is a part of the project.
            id: commit.clone(),
        })
    }

    fn issue(&self, project: &str, id: u64) -> Result<Issue> {
        let _ = self.find_project(project)?;

        Ok(self.find_issue(id)?.issue.clone())
    }

    fn merge_request(&self, project: &str, id: u64) -> Result<MergeRequest> {
        let _ = self.find_project(project)?;

        Ok(self.find_merge_request(id)?.mr.clone())
    }

    fn repo(&self, project: &str) -> Result<Repo> {
        Ok(self.find_project(project)?.clone())
    }

    fn user_by_id(&self, user: u64) -> Result<User> {
        Ok(self.find_user_by_id(user)?.clone())
    }

    fn commit_by_id(&self, project: u64, commit: &CommitId) -> Result<Commit> {
        let project = self.find_project_by_id(project)?;

        Ok(Commit {
            repo: project.clone(),
            refname: None,
            // Just act as if the commit is a part of the project.
            id: commit.clone(),
        })
    }

    fn issue_by_id(&self, project: u64, id: u64) -> Result<Issue> {
        let _ = self.find_project_by_id(project)?;

        Ok(self.find_issue(id)?.issue.clone())
    }

    fn merge_request_by_id(&self, project: u64, id: u64) -> Result<MergeRequest> {
        let _ = self.find_project_by_id(project)?;

        Ok(self.find_merge_request(id)?.mr.clone())
    }

    fn repo_by_id(&self, project: u64) -> Result<Repo> {
        Ok(self.find_project_by_id(project)?.clone())
    }

    fn get_issue_comments(&self, issue: &Issue) -> Result<Vec<Comment>> {
        let issue = self.find_issue(issue.id)?;

        Ok(issue.comments
            .iter()
            .map(|issue_comment| issue_comment.0.clone())
            .collect())
    }

    fn post_issue_comment(&self, issue: &Issue, content: &str) -> Result<()> {
        let mut state = self.state.lock().expect(LOCK_POISONED);
        let comments = state.issue_comments.entry(issue.id).or_insert_with(Vec::new);

        info!(target: "test/service",
              "posting a comment to Issue#{}: {}",
              issue.id, content);

        Ok(comments.push(content.to_string()))
    }

    fn get_mr_comments(&self, mr: &MergeRequest) -> Result<Vec<Comment>> {
        let mr = self.find_merge_request(mr.id)?;

        Ok(mr.comments
            .iter()
            .map(|mr_comment| mr_comment.0.clone())
            .collect())
    }

    fn post_mr_comment(&self, mr: &MergeRequest, content: &str) -> Result<()> {
        let mut state = self.state.lock().expect(LOCK_POISONED);
        let comments = state.mr_comments.entry(mr.id).or_insert_with(Vec::new);

        info!(target: "test/service",
              "posting a comment to MR#{}: {}",
              mr.id, content);

        Ok(comments.push(content.to_string()))
    }

    fn post_commit_comment(&self, commit: &Commit, content: &str) -> Result<()> {
        let mut state = self.state.lock().expect(LOCK_POISONED);
        let comments = state.commit_comments
            .entry(commit.id.as_str().to_string())
            .or_insert_with(Vec::new);

        info!(target: "test/service",
              "posting a comment to commit {}: {}",
              commit.id, content);

        Ok(comments.push(content.to_string()))
    }

    fn get_commit_statuses(&self, commit: &Commit) -> Result<Vec<CommitStatus>> {
        Ok(self.state
            .lock()
            .expect(LOCK_POISONED)
            .commit_statuses
            .get(commit.id.as_str())
            .map_or_else(Vec::new, Clone::clone))
    }

    fn post_commit_status(&self, status: PendingCommitStatus) -> Result<()> {
        let mut state = self.state.lock().expect(LOCK_POISONED);
        let statuses = state.commit_statuses
            .entry(status.commit.id.as_str().to_string())
            .or_insert_with(Vec::new);

        info!(target: "test/service",
              "posting a {:?} status to commit {} ({:?}) by {}: {}",
              status.state, status.commit.id, status.refname,
              status.name, status.description);

        statuses.push(commit_status_from_pending(&status, self.user.clone()));

        Ok(())
    }

    fn get_mr_awards(&self, mr: &MergeRequest) -> Result<Vec<Award>> {
        let mr = self.find_merge_request(mr.id)?;

        Ok(mr.awards.clone())
    }

    fn get_mr_comment_awards(&self, mr: &MergeRequest, comment: &Comment) -> Result<Vec<Award>> {
        let mr = self.find_merge_request(mr.id)?;

        Ok(mr.comments[comment.id as usize].1.clone())
    }

    // Leave unimplemented; awards created by actions are dropped because of this.
    fn award_mr_comment(&self, _mr: &MergeRequest, _comment: &Comment, _award: &str) -> Result<()> {
        Ok(())
    }

    fn comment_award_name(&self) -> &str {
        "award"
    }

    fn issues_closed_by_mr(&self, mr: &MergeRequest) -> Result<Vec<Issue>> {
        let mr = self.find_merge_request(mr.id)?;

        mr.closes_issues
            .iter()
            .map(|&id| {
                self.find_issue(id)
                    .map(|issue| issue.issue.clone())
            })
            .collect()
    }

    fn add_issue_labels(&self, issue: &Issue, labels: &[&str]) -> Result<()> {
        let mock_issue = self.find_issue(issue.id)?;
        if mock_issue.reject_labels {
            bail!(ErrorKind::Host);
        }

        let mut state = self.state.lock().expect(LOCK_POISONED);
        let cur_labels = state.issue_labels.entry(issue.id).or_insert_with(Vec::new);

        info!(target: "test/service",
              "adding labels to Issue#{}: `{}`",
              issue.id, labels.iter().join("`, `"));

        Ok(cur_labels.extend(labels.iter()
            .map(ToString::to_string)))
    }
}
