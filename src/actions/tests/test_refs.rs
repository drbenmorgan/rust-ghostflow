// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_workarea::GitContext;

use actions::test::refs::*;
use actions::tests::host::MockService;
use actions::tests::utils::test_workspace_dir;
use host::{CommitStatusState, HostedProject, HostingService};

use std::fmt::Display;
use std::path::Path;
use std::process::Command;
use std::sync::Arc;

fn git_context(workspace_path: &Path) -> GitContext {
    // Here, we create two clones of the current repository: one to act as the remote and another
    // to be the repository the test action acts on. The first is cloned from the source tree's
    // directory while the second is cloned from that first clone. This sets up the `origin` remote
    // properly for the `test` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("working clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    GitContext::new(gitdir)
}

fn create_test(git: &GitContext, service: &Arc<MockService>) -> TestRefs {
    let project = HostedProject {
        name: "base".to_string(),
        service: Arc::clone(service) as Arc<HostingService>,
    };

    TestRefs::new(git.clone(), project)
}

// Creating the stage action structure should create the stage ref on the remote.
fn test_ref_exists<I>(ctx: &GitContext, namespace: &str, id: I) -> bool
    where I: Display,
{
    let refname = format!("refs/{}/{}", namespace, id);
    let remote = ctx.git()
        .arg("ls-remote")
        .arg("--exit-code")
        .arg("origin")
        .arg(&refname)
        .output()
        .unwrap()
        .status
        .success();
    let local = ctx.git()
        .arg("show-ref")
        .arg("--quiet")
        .arg("--verify")
        .arg(&refname)
        .status()
        .unwrap()
        .success();

    assert_eq!(remote,
               local,
               "The `refs/{}/...` refs should be the same on the local and remote repositories.",
               namespace);
    remote && local
}

#[test]
// Testing an MR should push a ref and make a comment.
fn test_test_mr() {
    let tempdir = test_workspace_dir("test_test_mr");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let test = create_test(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    test.test_mr(&mr).unwrap();

    assert!(test_ref_exists(&ctx, "test-topics", mr.id));

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "This topic has been pushed for testing.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "pushed for testing");
}

#[test]
// Testing an MR with a custom namespace should work.
fn test_test_mr_namespace() {
    let tempdir = test_workspace_dir("test_test_mr");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut test = create_test(&ctx, &service);
    let namespace = "test-topics-alternate";
    test.ref_namespace(namespace);

    let mr = service.merge_request("base", 1).unwrap();
    test.test_mr(&mr).unwrap();

    assert!(test_ref_exists(&ctx, namespace, mr.id));

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "This topic has been pushed for testing.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "pushed for testing");
}

#[test]
// Testing an MR should push a ref, but not make a comment if quiet.
fn test_test_mr_quiet() {
    let tempdir = test_workspace_dir("test_test_mr_quiet");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut test = create_test(&ctx, &service);
    test.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    test.test_mr(&mr).unwrap();

    assert!(test_ref_exists(&ctx, "test-topics", mr.id));

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "pushed for testing");
}

#[test]
// Untesting an MR should remove the ref from the remote.
fn test_untest_mr() {
    let tempdir = test_workspace_dir("test_untest_mr");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let test = create_test(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    test.test_mr(&mr).unwrap();
    test.untest_mr(&mr).unwrap();

    assert!(!test_ref_exists(&ctx, "test-topics", mr.id));

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "This topic has been pushed for testing.");

    assert_eq!(mr_commit_statuses.len(), 2);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "pushed for testing");

    let status = &mr_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "removed from testing");
}

#[test]
// Untesting an untested MR is fine.
fn test_untest_mr_untested() {
    let tempdir = test_workspace_dir("test_untest_mr_untested");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let test = create_test(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    test.untest_mr(&mr).unwrap();

    assert!(!test_ref_exists(&ctx, "test-topics", mr.id));

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Clearing the remote should clear all refs.
fn test_clear_all_tests() {
    let tempdir = test_workspace_dir("test_clear_all_tests");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let test = create_test(&ctx, &service);

    let mr = service.merge_request("base", 1).unwrap();
    let mr2 = service.merge_request("base", 2).unwrap();
    test.test_mr(&mr).unwrap();
    test.test_mr(&mr2).unwrap();
    test.clear_all_mrs().unwrap();

    assert!(!test_ref_exists(&ctx, "test-topics", mr.id));
    assert!(!test_ref_exists(&ctx, "test-topics", mr2.id));

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let mr2_commit_comments = service.mr_comments(mr2.id);
    let mr2_commit_statuses = service.commit_statuses(&mr2.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "This topic has been pushed for testing.");

    assert_eq!(mr2_commit_comments.len(), 1);
    assert_eq!(mr2_commit_comments[0],
               "This topic has been pushed for testing.");

    assert_eq!(mr_commit_statuses.len(), 2);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "pushed for testing");

    let status = &mr_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "removed from testing");

    assert_eq!(mr2_commit_statuses.len(), 2);

    let status = &mr2_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr2.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "pushed for testing");

    let status = &mr2_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr2.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-test");
    assert_eq!(status.description, "removed from testing");
}

#[test]
// Clearing the remote should delete invalid refs.
fn test_clear_all_invalid() {
    let tempdir = test_workspace_dir("test_clear_all_invalid");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let test = create_test(&ctx, &service);
    let id = "abc";

    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(format!("refs/test-topics/{}", id))
        .arg("HEAD")
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("update-ref failed: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    test.clear_all_mrs().unwrap();

    assert!(!test_ref_exists(&ctx, "test-topics", id));

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Clearing the remote should delete refs for which there is no merge request.
fn test_clear_all_no_mr() {
    let tempdir = test_workspace_dir("test_clear_all_no_mr");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let test = create_test(&ctx, &service);
    let id = 100;

    let update_ref = ctx.git()
        .arg("update-ref")
        .arg(format!("refs/test-topics/{}", id))
        .arg("HEAD")
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("update-ref failed: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    test.clear_all_mrs().unwrap();

    assert!(!test_ref_exists(&ctx, "test-topics", id));

    assert_eq!(service.remaining_data(), 0);
}
