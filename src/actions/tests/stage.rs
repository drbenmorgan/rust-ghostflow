// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::Utc;
use crates::git_workarea::{CommitId, GitContext, Identity};
use crates::git_topic_stage::Stager;

use actions::stage::*;
use actions::tests::host::MockService;
use actions::tests::utils::test_workspace_dir;
use host::{CommitStatusState, HostedProject, HostingService};

use std::path::Path;
use std::process::Command;
use std::sync::Arc;

const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";
const BASE_UPDATE: &str = "11bfbf44147015650afe6f95508ecfb1a77443cd";

lazy_static! {
    static ref STAGER_IDENT: Identity =
        Identity::new("ghostflow-stager", "ghostflow-stager@example.com");
}

fn git_context(workspace_path: &Path) -> GitContext {
    // Here, we create two clones of the current repository: one to act as the remote and another
    // to be the repository the stage acts on. The first is cloned from the source tree's directory
    // while the second is cloned from that first clone. This sets up the `origin` remote properly
    // for the `stage` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("working clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    GitContext::new(gitdir)
}

fn create_stage(git: &GitContext, base: &str, service: &Arc<MockService>) -> Stage {
    let stager = Stager::new(git, CommitId::new(base), STAGER_IDENT.clone());
    let project = HostedProject {
        name: "base".to_string(),
        service: Arc::clone(service) as Arc<HostingService>,
    };

    Stage::new(stager, base, project).unwrap()
}

#[test]
// Updating the base on an empty stage should succeed.
fn test_update_base_empty() {
    let tempdir = test_workspace_dir("test_update_base_empty");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let base_update = service.commit("base", &CommitId::new(BASE_UPDATE)).unwrap();
    stage.base_branch_update(&base_update, &STAGER_IDENT, Utc::now()).unwrap();

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Creating the stage action structure should create the stage ref on the remote.
fn test_stage_on_remote() {
    let tempdir = test_workspace_dir("test_stage_on_remote");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let _ = create_stage(&ctx, BASE, &service);

    let ls_remote = ctx.git()
        .arg("ls-remote")
        .arg("--exit-code")
        .arg("origin")
        .arg(format!("refs/stage/{}/head", BASE))
        .output()
        .unwrap();
    if !ls_remote.status.success() {
        panic!("ls-remote failed: {}",
               String::from_utf8_lossy(&ls_remote.stderr));
    }

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Staging a branch should and add a commit status and MR comment.
fn test_stage_branch() {
    let tempdir = test_workspace_dir("test_stage_branch");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Successfully staged.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");
}

#[test]
// Staging a branch should and add a commit status.
fn test_stage_branch_quiet() {
    let tempdir = test_workspace_dir("test_stage_branch");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");
}

#[test]
// Staging a branch which is already staged should add a comment.
fn test_stage_branch_staged() {
    let tempdir = test_workspace_dir("test_stage_branch_staged");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has already been staged; \
                ignoring the request to stage.");
}

#[test]
// Staging a branch which is already staged should not comment.
fn test_stage_branch_staged_quiet() {
    let tempdir = test_workspace_dir("test_stage_branch_staged");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Updating a branch should add a commit status and MR comment.
fn test_stage_branch_update() {
    let tempdir = test_workspace_dir("test_stage_branch_update");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(2);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let old_mr_commit_statuses = service.commit_statuses(&mr.old_commit.unwrap().id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Successfully staged.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");

    assert_eq!(old_mr_commit_statuses.len(), 1);

    let status = &old_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Updating a branch with a different topic name should act the same.
fn test_stage_branch_update_rename() {
    let tempdir = test_workspace_dir("test_stage_branch_update_rename");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(2);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request_named(&mr, "custom-name", &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let old_mr_commit_statuses = service.commit_statuses(&mr.old_commit.unwrap().id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Successfully staged.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");

    assert_eq!(old_mr_commit_statuses.len(), 1);

    let status = &old_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Updating a branch should add a commit status and MR comment.
fn test_stage_branch_update_quiet() {
    let tempdir = test_workspace_dir("test_stage_branch_update");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(2);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let old_mr_commit_statuses = service.commit_statuses(&mr.old_commit.unwrap().id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");

    assert_eq!(old_mr_commit_statuses.len(), 1);

    let status = &old_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Updating a branch where we missed an update should handle the topic as if it matched what the
// stage shows.
fn test_stage_branch_missed_update() {
    let tempdir = test_workspace_dir("test_stage_branch_update");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(3);

    let orig_mr = mr;
    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let orig_mr_commit_statuses = service.commit_statuses(&orig_mr.commit.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Successfully staged.");

    assert_eq!(orig_mr_commit_statuses.len(), 1);

    let status = &orig_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");
}

#[test]
// Updating a branch where we missed an update should handle the topic as if it matched what the
// stage shows.
fn test_stage_branch_missed_update_quiet() {
    let tempdir = test_workspace_dir("test_stage_branch_update");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(3);

    let orig_mr = mr;
    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let orig_mr_commit_statuses = service.commit_statuses(&orig_mr.commit.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(orig_mr_commit_statuses.len(), 1);

    let status = &orig_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");
}

#[test]
// Updating a branch which causes a conflict should add a commit status and MR comment.
fn test_stage_branch_update_conflict() {
    let tempdir = test_workspace_dir("test_stage_branch_update_conflict");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr2 = service.merge_request("base", 2).unwrap();
    stage.stage_merge_request(&mr2, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(1);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let mr2_commit_statuses = service.commit_statuses(&mr2.commit.id);
    let old_mr_commit_statuses = service.commit_statuses(&mr.old_commit.unwrap().id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request has been unstaged due to merge conflicts in the following \
                paths:\n\
                \n  - `base`");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "failed to merge: 1 conflicting paths");

    assert_eq!(mr2_commit_statuses.len(), 1);

    let status = &mr2_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr2.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");

    assert_eq!(old_mr_commit_statuses.len(), 1);

    let status = &old_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Updating a branch which causes a conflict should add a commit status and MR comment.
fn test_stage_branch_update_conflict_quiet() {
    let tempdir = test_workspace_dir("test_stage_branch_update_conflict");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr2 = service.merge_request("base", 2).unwrap();
    stage.stage_merge_request(&mr2, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();
    service.step(1);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);
    let mr2_commit_statuses = service.commit_statuses(&mr2.commit.id);
    let old_mr_commit_statuses = service.commit_statuses(&mr.old_commit.unwrap().id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request has been unstaged due to merge conflicts in the following \
                paths:\n\
                \n  - `base`");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "failed to merge: 1 conflicting paths");

    assert_eq!(mr2_commit_statuses.len(), 1);

    let status = &mr2_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr2.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");

    assert_eq!(old_mr_commit_statuses.len(), 1);

    let status = &old_mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, None);
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Staging a branch which conflicts should add a commit status an MR comment.
fn test_stage_branch_conflict() {
    let tempdir = test_workspace_dir("test_stage_branch_conflict");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE_UPDATE, &service);

    let mr = service.merge_request("base", 3).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request has been unstaged due to merge conflicts in the following \
                paths:\n\
                \n  - `base`");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "failed to merge: 1 conflicting paths");
}

#[test]
// Staging a branch which conflicts should add a commit status an MR comment.
fn test_stage_branch_conflict_quiet() {
    let tempdir = test_workspace_dir("test_stage_branch_conflict");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE_UPDATE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 3).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request has been unstaged due to merge conflicts in the following \
                paths:\n\
                \n  - `base`");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "failed to merge: 1 conflicting paths");
}

#[test]
// Updating the base should leave branches which are still OK alone.
fn test_update_base_ok() {
    let tempdir = test_workspace_dir("test_update_base_ok");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    let base_update = service.commit("base", &CommitId::new(BASE_UPDATE)).unwrap();
    stage.base_branch_update(&base_update, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "staged");
}

#[test]
// Updating the base should add a commit status and MR comment on MRs which now cause conflicts.
fn test_update_base_conflict() {
    let tempdir = test_workspace_dir("test_update_base_conflict");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 3).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    let base_update = service.commit("base", &CommitId::new(BASE_UPDATE)).unwrap();
    stage.base_branch_update(&base_update, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               format!("This merge request has been unstaged due to an update to the {} branch \
                        causing merge conflicts in the following paths:\n\
                        \n  - `base`",
                       BASE));

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "failed to merge: 1 conflicting paths");
}

#[test]
// Updating the base should add a commit status and MR comment on MRs which now cause conflicts.
fn test_update_base_conflict_quiet() {
    let tempdir = test_workspace_dir("test_update_base_conflict");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 3).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    let base_update = service.commit("base", &CommitId::new(BASE_UPDATE)).unwrap();
    stage.base_branch_update(&base_update, &STAGER_IDENT, Utc::now()).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               format!("This merge request has been unstaged due to an update to the {} branch \
                        causing merge conflicts in the following paths:\n\
                        \n  - `base`",
                       BASE));

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "failed to merge: 1 conflicting paths");
}

#[test]
// Unstaging due to an update should add a commit status and MR comment.
fn test_unstage_branch_update() {
    let tempdir = test_workspace_dir("test_unstage_branch_update");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    stage.unstage_update_merge_request(&mr, "as per policy").unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request has been unstaged as per policy.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Unstaging due to an update for a branch not on the stage should be silent.
fn test_unstage_branch_update_missing() {
    let tempdir = test_workspace_dir("test_unstage_branch_update_missing");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();

    service.reset_data();

    stage.unstage_update_merge_request(&mr, "as per policy").unwrap();

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Unstaging should add a commit status and MR comment.
fn test_unstage_branch() {
    let tempdir = test_workspace_dir("test_unstage_branch");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    stage.unstage_merge_request(&mr).unwrap();

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This merge request has been unstaged upon request.");

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Unstaging should add a commit status and MR comment.
fn test_unstage_branch_quiet() {
    let tempdir = test_workspace_dir("test_unstage_branch");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    stage.unstage_merge_request(&mr).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description, "unstaged");
}

#[test]
// Unstaging a branch which is not staged should add an MR comment, but no statues.
fn test_unstage_branch_not_staged() {
    let tempdir = test_workspace_dir("test_unstage_branch_not_staged");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.unstage_merge_request(&mr).unwrap();

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "Failed to find this merge request on the stage; \
                ignoring the request to unstage it.");
}

#[test]
// Unstaging a branch which is not staged should add an MR comment, but no statues.
fn test_unstage_branch_not_staged_quiet() {
    let tempdir = test_workspace_dir("test_unstage_branch_not_staged");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.unstage_merge_request(&mr).unwrap();

    assert_eq!(service.remaining_data(), 0);
}

#[test]
// Tagging a stage should comment on staged branches and add a comment on the staged commit.
fn test_tag_stage() {
    let tempdir = test_workspace_dir("test_tag_stage");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    let now = Utc::now();
    stage.tag_stage("nightly", "%Y/%m/%d", TagStagePolicy::ClearStage).unwrap();

    assert_eq!(stage.stager().topics().len(), 0);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               format!("This merge request has been pushed for nightly testing as of \
                        {} and unstaged.",
                       now.format("%Y/%m/%d")));

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));;
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description,
               format!("staged for nightly testing refs/stage/{}/{}",
                       BASE,
                       Utc::now().format("nightly/%Y/%m/%d")));
}

#[test]
// Tagging a stage should comment on staged branches and add a comment on the staged commit.
fn test_tag_stage_keep_topics() {
    let tempdir = test_workspace_dir("test_tag_stage");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    stage.tag_stage("nightly", "%Y/%m/%d", TagStagePolicy::KeepTopics).unwrap();

    assert_eq!(stage.stager().topics().len(), 1);

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));;
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description,
               format!("staged for nightly testing refs/stage/{}/{}",
                       BASE,
                       Utc::now().format("nightly/%Y/%m/%d")));
}

#[test]
// Tagging a stage should comment on staged branches and add a comment on the staged commit.
fn test_tag_stage_quiet() {
    let tempdir = test_workspace_dir("test_tag_stage");
    let ctx = git_context(tempdir.path());
    let service = MockService::test_service();
    let mut stage = create_stage(&ctx, BASE, &service);
    stage.quiet();

    let mr = service.merge_request("base", 1).unwrap();
    stage.stage_merge_request(&mr, &STAGER_IDENT, Utc::now()).unwrap();

    service.reset_data();

    let now = Utc::now();
    stage.tag_stage("nightly", "%Y/%m/%d", TagStagePolicy::ClearStage).unwrap();

    assert_eq!(stage.stager().topics().len(), 0);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               format!("This merge request has been pushed for nightly testing as of \
                        {} and unstaged.",
                       now.format("%Y/%m/%d")));

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));;
    assert_eq!(status.name, "ghostflow-stager");
    assert_eq!(status.description,
               format!("staged for nightly testing refs/stage/{}/{}",
                       BASE,
                       Utc::now().format("nightly/%Y/%m/%d")));
}
