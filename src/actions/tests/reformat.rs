// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_checks::Commit;
use crates::git_workarea::{CommitId, GitContext};

use actions::reformat::*;
use actions::tests::host::MockService;
use actions::tests::utils::test_workspace_dir;
use host::{HostedProject, HostingService, MergeRequest};

use std::path::Path;
use std::process::Command;
use std::sync::Arc;
use std::time::Duration;

const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";
const UNRELATED_COMMIT: &str = "1da7f0e3f3f5f6948835aa4f77a2ee2c2ae43854";

fn git_context(workspace_path: &Path) -> (GitContext, GitContext) {
    // Here, we create three clones of the current repository: one to act as the remote, another
    // to be the repository the reformatting acts on, and a third for the fork. The first is cloned
    // from the source tree's directory while the second is cloned from that first clone. The fork
    // is also cloned from the first repository. This sets up the `origin` remote properly for the
    // `reformat` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("origin clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    let forkdir = workspace_path.join("fork");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(&origindir)
        .arg(&forkdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("fork clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!("working clone failed: {}",
               String::from_utf8_lossy(&clone.stderr));
    }
    let gitctx = GitContext::new(gitdir);

    let remote_add = gitctx.git()
        .arg("remote")
        .arg("add")
        .arg("fork")
        .arg(&forkdir)
        .output()
        .unwrap();
    if !remote_add.status.success() {
        panic!("adding the fork as a remote failed: {}",
               String::from_utf8_lossy(&remote_add.stderr));
    }

    (gitctx, GitContext::new(forkdir))
}

fn create_formatter(kind: &str) -> Result<Formatter> {
    Formatter::new(kind,
                   format!("{}/test/format.{}", env!("CARGO_MANIFEST_DIR"), kind))
}

fn create_reformat(git: &GitContext, service: &Arc<MockService>) -> Reformat {
    let project = HostedProject {
        name: "base".to_string(),
        service: Arc::clone(service) as Arc<HostingService>,
    };
    let mut reformat = Reformat::new(git.clone(), project);

    let mut simple_formatter = create_formatter("simple").unwrap();
    simple_formatter.add_config_files(&["format-config"]);

    let untracked_formatter = create_formatter("untracked").unwrap();
    let delete_formatter = create_formatter("delete").unwrap();
    let mut timeout_formatter = create_formatter("timeout").unwrap();
    timeout_formatter.with_timeout(Duration::from_secs(1));

    let formatters = vec![
        simple_formatter,
        untracked_formatter,
        delete_formatter,
        timeout_formatter,
    ];

    reformat.add_formatters(formatters);

    reformat
}

fn prepare_fork(git: &GitContext, mr: &MergeRequest) {
    let update_ref = git.git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", mr.source_branch))
        .arg(mr.commit.id.as_str())
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }
}

fn check_fork(git: &GitContext, commit: &CommitId) -> CommitId {
    let rev_parse = git.git()
        .arg("rev-parse")
        .arg(commit.as_str())
        .output()
        .unwrap();
    if !rev_parse.status.success() {
        panic!("failed to read the ref for the topic from the fork: {}",
               String::from_utf8_lossy(&rev_parse.stderr));
    }

    CommitId::new(String::from_utf8_lossy(&rev_parse.stdout).trim())
}

fn check_file_contents(git: &GitContext, commit: &CommitId, file: &str, expected: &str) {
    let ls_tree = git.git()
        .arg("ls-tree")
        .arg(commit.as_str())
        .arg(file)
        .output()
        .unwrap();
    if !ls_tree.status.success() {
        panic!("failed to run ls-tree on commit {} for the file {}: {}",
               commit,
               file,
               String::from_utf8_lossy(&ls_tree.stderr));
    }
    let ls_tree_output = String::from_utf8_lossy(&ls_tree.stdout);
    let blob = ls_tree_output.split_whitespace().nth(2).unwrap();

    let cat_file = git.git()
        .arg("cat-file")
        .arg("blob")
        .arg(blob)
        .output()
        .unwrap();
    if !cat_file.status.success() {
        panic!("failed to get the contents of the blob {}: {}",
               blob,
               String::from_utf8_lossy(&cat_file.stderr));
    }

    assert_eq!(String::from_utf8_lossy(&cat_file.stdout), expected);
}

fn file_exists(git: &GitContext, commit: &CommitId, file: &str) -> bool {
    let ls_tree = git.git()
        .arg("ls-tree")
        .arg(commit.as_str())
        .arg(file)
        .output()
        .unwrap();
    if !ls_tree.status.success() {
        panic!("failed to run ls-tree on commit {} for the file {}: {}",
               commit,
               file,
               String::from_utf8_lossy(&ls_tree.stderr));
    }

    // The file exists if there is any output.
    !ls_tree.stdout.is_empty()
}

#[test]
// Empty-kind formatters are not allowed.
fn test_reformat_format_empty_kind() {
    let err = Formatter::new("",
                             concat!(env!("CARGO_MANIFEST_DIR"), "/test/format.simple"))
        .unwrap_err();

    if let ErrorKind::EmptyKind = *err.kind() {
        // Expected error.
    } else {
        panic!("unexpected error: {:?}", err);
    }
}

#[test]
// Formatters which do not exist are not allowed.
fn test_reformat_format_no_exist() {
    let err = create_formatter("no-exist").unwrap_err();

    if let ErrorKind::MissingFormatter(_) = *err.kind() {
        // Expected error.
    } else {
        panic!("unexpected error: {:?}", err);
    }
}

#[test]
// Reformatting an MR where no formatters are necessary should change nothing.
fn test_reformat_no_attrs() {
    let tempdir = test_workspace_dir("test_reformat_no_attrs");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Reformatting an MR where everything is OK should change nothing.
fn test_reformat_no_change() {
    let tempdir = test_workspace_dir("test_reformat_no_change");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(1);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Reformatting an MR where everything is OK up to a commit should change nothing up to that
// commit. The commit should change though.
fn test_reformat_pass_simple() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(2);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    let cur_parent = check_fork(&fork_ctx,
                                &CommitId::new(format!("refs/heads/{}~", mr.source_branch)));
    let old_parent = check_fork(&fork_ctx, &CommitId::new(format!("{}~", mr.commit.id)));
    assert_eq!(cur_parent, old_parent);

    check_file_contents(&fork_ctx,
                        &cur_head,
                        "bad.simple.txt",
                        "This is a well-formatted file.\n");

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.");
}

#[test]
// Reformatting an MR where everything is OK up to a commit should change nothing up to that
// commit. Commits which manually reformat a repository as a fixup commit should disappear from the
// history.
fn test_reformat_pass_simple_remove_manual() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple_remove_manual");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(7);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    // A commit should have been removed from the history because it was a manual reformatting.
    let cur_parent = check_fork(&fork_ctx,
                                &CommitId::new(format!("refs/heads/{}~", mr.source_branch)));
    let old_grandparent = check_fork(&fork_ctx, &CommitId::new(format!("{}~~", mr.commit.id)));
    assert_eq!(cur_parent, old_grandparent);

    check_file_contents(&fork_ctx,
                        &cur_head,
                        "bad.simple.txt",
                        "This is a well-formatted file.\n");

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.\n\
                \n\
                The following commits were empty after reformatting and removed from the history: \
                ab7200256e78d4ae8e0a4ac807d6e048623f1e10.");
}

#[test]
// Reformatting an MR where everything is OK up to a commit should change nothing up to that
// commit. Empty commits should not be dropped.
fn test_reformat_pass_simple_keep_empty() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple_keep_empty");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(8);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Reformatting a topic which ends up deleting a file that gets reformatted should work as
// expected. The topic should change (since the history changed), but the tree should be the same.
fn test_reformat_pass_simple_remove_formatted_file() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple_remove_formatted_file");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(9);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    let cur_tree = check_fork(&fork_ctx, &CommitId::new(format!("{}^{{tree}}", cur_head)));
    let old_tree = check_fork(&fork_ctx,
                              &CommitId::new(format!("{}^{{tree}}", mr.commit.id)));
    assert_eq!(cur_tree, old_tree);

    assert!(!file_exists(&fork_ctx, &cur_head, "bad.simple.txt"));

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.");
}

#[test]
// Reformatting a topic which contains a commit which manually fixes formatting and removes
// another file should keep the commit which is now just a removal of a file.
fn test_reformat_pass_simple_keep_manual_extra() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple_keep_manual_extra");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(10);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    check_file_contents(&fork_ctx,
                        &cur_head,
                        "bad.simple.txt",
                        "This is a well-formatted file.\n");
    assert!(!file_exists(&fork_ctx, &cur_head, "ok.txt"));

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.");
}

#[test]
// Reformatting an MR with merges should succeed.
fn test_reformat_pass_merges() {
    let tempdir = test_workspace_dir("test_reformat_pass_merges");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(3);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    let old_commit = Commit::new(&ctx, &mr.commit.id).unwrap();
    let new_commit = Commit::new(&ctx, &cur_head).unwrap();

    assert_eq!(old_commit.parents.len(), 2);
    assert_eq!(new_commit.parents.len(), 2);

    check_file_contents(&fork_ctx,
                        &cur_head,
                        "bad.simple.txt",
                        "This is a well-formatted file.\n");

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.");
}

#[test]
// Reformatting an MR where a formatter fails should fail.
fn test_reformat_fail_reformat() {
    let tempdir = test_workspace_dir("test_reformat_fail_reformat");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(4);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let err = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap_err();

    if let ErrorKind::ReformatFailed(ref commit, ref files) = *err.kind() {
        assert_eq!(commit, &mr.commit.id);
        assert_eq!(files.len(), 2);
        assert_eq!(files[0], "bad.simple.txt");
        assert_eq!(files[1], "fail.simple.txt");
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "Failed to format the following files in \
                6137ad841169bdcd82dea88f7ff31ca8fdc4bc72:\n\n  - `bad.simple.txt`\n  - \
                `fail.simple.txt`");
}

#[test]
// Reformatting an MR which deletes files should fail.
fn test_reformat_fail_deleted() {
    let tempdir = test_workspace_dir("test_reformat_fail_deleted");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(5);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let err = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap_err();

    if let ErrorKind::DeletedFiles(ref commit, ref files) = *err.kind() {
        assert_eq!(commit, &mr.commit.id);
        assert_eq!(files.len(), 1);
        assert_eq!(files[0], "bad.delete.txt");
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "The following paths were deleted while formatting \
                d42d39e413d1944d4ad7394796c22d6aeb2818c4:\n\n  - `bad.delete.txt`");
}

#[test]
// Reformatting an MR which drops untracked files should fail.
fn test_reformat_fail_untracked() {
    let tempdir = test_workspace_dir("test_reformat_fail_untracked");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(6);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let err = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap_err();

    if let ErrorKind::UntrackedFiles(ref commit, ref files) = *err.kind() {
        assert_eq!(commit, &mr.commit.id);
        assert_eq!(files.len(), 1);
        assert_eq!(files[0], "untracked");
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "The following untracked paths were created while formatting \
                d4159cc5fd9d1bfa8b8b8ad7a7ace2464f6a59f6:\n\n  - `untracked`");
}

#[test]
// Reformatting an MR which causes reformat timeouts should fail.
fn test_reformat_fail_timeout() {
    let tempdir = test_workspace_dir("test_reformat_fail_timeout");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(11);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let err = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap_err();

    if let ErrorKind::ReformatFailed(ref commit, ref files) = *err.kind() {
        assert_eq!(commit, &mr.commit.id);
        assert_eq!(files.len(), 1);
        assert_eq!(files[0], "timeout.txt");
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "Failed to format the following files in \
                b7d30c687a0272b36df44614de3438efa726278e:\n\n  - `timeout.txt`");
}

#[test]
// Reformatting an MR should fail to push if the remote has updated since we started formatting.
fn test_reformat_fail_push() {
    let tempdir = test_workspace_dir("test_reformat_fail_push");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(2);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    // Act as if the fork had a push while the reformat occurred.
    let update_ref = fork_ctx.git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", mr.source_branch))
        .arg(UNRELATED_COMMIT)
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    let err = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap_err();

    if let ErrorKind::PushFailed(ref msg) = *err.kind() {
        assert!(msg.starts_with("failed to push the reformatted branch to fork: "));
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Failed to push the reformatted branch.");
}

#[test]
// Pushing would fail, but we don't push because the branch didn't change.
fn test_reformat_fail_push_no_rewrite() {
    let tempdir = test_workspace_dir("test_reformat_fail_push_no_rewrite");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    // Act as if the fork had a push while the reformat occurred.
    let update_ref = fork_ctx.git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", mr.source_branch))
        .arg(UNRELATED_COMMIT)
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Reformatting root commits should work.
fn test_reformat_root_commit() {
    let tempdir = test_workspace_dir("test_reformat_root_commit");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(1);

    let mr = service.merge_request("base", 5).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_mr(&CommitId::new(BASE), &mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Whole-repo formatting should work.
fn test_reformat_repo_no_change() {
    let tempdir = test_workspace_dir("test_reformat_repo_no_change");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(1);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_repo(&mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Reformatting a repo where everything is OK up to a commit should change nothing up to that
// commit. The commit should change though.
fn test_reformat_repo_pass_simple() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(2);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_repo(&mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    let cur_parent = check_fork(&fork_ctx,
                                &CommitId::new(format!("refs/heads/{}~", mr.source_branch)));
    let old_parent = check_fork(&fork_ctx, &CommitId::new(format!("{}~", mr.commit.id)));
    assert_eq!(cur_parent, old_parent);

    check_file_contents(&fork_ctx,
                        &cur_head,
                        "bad.simple.txt",
                        "This is a well-formatted file.\n");

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.");
}

#[test]
// Reformatting a repo should only change the HEAD commit.
fn test_reformat_repo_pass_simple_only_head() {
    let tempdir = test_workspace_dir("test_reformat_pass_simple_only_head");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(12);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_repo(&mr).unwrap();
    assert_ne!(cur_head, mr.commit.id);

    let cur_parent = check_fork(&fork_ctx,
                                &CommitId::new(format!("refs/heads/{}~", mr.source_branch)));
    let old_parent = check_fork(&fork_ctx, &CommitId::new(format!("{}~", mr.commit.id)));
    assert_eq!(cur_parent, old_parent);

    check_file_contents(&fork_ctx,
                        &cur_head,
                        "bad.simple.txt",
                        "This is a well-formatted file.\n");
    check_file_contents(&fork_ctx,
                        &CommitId::new(format!("{}~", cur_head)),
                        "bad.simple.txt",
                        "This is a poorly-formatted file.\n");

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic has been reformatted and pushed; please fetch from the source \
                repository and reset your local branch to continue with further development on \
                the reformatted commits.");
}

#[test]
// Reformatting a repo should fail to push if the remote has updated since we started formatting.
fn test_reformat_repo_fail_push() {
    let tempdir = test_workspace_dir("test_reformat_repo_fail_push");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(2);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    // Act as if the fork had a push while the reformat occurred.
    let update_ref = fork_ctx.git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", mr.source_branch))
        .arg(UNRELATED_COMMIT)
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    let err = reformat.reformat_repo(&mr).unwrap_err();

    if let ErrorKind::PushFailed(ref msg) = *err.kind() {
        assert!(msg.starts_with("failed to push the reformatted branch to fork: "));
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Failed to push the reformatted branch.");
}

#[test]
// Pushing would fail, but we don't push because the branch didn't change.
fn test_reformat_repo_fail_push_no_rewrite() {
    let tempdir = test_workspace_dir("test_reformat_repo_fail_push_no_rewrite");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    // Act as if the fork had a push while the reformat occurred.
    let update_ref = fork_ctx.git()
        .arg("update-ref")
        .arg(format!("refs/heads/{}", mr.source_branch))
        .arg(UNRELATED_COMMIT)
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!("failed to update the ref for the fork: {}",
               String::from_utf8_lossy(&update_ref.stderr));
    }

    let cur_head = reformat.reformat_repo(&mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}

#[test]
// Whole-repo formatting should reject merge commits.
fn test_reformat_repo_merge() {
    let tempdir = test_workspace_dir("test_reformat_repo_merge");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(3);

    let mr = service.merge_request("base", 4).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let err = reformat.reformat_repo(&mr).unwrap_err();

    if let ErrorKind::MergeCommit = *err.kind() {
    } else {
        panic!("unexpected error: {:?}", err);
    }

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "The repository cannot be reformatted using a merge commit. Please use a non-merge \
                commit.");
}

#[test]
// Whole-repo formatting should work on root commits.
fn test_reformat_repo_root() {
    let tempdir = test_workspace_dir("test_reformat_repo_root");
    let (ctx, fork_ctx) = git_context(tempdir.path());
    let service = MockService::test_service();
    let reformat = create_reformat(&ctx, &service);

    service.step(1);

    let mr = service.merge_request("base", 5).unwrap();
    prepare_fork(&fork_ctx, &mr);

    let cur_head = reformat.reformat_repo(&mr).unwrap();
    assert_eq!(cur_head, mr.commit.id);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0],
               "This topic is clean and required no reformatting.");
}
