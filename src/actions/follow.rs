// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `follow` action.
//!
//! This action pushes into a ref namespace on the remote that tracks a branch at a coarser
//! interval than every commit. Its intended use case is to keep a stable reference across a longer
//! timespan so that asynchronous external tools all use the same commit.

use crates::git_workarea::{self, GitContext};

error_chain! {
    links {
        GitWorkarea(git_workarea::Error, git_workarea::ErrorKind)
            #[doc = "Errors from the git-workarea crate."];
    }

    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

#[derive(Debug)]
/// Implementation of the `follow` action.
pub struct Follow {
    /// The context to use for fetching data refs.
    ctx: GitContext,
    /// The branch to follow.
    branch: String,
    /// The reference namespace to use for data.
    ref_namespace: String,
}

impl Follow {
    /// Create a new follow action.
    pub fn new<B>(ctx: GitContext, branch: B) -> Self
        where B: ToString,
    {
        Self {
            ctx: ctx,
            branch: branch.to_string(),
            ref_namespace: "follow".to_string(),
        }
    }

    /// Use the given ref namespace for follow refs.
    ///
    /// By default, `follow` is used to push under `refs/follow/`.
    pub fn ref_namespace<R>(&mut self, ref_namespace: R) -> &mut Self
        where R: ToString,
    {
        self.ref_namespace = ref_namespace.to_string();
        self
    }

    /// Update the remote ref using the given name.
    pub fn update<N>(&self, name: N) -> Result<()>
        where N: AsRef<str>,
    {
        self.update_impl(name.as_ref())
    }

    /// Non-generic version of `update`.
    fn update_impl(&self, name: &str) -> Result<()> {
        info!(target: "ghostflow/follow",
              "following {} into {}",
              self.branch,
              name);

        let refname = format!("refs/{}/{}/{}", self.ref_namespace, self.branch, name);

        let push = self.ctx
            .git()
            .arg("push")
            .arg("--atomic")
            .arg("--porcelain")
            .arg("origin")
            .arg(format!("+refs/heads/{}:{}", self.branch, refname))
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !push.status.success() {
            bail!(ErrorKind::Git(format!("failed to push {} into {}: {}",
                                         self.branch,
                                         refname,
                                         String::from_utf8_lossy(&push.stderr))))
        }

        Ok(())
    }
}
