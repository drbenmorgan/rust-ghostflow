// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `data` action.
//!
//! This action fetches data objects pushed to a repository under `refs/data/` (or another
//! namespace) and pushes them using `rsync` to multiple destinations.

use crates::crypto::digest::Digest;
use crates::crypto::md5::Md5;
use crates::crypto::sha2::{Sha256, Sha512};
use crates::git_workarea::{self, GitContext};
use crates::itertools::Itertools;
use crates::tempdir::TempDir;

use host::Repo;

use std::fs::{self, OpenOptions};
use std::io::Write;
use std::os::unix::fs::OpenOptionsExt;
use std::process::Command;

error_chain! {
    links {
        GitWorkarea(git_workarea::Error, git_workarea::ErrorKind)
            #[doc = "Errors from the git-workarea crate."];
    }

    errors {
        /// No destinations were configured for the data.
        NoDestinations {
            display("no destinations configured")
        }

        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }

        /// An error occurred when executing rsync commands.
        Rsync(msg: String) {
            display("rsync error: {}", msg)
        }
    }
}

#[derive(Debug)]
/// Implementation of the `data` action.
pub struct Data {
    /// The context to use for fetching data refs.
    ctx: GitContext,
    /// The `rsync` destinations to upload data to.
    destinations: Vec<String>,
    /// The reference namespace to use for data.
    ref_namespace: String,
    /// Whether to keep refs on remotes or not.
    keep_refs: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// The result of the data action.
pub enum DataActionResult {
    /// No data was found in the repository.
    NoData,
    /// Data was successfully pushed to the destinations.
    DataPushed,
}

impl Data {
    /// Create a new data action.
    pub fn new(ctx: GitContext) -> Self {
        Self {
            ctx: ctx,
            destinations: Vec::new(),
            ref_namespace: "data".to_string(),
            keep_refs: false,
        }
    }

    /// Add a destination for the data.
    pub fn add_destination<D>(&mut self, destination: D) -> &mut Self
        where D: ToString,
    {
        self.destinations.push(destination.to_string());
        self
    }

    /// Preserve data refs found on remote servers.
    ///
    /// By default, data which has been successfully fetched will be deleted from the given remote.
    pub fn keep_refs(&mut self) -> &mut Self {
        self.keep_refs = true;
        self
    }

    /// Use the given ref namespace for data objects.
    ///
    /// By default, `data` is used to look under `refs/data/`.
    pub fn ref_namespace<R>(&mut self, ref_namespace: R) -> &mut Self
        where R: ToString,
    {
        self.ref_namespace = ref_namespace.to_string();
        self
    }

    /// Fetch all data from a repository and mirror it to the destinations.
    pub fn fetch_data(&self, repo: &Repo) -> Result<DataActionResult> {
        info!(target: "ghostflow/data",
              "checking for data in {}",
              repo.url);

        let data_ref_ns = format!("refs/{}/", self.ref_namespace);
        let data_ref_glob = format!("{}*", data_ref_ns);

        // Find all of the data refs in the repository.
        let ls_remote = self.ctx
            .git()
            .arg("ls-remote")
            .arg("--quiet")
            .arg("--exit-code")
            .arg(&repo.url)
            .arg(&data_ref_glob)
            .output()
            .chain_err(|| "failed to construct ls-remote command")?;
        if !ls_remote.status.success() {
            if let Some(2) = ls_remote.status.code() {
                return Ok(DataActionResult::NoData);
            } else {
                bail!(ErrorKind::Git(format!("failed to list remote refs in {}: {}",
                                             repo.url,
                                             String::from_utf8_lossy(&ls_remote.stderr))));
            }
        }
        let remote_data_refs = String::from_utf8_lossy(&ls_remote.stdout);
        // Get the names of all the refs.
        let data_refs = remote_data_refs.lines()
            // Extract just the refname from the output.
            .filter_map(|line| line.splitn(2, '\t').nth(1))
            .collect::<Vec<_>>();

        info!(target: "ghostflow/data",
              "fetching data from {}",
              repo.url);

        // Fetch the data from the local repository.
        self.ctx.force_fetch_into(&repo.url, &data_ref_glob, &data_ref_glob)?;

        if self.destinations.is_empty() {
            bail!(ErrorKind::NoDestinations);
        }

        if !self.keep_refs {
            // Delete the refs from the remote server.
            let delete_refs = self.ctx
                .git()
                .arg("push")
                .arg("--atomic")
                .arg("--porcelain")
                .arg("--delete")
                .arg(&repo.url)
                .args(&data_refs)
                .output()
                .chain_err(|| "failed to construct push command")?;
            if !delete_refs.status.success() {
                bail!(ErrorKind::Git(format!("failed to delete data from {}: {}",
                                             repo.url,
                                             String::from_utf8_lossy(&delete_refs.stderr))));
            }
        }

        // Create a temporary directory to store data objects to push to the destinations.
        let tempdir = TempDir::new_in(self.ctx.gitdir(), "data-extraction")
            .chain_err(|| "failed to create a temp directory for data extraction")?;

        // Compute the number of path parts in the ref namespace.
        let namespace_parts = 1 + self.ref_namespace
            .chars()
            .filter(|&ch| ch == '/')
            .count();
        let mut valid_refs = Vec::new();
        for data_ref in data_refs {
            // Extract the algorithm and hash parts from the refname.
            let ref_parts = data_ref.splitn(3 + namespace_parts, '/')
                // Skip the `refs/.../` bit.
                .skip(1 + namespace_parts)
                .tuples()
                .next();
            let (digest_str, expected_hash) = if let Some(bits) = ref_parts {
                bits
            } else {
                warn!(target: "ghostflow/data",
                      "unsupported refname {}",
                      data_ref);

                self.delete_ref(data_ref)?;
                continue;
            };
            let digest: Box<Digest> = match digest_str {
                "MD5" => Box::new(Md5::new()),
                "SHA256" => Box::new(Sha256::new()),
                "SHA512" => Box::new(Sha512::new()),
                _ => {
                    error!(target: "ghostflow/data",
                           "unsupported digest algorithm {}; ignoring",
                           digest_str);
                    continue;
                },
            };
            let (contents, hash) = self.hash_blob(data_ref, digest)?;
            let hash_matches = expected_hash == hash;

            if hash_matches {
                let output_dir = tempdir.path().join(&digest_str);
                fs::create_dir_all(&output_dir).chain_err(|| {
                    format!("failed to create directory {}",
                            output_dir.display())
                })?;

                let output_path = output_dir.join(&hash);

                // Write to the data file.
                {
                    let mut output_file = OpenOptions::new()
                        .mode(0o444)
                        .write(true)
                        .create_new(true)
                        .open(&output_path)
                        .chain_err(|| format!("failed to create file {}", output_path.display()))?;

                    output_file.write_all(&contents)
                        .chain_err(|| {
                            format!("failed to write to file {}", output_path.display())
                        })?;
                }

                valid_refs.push(data_ref);
            } else {
                warn!(target: "ghostflow/data",
                      "failed to verify {} hash; expected {}, actually {}",
                      data_ref,
                      expected_hash,
                      hash);

                self.lenient_delete_ref(data_ref);
            }
        }

        let mut source = tempdir.path().as_os_str().to_os_string();
        // We want to sync the contents of this directory, so add the trailing slash.
        source.push("/");
        self.destinations
            .iter()
            .map(|destination| {
                // Push the data to the remote server.
                let rsync = Command::new("rsync")
                    .arg("--recursive")
                    .arg("--perms")
                    .arg("--times")
                    .arg("--verbose")
                    .arg(&source)
                    .arg(destination)
                    .output()
                    .chain_err(|| "failed to construct rsync command")?;
                if !rsync.status.success() {
                    bail!(ErrorKind::Rsync(format!("failed to sync data to {}: {}",
                                                   destination,
                                                   String::from_utf8_lossy(&rsync.stderr))));
                }

                Ok(())
            })
            .collect::<Vec<Result<_>>>()
            .into_iter()
            .collect::<Result<Vec<_>>>()?;

        if !self.keep_refs {
            valid_refs.into_iter()
                .foreach(|refname| self.lenient_delete_ref(refname));
        }

        Ok(DataActionResult::DataPushed)
    }

    /// Hash a git object using a digest.
    fn hash_blob(&self, refname: &str, mut digest: Box<Digest>) -> Result<(Vec<u8>, String)> {
        // Get the type of the object.
        let cat_file = self.ctx
            .git()
            .arg("cat-file")
            .arg("-t")
            .arg(&refname)
            .output()
            .chain_err(|| "failed to construct cat-file -t command")?;
        if !cat_file.status.success() {
            bail!(ErrorKind::Git(format!("failed to get the type of {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&cat_file.stderr))));
        }
        let object_type = String::from_utf8_lossy(&cat_file.stdout);

        let contents = if object_type.trim() == "blob" {
            // Get the contents of the file.
            let cat_file = self.ctx
                .git()
                .arg("cat-file")
                .arg("blob")
                .arg(&refname)
                .output()
                .chain_err(|| "failed to construct cat-file command")?;
            if !cat_file.status.success() {
                bail!(ErrorKind::Git(format!("failed to get the contents of {}: {}",
                                             refname,
                                             String::from_utf8_lossy(&cat_file.stderr))));
            }

            cat_file.stdout
        } else {
            // Other object types are not supported.
            bail!(ErrorKind::Msg(format!("unsupported blob type {} for ref {}",
                                         object_type.trim(),
                                         refname)));
        };

        // Compute the hash of the contents.
        digest.input(&contents);
        Ok((contents, digest.result_str()))
    }

    /// Delete a local ref.
    fn delete_ref(&self, refname: &str) -> Result<()> {
        let update_ref = self.ctx
            .git()
            .arg("update-ref")
            .arg("-d")
            .arg(&refname)
            .output()
            .chain_err(|| "failed to construct update-ref command")?;
        if !update_ref.status.success() {
            bail!(ErrorKind::Git(format!("failed to delete data ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&update_ref.stderr))));
        }

        Ok(())
    }

    /// Delete a local ref, ignoring errors.
    fn lenient_delete_ref(&self, refname: &str) {
        let _ = self.delete_ref(refname)
            .map_err(|err| {
                error!(target: "ghostflow/data",
                       "failed to delete ref {}: {:?}",
                       refname,
                       err);
            });
    }
}
