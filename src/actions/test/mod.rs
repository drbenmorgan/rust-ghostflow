// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Testing actions
//!
//! Different projects have different strategies for proposing topics for testing. The different
//! strategies are implemented as actions here.

error_chain! {
    links {
        Jobs(jobs::Error, jobs::ErrorKind)
            #[doc = "Errors from the test jobs action."];
        Refs(refs::Error, refs::ErrorKind)
            #[doc = "Errors from the test refs action."];
    }
}

pub mod jobs;
pub mod refs;
