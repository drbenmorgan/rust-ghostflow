// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `test` action using ref-based testing.
//!
//! This action pushes refs into a ref namespace for use by testing machines.

use crates::git_workarea::GitContext;

use host::{self, CommitStatusState, HostedProject, MergeRequest};

error_chain! {
    links {
        Host(host::Error, host::ErrorKind)
            #[doc = "Errors from the service host."];
    }

    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

#[derive(Debug)]
/// Implementation of the `test` action.
pub struct TestRefs {
    /// The context to use for Git actions.
    ctx: GitContext,
    /// The project to be tested.
    project: HostedProject,
    /// The namespace to use for test refs.
    namespace: String,
    /// Whether the action should create informational comments or not.
    ///
    /// Errors always create comments.
    quiet: bool,
}

impl TestRefs {
    /// Create a new test action.
    pub fn new(ctx: GitContext, project: HostedProject) -> Self {
        Self {
            ctx: ctx,
            project: project,
            namespace: "test-topics".to_string(),
            quiet: false,
        }
    }

    /// Reduce the number of comments made by the test action.
    pub fn quiet(&mut self) -> &mut Self {
        self.quiet = true;
        self
    }

    /// The ref namespace to use for test topics.
    pub fn ref_namespace<N>(&mut self, namespace: N) -> &mut Self
        where N: ToString,
    {
        self.namespace = namespace.to_string();
        self
    }

    /// Push a merge request for testing.
    pub fn test_mr(&self, mr: &MergeRequest) -> Result<()> {
        info!(target: "ghostflow/test/refs",
              "pushing a test ref for {}",
              mr.url);

        // Fetch the merge request into the stager's git context.
        self.project.service.fetch_mr(&self.ctx, mr)?;

        let refname = self.refname(mr);

        let update_ref = self.ctx
            .git()
            .arg("update-ref")
            .arg(&refname)
            .arg(mr.commit.id.as_str())
            .output()
            .chain_err(|| "failed to construct update-ref command")?;
        if !update_ref.status.success() {
            bail!(ErrorKind::Git(format!("failed to update the test-topic ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&update_ref.stderr))));
        }

        let push = self.ctx
            .git()
            .arg("push")
            .arg("origin")
            .arg("--atomic")
            .arg("--porcelain")
            .arg(format!("{}:{}", refname, refname))
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !push.status.success() {
            bail!(ErrorKind::Git(format!("failed to push the test-topic ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&push.stderr))));
        }

        self.send_info_mr_comment(mr, "This topic has been pushed for testing.");

        self.send_mr_commit_status(mr, CommitStatusState::Success, "pushed for testing");

        Ok(())
    }

    /// Remove a merge request from the testing set.
    pub fn untest_mr(&self, mr: &MergeRequest) -> Result<()> {
        info!(target: "ghostflow/test/refs",
              "deleting the test ref for {}",
              mr.url);

        let refname = self.refname(mr);

        let show_ref = self.ctx
            .git()
            .arg("show-ref")
            .arg("--quiet")
            .arg("--verify")
            .arg(&refname)
            .status()
            .chain_err(|| "failed to construct show-ref command")?;
        if !show_ref.success() {
            // There is no such ref; skip.
            return Ok(());
        }

        self.delete_ref(&refname)?;

        let push = self.ctx
            .git()
            .arg("push")
            .arg("origin")
            .arg("--atomic")
            .arg("--porcelain")
            .arg(format!(":{}", refname))
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !push.status.success() {
            bail!(ErrorKind::Git(format!("failed to push the test-topic ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&push.stderr))));
        }

        self.send_mr_commit_status(mr, CommitStatusState::Success, "removed from testing");

        Ok(())
    }

    /// Clear the set of merge requests for testing.
    pub fn clear_all_mrs(&self) -> Result<()> {
        info!(target: "ghostflow/test/refs",
              "clearing all test refs for {}",
              self.project.name);

        let test_refs = self.ctx
            .git()
            .arg("for-each-ref")
            .arg("--format=%(refname:strip=2)")
            .arg(format!("refs/{}/", self.namespace))
            .output()
            .chain_err(|| "failed to construct for-each-ref command")?;
        if !test_refs.status.success() {
            bail!(ErrorKind::Git(format!("failed to list all test refs: {}",
                                         String::from_utf8_lossy(&test_refs.stderr))));
        }
        let topic_ids = String::from_utf8_lossy(&test_refs.stdout);
        let cleanup_results = topic_ids.lines()
            .filter_map(|topic_id| {
                match topic_id.parse() {
                    Ok(id) => Some(id),
                    Err(err) => {
                        error!(target: "ghostflow/test/refs",
                               "failed to parse {} as a topic id; deleting the ref: {:?}",
                               topic_id,
                               err);

                        let refname = format!("refs/{}/{}", self.namespace, topic_id);
                        self.lenient_delete_ref(refname);

                        None
                    },
                }
            })
            .filter_map(|topic_id| {
                match self.project.merge_request(topic_id) {
                    Ok(mr) => Some(self.untest_mr(&mr)),
                    Err(err) => {
                        error!(target: "ghostflow/test/refs",
                               "ref {} is not a valid merge request; deleting the ref: {:?}",
                               topic_id,
                               err);

                        let refname = format!("refs/{}/{}", self.namespace, topic_id);
                        self.lenient_delete_ref(refname);

                        None
                    },
                }
            })
            .collect::<Vec<_>>();

        cleanup_results.into_iter()
            .collect::<Result<Vec<_>>>()?;

        Ok(())
    }

    /// The refname for a merge request.
    fn refname(&self, mr: &MergeRequest) -> String {
        format!("refs/{}/{}", self.namespace, mr.id)
    }

    /// Delete a test ref from the local repository.
    fn delete_ref(&self, refname: &str) -> Result<()> {
        info!(target: "ghostflow/test/refs",
              "deleting test ref {}",
              refname);

        let delete_ref = self.ctx
            .git()
            .arg("update-ref")
            .arg("-d")
            .arg(&refname)
            .output()
            .chain_err(|| "failed to construct update-ref command")?;
        if !delete_ref.status.success() {
            bail!(ErrorKind::Git(format!("failed to delete test ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&delete_ref.stderr))));
        }

        Ok(())
    }

    /// Delete a test ref, ignoring errors.
    fn lenient_delete_ref(&self, refname: String) {
        let _ = self.delete_ref(&refname)
            .map_err(|err| {
                error!(target: "ghostflow/test/refs",
                       "failed to delete the {} ref from {}: {:?}",
                       refname,
                       self.project.name,
                       err);
            });
    }

    /// Set the commit status to a merge request.
    fn send_mr_commit_status(&self, mr: &MergeRequest, status: CommitStatusState, desc: &str) {
        let status = mr.create_commit_status(status, "ghostflow-test", desc);
        if let Err(err) = self.project.service.post_commit_status(status) {
            warn!(target: "ghostflow/test/refs",
                  "failed to post a commit status for mr {} on {} for '{}': {:?}",
                  mr.id,
                  mr.commit.id,
                  desc,
                  err);
        }
    }

    /// Send a comment to a merge request.
    fn send_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if let Err(err) = self.project.service.post_mr_comment(mr, content) {
            error!(target: "ghostflow/test/refs",
                   "failed to post a comment to merge request: {}, {}: {:?}",
                   self.project.name,
                   mr.id,
                   err);
        }
    }

    /// Send an informational comment to a merge request.
    fn send_info_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if !self.quiet {
            self.send_mr_comment(mr, content)
        }
    }
}
