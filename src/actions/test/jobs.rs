// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `test` action using JSON job files.
//!
//! This action drops a job file into a directory so that another tool can use it to perform the
//! testing.

use crates::chrono::Utc;
use crates::rand::{self, Rng};
use crates::serde_json::{self, Value};

use host::{self, HostedProject, MergeRequest};

use std::fs::{self, File};
use std::path::{Path, PathBuf};

error_chain! {
    links {
        Host(host::Error, host::ErrorKind)
            #[doc = "Errors from the service host."];
    }
}

#[derive(Debug)]
/// Implementation of the `test` action.
pub struct TestJobs {
    /// The directory to place test job files into.
    queue: PathBuf,
    /// The project to be tested.
    project: HostedProject,
    /// Whether the action should create informational comments or not.
    ///
    /// Errors always create comments.
    quiet: bool,
}

impl TestJobs {
    /// Create a new test action.
    pub fn new<Q>(queue: Q, project: HostedProject) -> Result<Self>
        where Q: AsRef<Path>,
    {
        fs::create_dir_all(queue.as_ref()).chain_err(|| {
            format!("failed to create the queue directory: {}",
                    queue.as_ref().display())
        })?;

        Ok(Self {
            queue: queue.as_ref().to_path_buf(),
            project: project,
            quiet: false,
        })
    }

    /// Reduce the number of comments made by the test action.
    pub fn quiet(&mut self) -> &mut Self {
        self.quiet = true;
        self
    }

    /// Test an update to a branch.
    pub fn test_update(&self, data: Value) -> Result<()> {
        info!(target: "ghostflow/test/jobs",
              "queuing an update test job for {}",
              self.project.name);

        self.queue_job(data)
    }

    /// Push a merge request for testing.
    pub fn test_mr(&self, mr: &MergeRequest, data: Value) -> Result<()> {
        info!(target: "ghostflow/test/jobs",
              "queuing a test job for {}",
              mr.url);

        self.queue_job(data)?;

        self.send_info_mr_comment(mr, "This topic has been queued for testing.");

        Ok(())
    }

    /// Queue a job into the target directory.
    fn queue_job(&self, data: Value) -> Result<()> {
        let rndpart = rand::thread_rng()
            .gen_ascii_chars()
            .take(12)
            .collect::<String>();
        let job_path = self.queue.join(format!("{}-{}.json", Utc::now().to_rfc3339(), rndpart));
        let mut job_file = File::create(&job_path).chain_err(|| {
            format!("failed to create the job file: {}",
                    job_path.display())
        })?;
        serde_json::to_writer(&mut job_file, &data).chain_err(|| {
            format!("failed to write to the job file: {}",
                    job_path.display())
        })?;

        Ok(())
    }

    /// Send a comment to a merge request.
    fn send_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if let Err(err) = self.project.service.post_mr_comment(mr, content) {
            error!(target: "ghostflow/test/jobs",
                   "failed to post a comment to merge request: {}, {}: {:?}",
                   self.project.name,
                   mr.id,
                   err);
        }
    }

    /// Send an informational comment to a merge request.
    fn send_info_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if !self.quiet {
            self.send_mr_comment(mr, content)
        }
    }
}
