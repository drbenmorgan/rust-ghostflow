// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `clone` action.
//!
//! This action clones a repository such that it is configured for use by other workflow actions.

use crates::git_workarea::GitContext;

use host::{self, HostedProject};

use std::borrow::Cow;
use std::collections::hash_map::HashMap;
use std::fmt::{self, Debug};
use std::fs::{create_dir_all, remove_dir_all};
use std::os::unix::fs::symlink;
use std::path::{Path, PathBuf};

error_chain! {
    links {
        Host(host::Error, host::ErrorKind)
            #[doc = "Errors from the service host."];
    }

    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

#[derive(Debug)]
/// A submodule which should be linked by the clone action from the top-level project.
pub enum CloneSubmoduleLink {
    /// A submodule which is hosted under the same work directory as the top-level project.
    Internal(String),
    /// An externally hosted submodule.
    External(PathBuf),
}

impl CloneSubmoduleLink {
    /// Creates a new link.
    ///
    /// Relative paths are assumed to be internal submodules whereas absolute paths are external.
    pub fn new<L: ToString>(link: L) -> Self {
        let link = link.to_string();
        let path = PathBuf::from(&link);

        if path.is_absolute() {
            CloneSubmoduleLink::External(path)
        } else {
            CloneSubmoduleLink::Internal(link)
        }
    }

    /// The path of the git directory.
    fn path<'a>(&'a self, workdir: &Path) -> Cow<'a, Path> {
        match *self {
            CloneSubmoduleLink::External(ref path) => Cow::Borrowed(path),
            CloneSubmoduleLink::Internal(ref link) => {
                Cow::Owned(workdir.join(format!("{}.git", link)))
            },
        }
    }
}

/// A map for submodule paths.
type CloneSubmoduleMap = HashMap<String, CloneSubmoduleLink>;

/// Implementation of the `clone` action.
///
/// Repositories need to be cloned in order for other actions to work. This action bootstraps gets
/// a repository onto the local filesystem and prepared the right way.
pub struct Clone_ {
    /// The path to the working directory for the service.
    ///
    /// All clones are placed underneath this path.
    workdir: PathBuf,
    /// The path to the `.git` directory of the specific clone.
    gitdir: PathBuf,
    /// The project to be cloned.
    project: HostedProject,
    /// Submodules which should be set up for the project.
    submodules: CloneSubmoduleMap,
}

impl Clone_ {
    /// Create a new clone action.
    pub fn new<W: AsRef<Path>>(workdir: W, project: HostedProject) -> Self {
        Self {
            workdir: workdir.as_ref().to_path_buf(),
            gitdir: workdir.as_ref().join(format!("{}.git", project.name)),
            project: project,
            submodules: CloneSubmoduleMap::new(),
        }
    }

    /// Add a submodule which should be linked to from the clone.
    pub fn with_submodule<N>(&mut self, name: N, submodule: CloneSubmoduleLink) -> &mut Self
        where N: ToString,
    {
        self.submodules.insert(name.to_string(), submodule);
        self
    }

    /// Check if the repository is already cloned.
    pub fn exists(&self) -> bool {
        self.gitdir.exists()
    }

    /// Clone a repository which is set up to mirror specific refs of a remote repository.
    pub fn clone_mirror_repo<R: ToString>(self, refs: &[R]) -> Result<GitContext> {
        let repo = self.project.service.repo(&self.project.name)?;

        let ctx = self.setup_clone_from(&repo.url)?;

        let clear_fetch = ctx.git()
            .arg("config")
            .arg("--unset-all")
            .arg("remote.origin.fetch")
            .status()
            .chain_err(|| "failed to construct config command")?;
        if let Some(5) = clear_fetch.code() {
            // git config --unset return 5 if there were no matches.
        } else if !clear_fetch.success() {
            bail!(ErrorKind::Git(format!("failed to unset all `remote.origin.fetch` settings: {}",
                                         clear_fetch.code().unwrap_or_else(i32::min_value))));
        }

        for ref_ in refs {
            let refname = ref_.to_string();
            let refs = ctx.git()
                .arg("config")
                .arg("--add")
                .arg("remote.origin.fetch")
                .arg(format!("+{}:{}", refname, refname))
                .status()
                .chain_err(|| "failed to construct config command")?;
            if !refs.success() {
                bail!(ErrorKind::Git(format!("failed to add `remote.origin.fetch` setting for \
                                              {}",
                                             refname)));
            }
        }

        self.setup_submodules(&ctx)?;
        self.fetch_configured(&ctx)?;

        Ok(ctx)
    }

    /// Clone a repository which will be updated manually.
    ///
    /// These repositories should be managed manually, such as triggered by notifications that the
    /// remote repository has been updated or on a timer.
    pub fn clone_watched_repo(self) -> Result<GitContext> {
        let repo = self.project.service.repo(&self.project.name)?;

        let ctx = self.setup_clone_from(&repo.url)?;

        // Tags should not be part of watched repos.
        let no_tags = ctx.git()
            .arg("config")
            .arg("remote.origin.tagopt")
            .arg("--no-tags")
            .status()
            .chain_err(|| "failed to construct config command")?;
        if !no_tags.success() {
            bail!(ErrorKind::Git("failed to set `remote.origin.tagopt`".to_string()));
        }

        self.setup_submodules(&ctx)?;

        // Fetch the data into the repository.
        self.fetch_heads(&ctx)?;

        Ok(ctx)
    }

    /// Internal method to perform the basic setup of a clone.
    fn setup_clone_from(&self, url: &str) -> Result<GitContext> {
        let ctx = GitContext::new(&self.gitdir);

        if self.exists() {
            return Ok(ctx);
        }

        create_dir_all(ctx.gitdir()).chain_err(|| {
            format!("failed to create the clone working directory {}",
                    self.gitdir.display())
        })?;

        info!(target: "ghostflow/clone",
              "cloning from {} into {} for {}",
              url,
              self.gitdir.display(),
              self.project.name);

        let init = ctx.git()
            .arg("--bare")
            .arg("init")
            .output()
            .chain_err(|| "failed to construct init command")?;
        if !init.status.success() {
            bail!(ErrorKind::Git(format!("failed to initialize a bare repository in {}: {}",
                                         self.gitdir.display(),
                                         String::from_utf8_lossy(&init.stderr))));
        }

        // Set the url for the origin remote.
        let remote = ctx.git()
            .arg("config")
            .arg("remote.origin.url")
            .arg(url)
            .output()
            .chain_err(|| "failed to construct config command")?;
        if !remote.status.success() {
            bail!(ErrorKind::Git(format!("failed to set the remote in {} to {}: {}",
                                         self.gitdir.display(),
                                         url,
                                         String::from_utf8_lossy(&remote.stderr))));
        }

        // All ref updates should be logged.
        let log_all_ref_updates = ctx.git()
            .arg("config")
            .arg("core.logAllRefUpdates")
            .arg("true")
            .output()
            .chain_err(|| "failed to construct config command")?;
        if !log_all_ref_updates.status.success() {
            bail!(ErrorKind::Git(format!("failed to set `core.logAllRefUpdates` in {}: {}",
                                         self.gitdir.display(),
                                         String::from_utf8_lossy(&log_all_ref_updates.stderr))));
        }

        Ok(ctx)
    }

    /// Create symlinks for the submodules of a clone.
    fn setup_submodules(&self, ctx: &GitContext) -> Result<()> {
        let moduledir = ctx.gitdir().join("modules");

        info!(target: "ghostflow/clone",
              "removing modules directory: {}",
              moduledir.display());

        if moduledir.exists() {
            remove_dir_all(&moduledir).chain_err(|| {
                format!("failed to remove old submodule directory {}",
                        moduledir.display())
            })?;
        }

        for (name, link) in &self.submodules {
            let submodulelink = moduledir.join(name);
            let submoduledir = submodulelink.parent()
                .expect("expected the submodule link to have a parent directory");
            let targetdir = link.path(&self.workdir);

            info!(target: "ghostflow/clone",
                  "linking submodule {}: {} -> {}",
                  name,
                  submodulelink.display(),
                  targetdir.display());

            create_dir_all(&submoduledir).chain_err(|| {
                format!("failed to create submodule directory {}",
                        submoduledir.display())
            })?;
            symlink(targetdir, &submodulelink).chain_err(|| {
                format!("failed to symlink submodule directory {} to {}",
                        submoduledir.display(),
                        submodulelink.display())
            })?;
        }

        Ok(())
    }

    /// Fetch the default refs from `origin` into the clone.
    fn fetch_configured(&self, ctx: &GitContext) -> Result<()> {
        info!(target: "ghostflow/clone",
              "fetching initial pre-configured refs into {}",
              self.gitdir.display());

        let fetch = ctx.git()
            .arg("fetch")
            .arg("origin")
            .output()
            .chain_err(|| "failed to construct fetch command")?;
        if !fetch.status.success() {
            bail!(ErrorKind::Git(format!("failed to fetch from origin in {}: {}",
                                         self.gitdir.display(),
                                         String::from_utf8_lossy(&fetch.stderr))));
        }

        Ok(())
    }

    /// Fetch the head refs from `origin` into the clone.
    fn fetch_heads(&self, ctx: &GitContext) -> Result<()> {
        info!(target: "ghostflow/clone",
              "fetching initial branch refs into {}",
              self.gitdir.display());

        let fetch = ctx.git()
            .arg("fetch")
            .arg("origin")
            .arg("--prune")
            .arg("+refs/heads/*:refs/heads/*")
            .output()
            .chain_err(|| "failed to construct fetch command")?;
        if !fetch.status.success() {
            bail!(ErrorKind::Git(format!("failed to fetch from origin in {}: {}",
                                         self.gitdir.display(),
                                         String::from_utf8_lossy(&fetch.stderr))));
        }

        Ok(())
    }
}

impl Debug for Clone_ {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Clone")
            .field("gitdir", &self.gitdir)
            .field("project", &self.project)
            .field("submodules", &self.submodules)
            .finish()
    }
}
