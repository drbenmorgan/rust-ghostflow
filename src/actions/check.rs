// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `check` action.
//!
//! This action checks that commits pass to a set of git checks.

use crates::git_checks::{self, CheckResult, GitCheckConfiguration};
use crates::git_workarea::{CommitId, GitContext};
use crates::itertools::Itertools;

use host::{self, CommitStatusState, HostingService, MergeRequest};
use utils::mr;

use std::fmt::{self, Debug};
use std::sync::Arc;

error_chain! {
    links {
        GitChecks(git_checks::Error, git_checks::ErrorKind)
            #[doc = "Errors from the git-checks crate."];
        Host(host::Error, host::ErrorKind)
            #[doc = "Errors from the service host."];
        MrUtils(mr::Error, mr::ErrorKind)
            #[doc = "Errors from the merge request utilities."];
    }

    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }

        /// A commit requested to be checked is not part of the merge request.
        UnrelatedCommit(commit: CommitId) {
            display("commit unrelated to the merge request: {}", commit.as_str())
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// States for a check result.
pub enum CheckStatus {
    /// The checks passed.
    Pass,
    /// The checks failed.
    Fail,
}

/// Implementation of the `check` action.
pub struct Check<'a> {
    /// The context to use for checking commits.
    ctx: GitContext,
    /// The service which hosts the project.
    service: Arc<HostingService>,
    /// The configuration to use for the check.
    ///
    /// This contains the actual checks to use.
    config: GitCheckConfiguration<'a>,
    /// The administrators of the project.
    ///
    /// These users are notified when checks find problems which should be brought to an
    /// administrator's attention.
    admins: &'a [String],
}

impl<'a> Check<'a> {
    /// Create a new check action.
    pub fn new(ctx: GitContext, service: Arc<HostingService>, config: GitCheckConfiguration<'a>,
               admins: &'a [String])
               -> Self {
        Check {
            ctx: ctx,
            service: service,
            config: config,
            admins: admins,
        }
    }

    /// The name of the status the `check` action will use for the given branch.
    pub fn status_name<B>(branch: B) -> String
        where B: AsRef<str>,
    {
        format!("ghostflow-branch-check-{}", branch.as_ref())
    }

    /// The description of the status the `check` action will use for the given branch.
    fn status_description<B>(branch: B, commit: &CommitId) -> String
        where B: AsRef<str>,
    {
        format!("overall branch status for the content checks against {}\n\
                 \n\
                 Branch-at: {}",
                branch.as_ref(),
                commit)
    }

    /// Check a range of commits.
    pub fn check_mr<R>(self, reason: R, base: &CommitId, mr: &MergeRequest) -> Result<CheckStatus>
        where R: AsRef<str>,
    {
        self.check_mr_with(reason.as_ref(), base, mr, &mr.commit.id)
    }

    /// Check a range of commits from a given commit.
    ///
    /// This allows checking a merge request against a backport branch and checking only the
    /// commits which belong on the backport branch.
    pub fn check_mr_with<R>(self, reason: R, base: &CommitId, mr: &MergeRequest,
                            commit_id: &CommitId)
                            -> Result<CheckStatus>
        where R: AsRef<str>,
    {
        self.check_mr_impl(reason.as_ref(), base, mr, commit_id)
    }

    /// Check a range of commits.
    ///
    /// This is the actual implementation of the check action.
    fn check_mr_impl(self, reason: &str, base: &CommitId, mr: &MergeRequest, commit_id: &CommitId)
                     -> Result<CheckStatus> {
        info!(target: "ghostflow/check",
              "checking merge request {}",
              mr.url);

        // Ensure that the commit is part of the merge request.
        if !mr::contains_commit(&self.ctx, mr, commit_id, base)? {
            // Explicitly do not contact the hosting service here; the caller should collect this
            // information and handle it.
            bail!(ErrorKind::UnrelatedCommit(commit_id.clone()));
        }

        let topic_checks = self.config
            .run_topic(&self.ctx, reason, base, commit_id, &mr.author.identity())?;

        topic_checks.commit_results()
            .map(|&(ref sha, ref result)| {
                let state = if result.pass() {
                    CommitStatusState::Success
                } else {
                    CommitStatusState::Failed
                };
                let topic_commit = {
                    let mut commit = mr.commit.clone();
                    commit.id = sha.clone();
                    commit
                };
                let status =
                    topic_commit.create_commit_status(state,
                                                      "ghostflow-commit-check",
                                                      "basic content checks");
                self.service.post_commit_status(status)?;

                Ok(())
            })
            .collect::<Result<Vec<_>>>()?;

        let mut result: CheckResult = topic_checks.into();

        if mr.work_in_progress {
            result.add_warning("the merge request is marked as a work-in-progress.");
        }

        let state = if result.pass() {
            CommitStatusState::Success
        } else {
            CommitStatusState::Failed
        };
        let status_name = Self::status_name(base.as_str());
        let status_description = Self::status_description(base.as_str(), commit_id);
        let status = mr.create_commit_status(state, &status_name, &status_description);
        self.service.post_commit_status(status)?;

        self.report_to_mr(mr, result)
    }

    /// Post the results of a check as a merge request comment.
    fn report_to_mr(&self, mr: &MergeRequest, result: CheckResult) -> Result<CheckStatus> {
        // Just silently accept allowed MRs.
        if result.allowed() {
            return Ok(CheckStatus::Pass);
        }

        let pass = result.pass();

        let comment = self.check_result_comment(result, true);
        if !comment.is_empty() {
            self.service.post_mr_comment(mr, &comment)?;
        }

        Ok(if pass {
            CheckStatus::Pass
        } else {
            CheckStatus::Fail
        })
    }

    /// Create a comment for the given check result.
    fn check_result_comment(&self, result: CheckResult, with_assist: bool) -> String {
        let mut comment = String::new();

        // This scope is necessary so that the borrow in `push_results` ends before we use
        // `comment` again at the end of the function.
        {
            let mut push_results = |label, items: &Vec<String>| {
                if !items.is_empty() {
                    comment.push_str(&Self::comment_fragment(label, items));
                }
            };

            push_results("Errors", result.errors());
            push_results("Warnings", result.warnings());
            push_results("Alerts", result.alerts());
        }

        if with_assist {
            if !result.warnings().is_empty() {
                comment.push_str("The warnings do not need to be fixed, but it is recommended to \
                                  do so.\n\n");
            }

            if !result.errors().is_empty() {
                comment.push_str("Please rewrite commits to fix the errors listed above (adding \
                                  fixup commits will not resolve the errors) and force-push the \
                                  branch again to update the merge request.\n\n");
            }

            if result.temporary() {
                comment.push_str("Some messages may be temporary; please trigger the checks again \
                                  if they have been resolved.\n\n");
            }
        }

        if !result.alerts().is_empty() {
            comment.push_str(&format!("Alert: @{}.\n\n", self.admins.join(" @")));
        }

        // Remove trailing whitespace from the comment.
        let non_ws_len = comment.trim_right().len();
        comment.truncate(non_ws_len);

        comment
    }

    /// Create a fragment of the comment.
    fn comment_fragment(label: &str, items: &[String]) -> String {
        format!("{}:\n\n  - {}\n\n", label, items.iter().join("\n  - "))
    }
}

impl<'a> Debug for Check<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Check")
            .field("ctx", &self.ctx)
            .field("config", &self.config)
            .field("admins", &self.admins)
            .finish()
    }
}
