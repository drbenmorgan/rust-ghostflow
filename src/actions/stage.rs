// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `stage` action.
//!
//! This action is intended to manage a temporary integration branch (normally `stage`) to perform
//! testing on a collection of branches which are on their way into the main integration branch
//! (normally `master`).

use crates::chrono::{DateTime, Utc};
use crates::git_topic_stage::{self, CandidateTopic, IntegrationResult, Stager, Topic, UnstageReason};
use crates::git_workarea::{Identity, MergeStatus};
use crates::itertools::Itertools;

use host::{self, Commit, CommitStatusState, HostedProject, MergeRequest};

use std::borrow::Cow;

error_chain! {
    links {
        Stage(git_topic_stage::Error, git_topic_stage::ErrorKind)
            #[doc = "Errors from the git-topic-stage crate."];
        Host(host::Error, host::ErrorKind)
            #[doc = "Errors from the service host."];
    }

    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

#[derive(Debug, Clone, Copy)]
/// Policies which may be used when tagging the stage.
pub enum TagStagePolicy {
    /// Topics currently on the stage may stay.
    KeepTopics,
    /// The stage is cleared of all topics and reset to the target branch.
    ClearStage,
}

impl Default for TagStagePolicy {
    fn default() -> Self {
        TagStagePolicy::ClearStage
    }
}

#[derive(Debug)]
/// Implementation of the `stage` action.
///
/// The stage is a collection of topic branches which should be tested together. The stage is meant
/// to be "tagged" on a regular basis and pushed for testing. In the meantime, topics may be added
/// to and removed from the staging branch. If any topic is updated, it is removed from the stage
/// and put at the end of the set of topics ready for merging. Additionally, if the base of the
/// stage updates, the entire stage is recreated.
pub struct Stage {
    /// The target branch for the stage.
    branch: String,
    /// The stager manager for the branch.
    stager: Stager,
    /// The project of the target branch.
    project: HostedProject,
    /// Whether the action should create informational comments or not.
    ///
    /// Errors always create comments.
    quiet: bool,
}

impl Stage {
    /// Create a new stage action.
    pub fn new<B: ToString>(stager: Stager, branch: B, project: HostedProject) -> Result<Self> {
        let stage = Self {
            branch: branch.to_string(),
            stager: stager,
            project: project,
            quiet: false,
        };

        stage.update_head_ref()?;

        Ok(stage)
    }

    /// Reduce the number of comments made by the stage action.
    ///
    /// The comments created by this action can be a bit much. This reduces the comments to those
    /// which are errors or are important.
    pub fn quiet(&mut self) -> &mut Self {
        self.quiet = true;
        self
    }

    /// A reference to the internal stager.
    pub fn stager(&self) -> &Stager {
        &self.stager
    }

    /// Update the base commit for the stage.
    ///
    /// Note that this function does no checking to ensure that the given commit is related to the
    /// existing base commit.
    pub fn base_branch_update(&mut self, commit: &Commit, who: &Identity, when: DateTime<Utc>)
                              -> Result<()> {
        info!(target: "ghostflow/stage",
              "updating the base commit for {}/{}",
              self.project.name,
              self.branch);

        // Fetch the commit into the stager's git context.
        self.project.service.fetch_commit(self.stager.git_context(), commit)?;

        let candidate = CandidateTopic {
            old_id: Some(Topic::new(self.stager.base().clone(),
                                    who.clone(),
                                    Utc::now(),
                                    0,
                                    "base",
                                    "url")),
            new_id: Topic::new(commit.id.clone(), who.clone(), when, 0, "base", "url"),
        };

        self.update_stage_base(candidate)?;
        Ok(self.update_head_ref()?)
    }

    /// Add a merge request to the stage.
    fn stage_merge_request_impl(&mut self, mr: &MergeRequest, topic_name: &str, who: &Identity,
                                when: DateTime<Utc>)
                                -> Result<()> {
        info!(target: "ghostflow/stage",
              "attempting to stage {}",
              mr.url);

        // Fetch the MR commit into the stager's git context.
        self.project.service.fetch_mr(self.stager.git_context(), mr)?;

        let old_commit = if let Some(staged) = self.stager.find_topic_by_id(mr.id) {
            if &mr.commit.id == staged.commit() {
                self.send_info_mr_comment(mr,
                                          "This topic has already been staged; ignoring the \
                                           request to stage.");

                return Ok(());
            }

            let expected = match mr.old_commit {
                Some(ref old_commit) => {
                    let expected = staged.commit();

                    if &old_commit.id != expected {
                        warn!(target: "ghostflow/stage",
                              "it appears as though an update for the merge request {} \
                               was missed; munging the request so that it removes the stale \
                               branch ({}) from the stage instead of the indicated branch \
                               ({}).",
                              mr.url,
                              expected,
                              old_commit.id);
                    }

                    expected
                },
                None => staged.commit(),
            };

            Some(self.project.service.commit(&mr.source_repo.name, expected)?)
        } else {
            None
        };

        // Create the candidate topic for the MR.
        let old_hosted_commit = old_commit.as_ref();
        let candidate = CandidateTopic {
            old_id: old_hosted_commit.map(|c| {
                Topic::new(c.id.clone(),
                           who.clone(),
                           Utc::now(),
                           mr.id,
                           topic_name,
                           &mr.url)
            }),
            new_id: Topic::new(mr.commit.id.clone(),
                               who.clone(),
                               when,
                               mr.id,
                               topic_name,
                               &mr.url),
        };

        // Update the stage.
        self.update_stage_mr(candidate, old_hosted_commit, mr)?;
        // Push the new stage state to the remote.
        Ok(self.update_head_ref()?)
    }

    /// Add a merge request to the stage with a given name.
    pub fn stage_merge_request_named<N>(&mut self, mr: &MergeRequest, name: N, who: &Identity,
                                        when: DateTime<Utc>)
                                        -> Result<()>
        where N: AsRef<str>,
    {
        self.stage_merge_request_impl(mr, name.as_ref(), who, when)
    }

    /// Add a merge request to the stage.
    pub fn stage_merge_request(&mut self, mr: &MergeRequest, who: &Identity, when: DateTime<Utc>)
                               -> Result<()> {
        self.stage_merge_request_impl(mr, &mr.source_branch, who, when)
    }

    /// Unstage a merge request.
    fn unstage_merge_request_impl(&mut self, mr: &MergeRequest, success_msg: &str,
                                  missing_msg: Option<&str>)
                                  -> Result<()> {
        let staged_topic_opt = self.stager.find_topic_by_id(mr.id).cloned();

        Ok(if let Some(staged_topic) = staged_topic_opt {
            let stage_result = self.stager.unstage(staged_topic)?;

            self.send_info_mr_comment(mr,
                                      &format!("This merge request has been unstaged {}.",
                                               success_msg));
            self.send_mr_commit_status(mr, CommitStatusState::Success, "unstaged");

            // Update topics have been punted off of the stage (successfully staged commits are
            // fine).
            for topic in &stage_result.results {
                self.update_mr_state(topic, false, &mr_update_reason(mr))?;
            }

            // Push the new stage state to the remote.
            self.update_head_ref()?
        } else {
            if let Some(msg) = missing_msg {
                self.send_info_mr_comment(mr, msg);
            }

            ()
        })
    }

    /// Remove a merge request from the stage due to an update.
    pub fn unstage_update_merge_request(&mut self, mr: &MergeRequest, reason: &str) -> Result<()> {
        info!(target: "ghostflow/stage",
              "attempting to unstage {} because of an update {}",
              mr.url,
              reason);

        self.unstage_merge_request_impl(mr, reason, None)
    }

    /// Remove a merge request from the stage.
    pub fn unstage_merge_request(&mut self, mr: &MergeRequest) -> Result<()> {
        info!(target: "ghostflow/stage",
              "attempting to unstage {}",
              mr.url);

        self.unstage_merge_request_impl(mr,
                                        "upon request",
                                        Some("Failed to find this merge request on the stage; \
                                              ignoring the request to unstage it."))
    }

    /// Tag the stage into a ref and reset the state of the stage.
    ///
    /// The ref `refs/stage/{branch}/{reason}/latest` and
    /// `refs/stage/{branch}/{reason}/{dateformat}` are updated to point to the current state of
    /// the stage.
    pub fn tag_stage(&mut self, reason: &str, ref_date_format: &str, policy: TagStagePolicy)
                     -> Result<()> {
        info!(target: "ghostflow/stage",
              "tagging the stage for {}/{}",
              self.project.name,
              self.branch);

        // Tag the current state of the stage.
        let (when, stage_ref) = self.tag_latest_ref(reason, ref_date_format)?;

        let (staged_topics, msg) = match policy {
            TagStagePolicy::ClearStage => {
                let msg = format!("This merge request has been pushed for {} testing as of {} \
                                   and unstaged.",
                                  reason,
                                  when.format(ref_date_format));
                (Cow::Owned(self.stager.clear()), Some(msg))
            },
            TagStagePolicy::KeepTopics => (Cow::Borrowed(self.stager.topics()), None),
        };
        let state_desc = format!("staged for {} testing {}", reason, stage_ref);
        for staged_topic in staged_topics.iter() {
            let mr_res = self.hosted_mr(&staged_topic.topic);
            match mr_res {
                Ok(mr) => {
                    self.send_mr_commit_status(&mr, CommitStatusState::Success, &state_desc);

                    msg.as_ref()
                        .map(|msg| self.send_mr_comment(&mr, msg));
                },
                Err(err) => {
                    error!(target: "ghostflow/stage",
                           "failed to fetch mr {} for {}: {:?}",
                           staged_topic.topic.id,
                           self.project.name,
                           err);
                },
            }
        }

        // Push the new stage to the remote.
        Ok(self.update_head_ref()?)
    }

    /// Update the base of the stage.
    fn update_stage_base(&mut self, candidate: CandidateTopic) -> Result<()> {
        let stage_result = self.stager.stage(candidate)?;

        // Update topics have been punted off of the stage (successfully staged commits are fine).
        for topic in &stage_result.results {
            self.update_mr_state(topic,
                                 false,
                                 &format!("an update to the {} branch causing ", self.branch))?;
        }

        Ok(())
    }

    /// Update a merge request which is already on the stage.
    fn update_stage_mr(&mut self, candidate: CandidateTopic, old_commit: Option<&Commit>,
                       mr: &MergeRequest)
                       -> Result<()> {
        let stage_result = self.stager.stage(candidate)?;

        old_commit.map(|commit| {
            // We use success here because it was successfully unstaged. A failure would cause a
            // old commits to never be shown as "passing" where this information might be useful at
            // a glance.
            self.send_commit_status(commit, CommitStatusState::Success, "unstaged")
        });

        // If no results were made, the base branch was updated and no topics were already staged;
        // everything is fine.
        let results = stage_result.results[..].split_last();
        if let Some((new_topic, restaged_topics)) = results {
            let update_reason = mr_update_reason(mr);

            // Update topics have been punted off of the stage (successfully staged commits are
            // fine).
            for topic in restaged_topics {
                self.update_mr_state(topic, false, &update_reason)?;
            }

            self.update_mr_state(new_topic, true, "")?;
        }

        Ok(())
    }

    /// Update the `HEAD` ref of the stage.
    fn update_head_ref(&self) -> Result<()> {
        let ctx = self.stager.git_context();
        let refname = format!("refs/stage/{}/head", self.branch);

        let update_ref = ctx.git()
            .arg("update-ref")
            .arg(&refname)
            .arg(self.stager.head().as_str())
            .output()
            .chain_err(|| "failed to construct update-ref command")?;
        if !update_ref.status.success() {
            bail!(ErrorKind::Git(format!("failed to update the stage head ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&update_ref.stderr))));
        }

        let push = ctx.git()
            .arg("push")
            .arg("origin")
            .arg("--atomic")
            .arg("--porcelain")
            .arg(format!("+{}:{}", refname, refname))
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !push.status.success() {
            bail!(ErrorKind::Git(format!("failed to push the stage head ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&push.stderr))));
        }

        Ok(())
    }

    /// Tag the current `HEAD` of the stage as a named ref.
    fn tag_latest_ref(&self, reason: &str, date_format: &str) -> Result<(DateTime<Utc>, String)> {
        let ctx = self.stager.git_context();
        let now = Utc::now();
        let refname = format!("refs/stage/{}/{}/{}",
                              self.branch,
                              reason,
                              now.format(date_format));

        let update_ref = ctx.git()
            .arg("update-ref")
            .arg(&refname)
            .arg(self.stager.head().as_str())
            .arg("0000000000000000000000000000000000000000")
            .output()
            .chain_err(|| "failed to construct update-ref command")?;
        if !update_ref.status.success() {
            bail!(ErrorKind::Git(format!("failed to update the tag stage head ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&update_ref.stderr))));
        }

        let reason_refname = format!("refs/stage/{}/{}/latest", self.branch, reason);

        let update_ref_tagged = ctx.git()
            .arg("update-ref")
            .arg(&reason_refname)
            .arg(self.stager.head().as_str())
            .output()
            .chain_err(|| "failed to construct update-ref command")?;
        if !update_ref_tagged.status.success() {
            bail!(ErrorKind::Git(format!("failed to update the tag reason stage head ref {}: {}",
                                         refname,
                                         String::from_utf8_lossy(&update_ref_tagged.stderr))));
        }

        let push = ctx.git()
            .arg("push")
            .arg("origin")
            .arg("--atomic")
            .arg("--porcelain")
            .arg(format!("+{}:{}", reason_refname, reason_refname))
            .arg(&refname)
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !push.status.success() {
            bail!(ErrorKind::Git(format!("failed to push the tag stage refs {} and {}: {}",
                                         refname,
                                         reason_refname,
                                         String::from_utf8_lossy(&push.stderr))));
        }

        Ok((now, refname))
    }

    /// The merge request for a given topic.
    fn hosted_mr(&self, topic: &Topic) -> host::Result<MergeRequest> {
        self.project.merge_request(topic.id)
    }

    /// Update the state of a merge request after being staged.
    fn update_mr_state(&self, result: &IntegrationResult, post_success: bool, update_reason: &str)
                       -> Result<()> {
        let mr = self.hosted_mr(result.topic())?;
        match *result {
            IntegrationResult::Staged(_) => {
                self.send_mr_commit_status(&mr, CommitStatusState::Success, "staged");

                if post_success {
                    self.send_info_mr_comment(&mr, "Successfully staged.");
                }
            },
            IntegrationResult::Unstaged(_, ref reason) => {
                self.send_mr_commit_status(&mr,
                                           CommitStatusState::Failed,
                                           &format!("failed to merge: {}",
                                                    unstaged_status_desc(reason)));
                self.send_mr_comment(&mr, &unstaged_status_message(reason, update_reason));
            },
            IntegrationResult::Unmerged(_, ref reason) => {
                let (status, desc) = unmerged_status_desc(reason);
                self.send_mr_commit_status(&mr, status, &format!("unstaged: {}", desc));

                let comment_method = if let CommitStatusState::Success = status {
                    Self::send_info_mr_comment
                } else {
                    Self::send_mr_comment
                };

                comment_method(self, &mr, &unmerged_status_message(reason));
            },
        }

        Ok(())
    }

    /// Set the commit status to a merge request.
    fn send_mr_commit_status(&self, mr: &MergeRequest, status: CommitStatusState, desc: &str) {
        let status = mr.create_commit_status(status, "ghostflow-stager", desc);
        if let Err(err) = self.project.service.post_commit_status(status) {
            warn!(target: "ghostflow/stage",
                  "failed to post a commit status for mr {} on {} for '{}': {:?}",
                  mr.id,
                  mr.commit.id,
                  desc,
                  err);
        }
    }

    /// Set the commit status on a commit.
    fn send_commit_status(&self, commit: &Commit, status: CommitStatusState, desc: &str) {
        let status = commit.create_commit_status(status, "ghostflow-stager", desc);
        if let Err(err) = self.project.service.post_commit_status(status) {
            warn!(target: "ghostflow/stage",
                  "failed to post a commit status on {} for '{}': {:?}",
                  commit.id,
                  desc,
                  err);
        }
    }

    /// Send a comment to a merge request.
    fn send_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if let Err(err) = self.project.service.post_mr_comment(mr, content) {
            error!(target: "ghostflow/stage",
                   "failed to post a comment to merge request: {}, {}: {:?}",
                   self.project.name,
                   mr.id,
                   err);
        }
    }

    /// Send an informational comment to a merge request.
    fn send_info_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if !self.quiet {
            self.send_mr_comment(mr, content)
        }
    }
}

/// The description for why a merge request has been unstaged.
fn unstaged_status_desc(reason: &UnstageReason) -> String {
    match *reason {
        UnstageReason::MergeConflict(ref conflicts) => {
            format!("{} conflicting paths", conflicts.iter().dedup().count())
        },
    }
}

/// The reason for a merge request update.
fn mr_update_reason(mr: &MergeRequest) -> String {
    format!("an update to the [{}]({}) topic causing ",
            mr.source_branch,
            mr.url)
}

/// The status message for an unstaged topic.
fn unstaged_status_message(reason: &UnstageReason, update_reason: &str) -> String {
    let reason_message = match *reason {
        UnstageReason::MergeConflict(ref conflicts) => {
            let mut conflict_paths = conflicts.iter()
                .map(|conflict| conflict.path().to_string_lossy())
                .dedup();

            format!("merge conflicts in the following paths:\n\
                     \n  - `{}`",
                    conflict_paths.join("`\n  - `"))
        },
    };

    format!("This merge request has been unstaged due to {}{}",
            update_reason,
            reason_message)
}

/// The description for a merge status.
fn unmerged_status_desc(reason: &MergeStatus) -> (CommitStatusState, &str) {
    match *reason {
        MergeStatus::NoCommonHistory => (CommitStatusState::Failed, "no common history"),
        MergeStatus::AlreadyMerged => (CommitStatusState::Success, "already merged"),
        MergeStatus::Mergeable(_) => {
            error!(target: "ghostflow/stage",
                   "mergeable unmergeable state?");
            (CommitStatusState::Failed, "mergeable?")
        },
    }
}

/// The status message for a merge status.
fn unmerged_status_message(reason: &MergeStatus) -> String {
    let reason_message = match *reason {
        MergeStatus::NoCommonHistory => "there is no common history",
        MergeStatus::AlreadyMerged => "it has already been merged",
        MergeStatus::Mergeable(_) => "it is\u{2026}mergeable? Sorry, something went wrong",
    };

    format!("This merge request has been unstaged because {}.",
            reason_message)
}
