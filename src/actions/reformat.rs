// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `reformat` action.
//!
//! This action takes a merge request and reformats all commits it adds to its target branch and
//! reformats each commit using tools to format selected files within the repository according to
//! the appropriate `format.{kind}` attribute on files changed in a commit.
//!
//! There may be multiple formatters to run on a given repository. Each is given a `kind` (used in
//! attribute lookups), a path to an executable to run to perform the formatting, and a set of
//! files within the repository which contain configuration files for the formatter.

use crates::chrono::{DateTime, FixedOffset};
use crates::git_checks::{self, AttributeState, CheckGitContext, Commit, Content, FileName};
use crates::git_workarea::{self, CommitId, GitContext, GitWorkArea};
use crates::itertools::Itertools;
use crates::rayon::prelude::*;
use crates::wait_timeout::ChildExt;

use host::{HostedProject, MergeRequest};

use std::collections::hash_map::HashMap;
use std::io::Write;
use std::iter;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::time::Duration;

error_chain! {
    links {
        GitChecks(git_checks::Error, git_checks::ErrorKind)
            #[doc = "Errors from the git-checks crate."];
        GitWorkArea(git_workarea::Error, git_workarea::ErrorKind)
            #[doc = "Errors from the git-workarea crate."];
    }

    errors {
        /// An empty kind was given.
        EmptyKind {
            display("an empty `kind` was given")
        }

        /// A merge commit may not be used to reformat the entire repository.
        MergeCommit {
            display("merge commits cannot be used to reformat repositories")
        }

        /// A formatter path does not exist.
        MissingFormatter(formatter: PathBuf) {
            display("the formatter path does not exist: {}", formatter.display())
        }

        /// Parsing an author or committer date from a commit failed.
        DateParse {
            display("parsing a date")
        }

        /// A formatter failed.
        ReformatFailed(commit: CommitId, paths: Vec<String>) {
            display("failed to reformat commit {} files: `{}`", commit, paths.iter().format("`, `"))
        }

        /// Deleted files were expected to still be in the work tree.
        DeletedFiles(commit: CommitId, paths: Vec<String>) {
            display("reformatting commit {} deleted files: `{}`",
                    commit,
                    paths.iter().format("`, `"))
        }

        /// Untracked files were found in the work tree.
        UntrackedFiles(commit: CommitId, paths: Vec<String>) {
            display("reformatting commit {} added untracked files: `{}`",
                    commit,
                    paths.iter().format("`, `"))
        }

        /// Pushing to the remote repository failed.
        PushFailed(msg: String) {
            display("failed to push: {}", msg)
        }

        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }
    }
}

#[derive(Debug)]
/// A formatter for source code in a repository.
///
/// The formatter is passed the file it is expected to format as its only argument.
///
/// Generally, formatters should be idempotent so that lines are not changed multiple times
/// over the course of a topic.
///
/// Untracked files left at the end of the formatter are treated as a failure.
pub struct Formatter {
    /// The `kind` of the formatter (used for attribute queries).
    kind: String,
    /// The path to the formatter.
    formatter: PathBuf,
    /// Configuration files within the repository for the formatter.
    config_files: Vec<String>,
    /// A timeout for running the formatter.
    ///
    /// If the formatter exceeds this timeout, it is considered to have failed.
    timeout: Option<Duration>,
}

lazy_static! {
    /// How long to wait for a timed-out formatter to respond to `SIGKILL` before leaving it as a
    /// zombie process.
    static ref ZOMBIE_TIMEOUT: Duration = Duration::from_secs(1);
}

impl Formatter {
    /// Create a new formatter.
    pub fn new<K, F>(kind: K, formatter: F) -> Result<Self>
        where K: ToString,
              F: AsRef<Path>,
    {
        let kind = kind.to_string();
        if kind.is_empty() {
            bail!(ErrorKind::EmptyKind);
        }

        let formatter = formatter.as_ref().to_path_buf();
        if !formatter.exists() {
            bail!(ErrorKind::MissingFormatter(formatter));
        }

        Ok(Self {
            kind: kind,
            formatter: formatter,
            config_files: Vec::new(),
            timeout: None,
        })
    }

    /// Add configuration files within the repository which should be checked out.
    pub fn add_config_files<I, F>(&mut self, files: I) -> &mut Self
        where I: IntoIterator<Item = F>,
              F: ToString,
    {
        self.config_files.extend(files.into_iter().map(|file| file.to_string()));
        self
    }

    /// Add a timeout to the formatter.
    pub fn with_timeout(&mut self, timeout: Duration) -> &mut Self {
        self.timeout = Some(timeout);
        self
    }

    /// Format a path within the repository.
    fn format_path<'a>(&self, workarea: &GitWorkArea, path: &'a FileName)
                       -> Result<Option<&'a FileName>> {
        let mut cmd = Command::new(&self.formatter);
        workarea.cd_to_work_tree(&mut cmd);
        cmd.arg(path.as_path());

        let (success, output) = if let Some(timeout) = self.timeout {
            let mut child = cmd
                .stdin(Stdio::null())
                .stdout(Stdio::null())
                .stderr(Stdio::null())
                .spawn()
                .chain_err(|| "failed to construct formatter command")?;
            let check = child.wait_timeout(timeout)
                .chain_err(|| "failed to wait on the formatter command")?;

            if let Some(status) = check {
                (status.success(), format!("failed with exit code {:?}, signal {:?}",
                                           status.code(),
                                           status.unix_signal()))
            } else {
                child.kill().chain_err(|| "failed to kill a timed-out formatter")?;
                let timed_out_status = child.wait_timeout(*ZOMBIE_TIMEOUT)
                    .chain_err(|| "failed to wait on a timed-out formatter")?;
                if timed_out_status.is_none() {
                    warn!(target: "ghostflow/formatting",
                          "leaving a zombie '{}' process; it did not respond to kill",
                          self.kind);
                }
                (false, "timeout reached".to_string())
            }
        } else {
            let check = cmd.output()
                .chain_err(|| "failed to construct formatter command")?;
            (check.status.success(), String::from_utf8_lossy(&check.stderr).into_owned())
        };

        Ok(if success {
            None
        } else {
            info!(target: "ghostflow/reformat",
                  "failed to run the {} formatter: {}",
                  self.kind,
                  output);
            Some(path)
        })
    }
}

/// Information about a commit for formatting purposes.
struct CommitInfo {
    /// Information about the commit for checks.
    commit: Commit,
    /// The authorship date of the commit.
    author_date: DateTime<FixedOffset>,
    /// The commit date of the commit.
    committer_date: DateTime<FixedOffset>,
}

#[derive(Debug)]
/// Implementation of the `reformat` action.
pub struct Reformat {
    /// The context to use for Git actions.
    ctx: GitContext,
    /// The project.
    project: HostedProject,
    /// Formatters to run during the action.
    formatters: Vec<Formatter>,
}

impl Reformat {
    /// Create a new reformat action.
    pub fn new(ctx: GitContext, project: HostedProject) -> Self {
        Self {
            ctx: ctx,
            project: project,
            formatters: Vec::new(),
        }
    }

    /// Add formatters to the action.
    pub fn add_formatters<I>(&mut self, formatters: I) -> &mut Self
        where I: IntoIterator<Item = Formatter>,
    {
        self.formatters.extend(formatters.into_iter());
        self
    }

    /// Reformat the entire tree through a merge request.
    ///
    /// This method rewrites the entire tree as part of a merge request by rewriting the `HEAD` of
    /// the source branch to have reformatting of the entire repository at once.
    pub fn reformat_repo(&self, mr: &MergeRequest) -> Result<CommitId> {
        let rewrite_map = HashMap::new();

        let work_commit = self.construct_work_commit(&mr.commit.id, &rewrite_map)?;

        let commit_info = self.commit_info(&work_commit)?;

        if commit_info.commit.parents.len() > 1 {
            self.send_mr_comment(mr,
                                 "The repository cannot be reformatted using a merge commit. \
                                  Please use a non-merge commit.");
            bail!(ErrorKind::MergeCommit);
        }

        let workarea = self.ctx.prepare(&work_commit)?;

        // List all files in the repository.
        let ls_files = workarea.git()
            // Force quoting to be on.
            .arg("-c").arg("core.quotePath=true")
            .arg("ls-files")
            .output()
            .chain_err(|| "failed to construct ls-files command")?;
        if !ls_files.status.success() {
            bail!(ErrorKind::Git(format!("failed to list merge request commit files: {}",
                                         String::from_utf8_lossy(&ls_files.stderr))));
        }
        let file_paths = String::from_utf8_lossy(&ls_files.stdout);
        let all_files = file_paths.lines()
            .map(FileName::new)
            .collect::<Vec<FileName>>();
        workarea.checkout(&all_files)?;
        let all_files_ref = all_files.iter().collect::<Vec<_>>();

        let new_tree = self.reformat_paths(workarea, all_files_ref, &mr.commit.id, mr)?;
        let new_commit = self.commit_tree(&commit_info, new_tree, &rewrite_map)?;

        self.push_new_head(&new_commit, mr, &[])?;
        Ok(new_commit)
    }

    /// Reformat a merge request and push it to its source repository.
    ///
    /// The topology of the topic is kept the same by rewriting commits in order and committing the
    /// reformatted trees by replacing the old parent commit IDs with the newly formed parent IDs.
    pub fn reformat_mr(&self, base: &CommitId, mr: &MergeRequest) -> Result<CommitId> {
        let rev_list = self.ctx
            .git()
            .arg("rev-list")
            .arg("--reverse")
            .arg("--topo-order")
            .arg(format!("^{}", base))
            .arg(mr.commit.id.as_str())
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !rev_list.status.success() {
            bail!(ErrorKind::Git(format!("failed to list merge request revisions: {}",
                                         String::from_utf8_lossy(&rev_list.stderr))));
        }
        let commits = String::from_utf8_lossy(&rev_list.stdout);

        let mut rewrite_map = HashMap::new();
        let mut empty_commits = Vec::new();

        for commit in commits.lines().map(CommitId::new) {
            let work_commit = self.construct_work_commit(&commit, &rewrite_map)?;

            let commit_info = self.commit_info(&work_commit)?;
            let workarea = self.ctx.prepare(&work_commit)?;

            let changed_files = commit_info.commit.modified_files();

            // Create the files necessary on the disk.
            let files_to_checkout = changed_files.iter()
                .map(|path| path.as_path())
                .chain(self.formatters
                    .iter()
                    .map(|formatter| {
                        formatter.config_files
                            .iter()
                            .map(AsRef::as_ref)
                    })
                    .flatten())
                .collect::<Vec<_>>();
            workarea.checkout(&files_to_checkout)?;

            let new_tree = self.reformat_paths(workarea, changed_files, &commit, mr)?;
            let new_commit = self.commit_tree(&commit_info, new_tree, &rewrite_map)?;

            // See if we can remove this commit from the history. We can only do this if the
            // following are true:
            //
            //   - the commit has a single parent (merge commits operate on more than just the code
            //     itself and must be kept for their history editing functionality as well);
            //   - the commit was not empty before the reformatting, but is now empty.
            //
            // A commit which meets both of these is highly likely to have been a "apply formatting
            // fixes" commit and may now be removed from the history. The commit will be saved for
            // later so that it may be mentioned as being dropped later.
            if commit_info.commit.parents.len() == 1 &&
               !Commit::new(&self.ctx, &commit)?.diffs.is_empty() &&
               Commit::new(&self.ctx, &new_commit)?.diffs.is_empty() {
                let parent = &commit_info.commit.parents[0];
                let new_parent = rewrite_map.get(parent).unwrap_or(parent).clone();

                // Add it to the set of commits to mention.
                empty_commits.push(commit.clone());
                // Stitch up history to point to the reformatted parent.
                rewrite_map.insert(commit, new_parent);

                continue;
            }

            rewrite_map.insert(commit, new_commit);
        }

        let new_head = rewrite_map.get(&mr.commit.id)
            .unwrap_or(&mr.commit.id);
        self.push_new_head(new_head, mr, &empty_commits)?;
        Ok(new_head.clone())
    }

    fn reformat_paths(&self, workarea: GitWorkArea, paths: Vec<&FileName>, commit: &CommitId,
                      mr: &MergeRequest)
                      -> Result<CommitId> {
        // Collect the set of files which fail their formatters.
        let check_ctx = CheckGitContext::new(workarea, mr.author.identity());
        let failed_paths = self.formatters
            .iter()
            .map(|formatter| {
                // Run paths handled by the formatters according to their attributes.
                let attr = format!("format.{}", formatter.kind);
                paths.par_iter()
                    .map(|path| {
                        let state = check_ctx.check_attr(&attr, path.as_path())?;
                        if let AttributeState::Set = state {
                            formatter.format_path(check_ctx.workarea(), path)
                        } else {
                            Ok(None)
                        }
                    })
                    .collect::<Vec<Result<_>>>()
                    .into_iter()
                    .collect::<Result<Vec<_>>>()
            })
            .collect::<Vec<Result<_>>>()
            .into_iter()
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .flatten()
            .filter_map(|path| path.map(FileName::as_str))
            .unique()
            .collect::<Vec<_>>();

        if !failed_paths.is_empty() {
            self.send_mr_comment(mr,
                                 &format!("Failed to format the following files in {}:\n\n  - \
                                           `{}`",
                                          commit,
                                          failed_paths.iter().join("`\n  - `")));
            bail!(ErrorKind::ReformatFailed(commit.clone(),
                                            failed_paths.into_iter()
                                                .map(ToString::to_string)
                                                .collect()));
        }

        self.check_deleted_files(check_ctx.workarea(), &paths, commit, mr)?;
        self.check_untracked_files(check_ctx.workarea(), commit, mr)?;

        let add = check_ctx.git()
            .arg("add")
            .args(&paths.iter()
                .map(|path| path.as_path())
                .collect::<Vec<_>>())
            .output()
            .chain_err(|| "failed to construct add command")?;
        if !add.status.success() {
            bail!(ErrorKind::Git(format!("failed to add files in the work area: {}",
                                         String::from_utf8_lossy(&add.stderr))));
        }

        let write_tree = check_ctx.git()
            .arg("write-tree")
            .output()
            .chain_err(|| "failed to construct write-tree command")?;
        if !write_tree.status.success() {
            bail!(ErrorKind::Git(format!("failed to write the reformatted tree: {}",
                                         String::from_utf8_lossy(&write_tree.stderr))));
        }
        Ok(CommitId::new(String::from_utf8_lossy(&write_tree.stdout).trim()))
    }

    /// Push a new head to the source repository of the merge request.
    ///
    /// This creates a comment about the result of the reformatting.
    fn push_new_head(&self, new_head: &CommitId, mr: &MergeRequest, empty_commits: &[CommitId]) -> Result<()> {
        if new_head == &mr.commit.id {
            self.send_mr_comment(mr, "This topic is clean and required no reformatting.");
        } else {
            let push = self.ctx
                .git()
                .arg("push")
                .arg("--atomic")
                .arg("--porcelain")
                .arg(format!("--force-with-lease=refs/heads/{}:{}",
                             mr.source_branch,
                             mr.commit.id))
                .arg(&mr.source_repo.url)
                .arg(format!("{}:refs/heads/{}", new_head, mr.source_branch))
                .output()
                .chain_err(|| "failed to construct push command")?;
            if !push.status.success() {
                let stderr = String::from_utf8_lossy(&push.stderr);

                self.send_mr_comment(mr, "Failed to push the reformatted branch.");
                bail!(ErrorKind::PushFailed(format!("failed to push the reformatted branch to {}: \
                                                     {}",
                                                    mr.source_repo.url,
                                                    stderr)));
            }

            let mut msg = "This topic has been reformatted and pushed; please fetch from the \
                           source repository and reset your local branch to continue with further \
                           development on the reformatted commits."
                .to_string();
            if !empty_commits.is_empty() {
                msg.push_str(&format!("\n\nThe following commits were empty after reformatting \
                                       and removed from the history: {}.",
                                      empty_commits.iter().format(", ")));
            }

            self.send_mr_comment(mr, &msg);
        }

        Ok(())
    }

    /// Commit a tree using the same information as a template commit.
    fn commit_tree(&self, commit_info: &CommitInfo, tree: CommitId,
                   rewrite_map: &HashMap<CommitId, CommitId>)
                   -> Result<CommitId> {
        let parent_args_paired = iter::repeat("-p")
            .zip(commit_info.commit
                .parents
                .iter()
                .map(|parent| {
                    rewrite_map.get(parent)
                        .unwrap_or(parent)
                        .as_str()
                }))
            .map(|(flag, arg)| [flag, arg])
            .collect::<Vec<_>>();
        let parent_args = parent_args_paired
            .iter()
            .flatten();
        let mut commit_tree = self.ctx
            .git()
            .arg("commit-tree")
            .arg(tree.as_str())
            .args(parent_args)
            .env("GIT_AUTHOR_NAME", &commit_info.commit.author.name)
            .env("GIT_AUTHOR_EMAIL", &commit_info.commit.author.email)
            .env("GIT_AUTHOR_DATE", commit_info.author_date.to_rfc2822())
            .env("GIT_COMMITTER_NAME", &commit_info.commit.committer.name)
            .env("GIT_COMMITTER_EMAIL", &commit_info.commit.committer.email)
            .env("GIT_COMMITTER_DATE",
                 commit_info.committer_date.to_rfc2822())
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .chain_err(|| "failed to construct commit-tree command")?;
        let _ = parent_args;

        {
            let commit_tree_stdin =
                commit_tree.stdin.as_mut().expect("expected commit-tree to have a stdin");
            commit_tree_stdin.write_all(commit_info.commit.message.as_bytes())
                .chain_err(|| {
                    ErrorKind::Git("failed to write the commit message to commit-tree".to_string())
                })?;
        }

        let commit_tree = commit_tree.wait_with_output()
            .chain_err(|| "failed to execute commit-tree command")?;
        if !commit_tree.status.success() {
            bail!(ErrorKind::Git(format!("failed to commit the reformatted tree: {}",
                                         String::from_utf8_lossy(&commit_tree.stderr))));
        }

        let new_commit = String::from_utf8_lossy(&commit_tree.stdout);
        Ok(CommitId::new(new_commit.trim()))
    }

    /// Construct a commit on which to perform the actual reformatting.
    ///
    /// This is required because we only reformat files which have changed in a particular commit.
    /// Given commit `A` which is an ancestor of commit `B`, if file `X` is reformatted in `A`,
    /// but unchanged in `B` from `A`, the blind reformatting of `B` will inherit the file `X` from
    /// `A`, ignoring the reformatting we just did for `A'`. Instead, create a commit `B''` in the
    /// location of the commit `B'` with the history of the target topic so that when we ask "what
    /// files changed in `B` compared to `A'`?", we see that `X` needs to be reformatted in `B` as
    /// well since it inherited it from `A` (and since it was reformatted in `A'`, there is now a
    /// difference).
    ///
    /// This function takes `B` and creates our working commit `B''` using the new history.
    fn construct_work_commit(&self, commit: &CommitId, rewrite_map: &HashMap<CommitId, CommitId>)
                             -> Result<CommitId> {
        let commit_info = self.commit_info(commit)?;
        let rev_parse = self.ctx
            .git()
            .arg("rev-parse")
            .arg(format!("{}^{{tree}}", commit))
            .output()
            .chain_err(|| "failed to construct rev-parse command")?;
        if !rev_parse.status.success() {
            bail!(ErrorKind::Git(format!("failed to get the tree for commit {}: {}",
                                         commit,
                                         String::from_utf8_lossy(&rev_parse.stderr))));
        }
        let tree = CommitId::new(String::from_utf8_lossy(&rev_parse.stdout).trim());
        self.commit_tree(&commit_info, tree, rewrite_map)
    }

    /// Gather the required information about a commit.
    fn commit_info(&self, commit: &CommitId) -> Result<CommitInfo> {
        let commit_dates = self.ctx
            .git()
            .arg("log")
            .arg("--pretty=%aI%n%cI")
            .arg("--max-count=1")
            .arg(commit.as_str())
            .output()
            .chain_err(|| "failed to construct log command for date query")?;
        if !commit_dates.status.success() {
            bail!(ErrorKind::Git(format!("failed to fetch dates on the {} commit: {}",
                                         commit,
                                         String::from_utf8_lossy(&commit_dates.stderr))));
        }
        let dates = String::from_utf8_lossy(&commit_dates.stdout);
        let date_lines = dates.lines().collect::<Vec<_>>();

        assert!(date_lines.len() == 2,
                "got {} rather than 2 lines when logging a commit: {:?}",
                date_lines.len(),
                date_lines);

        let commit = CommitId::new(commit);
        Ok(CommitInfo {
            commit: Commit::new(&self.ctx, &commit)?,
            author_date: date_lines[0].parse()
                .chain_err(|| ErrorKind::DateParse)?,
            committer_date: date_lines[1].parse()
                .chain_err(|| ErrorKind::DateParse)?,
        })
    }

    /// Check the workarea for files that have been deleted.
    fn check_deleted_files(&self, workarea: &GitWorkArea, paths: &[&FileName], commit: &CommitId,
                           mr: &MergeRequest)
                           -> Result<()> {
        // If there were no paths changed, all files are considered deleted because an empty list
        // of files causes `ls-files` to list all files.
        if paths.is_empty() {
            return Ok(());
        }

        let ls_files = workarea.git()
            .arg("ls-files")
            .arg("-d")
            .arg("--")
            .args(&paths.iter()
                .map(|path| path.as_path())
                .collect::<Vec<_>>())
            .output()
            .chain_err(|| "failed to construct ls-files command")?;
        if !ls_files.status.success() {
            bail!(ErrorKind::Git(format!("failed to list deleted files in the work area: \
                                          {}",
                                         String::from_utf8_lossy(&ls_files.stderr))));
        }
        let ls_files_output = String::from_utf8_lossy(&ls_files.stdout);
        let deleted_paths = ls_files_output.lines().collect::<Vec<_>>();

        if !deleted_paths.is_empty() {
            self.send_mr_comment(mr,
                                 &format!("The following paths were deleted while \
                                           formatting {}:\n\n  - `{}`",
                                          commit,
                                          deleted_paths.iter().join("`\n  - `")));
            bail!(ErrorKind::DeletedFiles(commit.clone(),
                                          deleted_paths.into_iter()
                                              .map(ToString::to_string)
                                              .collect()));
        }

        Ok(())
    }

    /// Check for untracked files which have been dropped into the workarea.
    fn check_untracked_files(&self, workarea: &GitWorkArea, commit: &CommitId, mr: &MergeRequest)
                             -> Result<()> {
        let ls_files = workarea.git()
            .arg("ls-files")
            .arg("-o")
            .output()
            .chain_err(|| "failed to construct ls-files command")?;
        if !ls_files.status.success() {
            bail!(ErrorKind::Git(format!("failed to list untracked files in the work area: {}",
                                         String::from_utf8_lossy(&ls_files.stderr))));
        }
        let ls_files_output = String::from_utf8_lossy(&ls_files.stdout);
        let untracked_paths = ls_files_output.lines().collect::<Vec<_>>();

        if !untracked_paths.is_empty() {
            self.send_mr_comment(mr,
                                 &format!("The following untracked paths were created while \
                                           formatting {}:\n\n  - `{}`",
                                          commit,
                                          untracked_paths.iter().join("`\n  - `")));
            bail!(ErrorKind::UntrackedFiles(commit.clone(),
                                            untracked_paths.into_iter()
                                                .map(ToString::to_string)
                                                .collect()));
        }

        Ok(())
    }

    /// Send a comment to a merge request.
    fn send_mr_comment(&self, mr: &MergeRequest, content: &str) {
        if let Err(err) = self.project.service.post_mr_comment(mr, content) {
            error!(target: "ghostflow/reformat",
                   "failed to post a comment to merge request: {}, {}: {:?}",
                   self.project.name,
                   mr.id,
                   err);
        }
    }
}
