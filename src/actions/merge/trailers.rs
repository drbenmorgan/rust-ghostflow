// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::itertools::Itertools;

use actions::merge::Result;
use host::{Award, Comment, HostedProject, HostingService, MergeRequest, User};
use utils::{Trailer, TrailerRef};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ParseTrailers;

/// Markers in comments which are parsed as trailers.
const TRAILER_MARKERS: &[(&str, &[&str])] = &[
    ("Acked-by", &[
        "+1",
        ":+1:",
        ":thumbsup:",
    ]),
    ("Reviewed-by", &[
        "+2",
    ]),
    ("Tested-by", &[
        "+3",
    ]),
    ("Rejected-by", &[
        "-1",
        ":-1:",
        ":thumbsdown:",
    ]),
];

impl ParseTrailers {
    /// Find trailers from the merge request awards and comment stream.
    pub fn find(project: &HostedProject, mr: &MergeRequest)
                -> Result<Vec<(Trailer, Option<User>)>> {
        // Gather trailer information from the merge request.
        let comments = project.service.get_mr_comments(mr)?;
        let mr_awards = match project.service.get_mr_awards(mr) {
            Ok(awards) => awards,
            Err(err) => {
                error!(target: "ghostflow/merge",
                       "failed to get awards for mr {}: {:?}",
                       mr.url,
                       err);

                Vec::new()
            },
        };

        Ok(comments.iter()
            // Look at comments from newest to oldest.
            .rev()
            // Stop when we have a branch update comment.
            //
            // TODO: This should instead probably be "newer than the last branch update" since it
            // looks like not all hosting services (e.g., Github) support seeing the "system"
            // comments via the API. However, it seems that the `updated_at` field provided by
            // Gitlab and Github changes with things like editing the description. May require
            // feature requests to Github and a patch to Gitlab.
            .take_while(|comment| !comment.is_branch_update)
            .filter_map(|comment| {
                if comment.is_system {
                    // Ignore system comments.
                    None
                } else {
                    // Parse trailers from each comment.
                    Some(Self::parse_comment_for_trailers(project.service.as_ref(), comment))
                }
            })
            // Now that we have all the trailers, gather them up.
            .collect::<Vec<_>>()
            .into_iter()
            // Put them back into chronological order.
            .rev()
            // Put all of the trailers together into a single vector.
            .flatten()
            // Get trailers via awards on the MR itself.
            .chain({
                mr_awards.into_iter()
                    .filter_map(Self::parse_award_as_trailers)
                    .collect::<Vec<_>>()
            })
            .collect())
    }

    /// Create a trailer from a user.
    fn make_user_trailer(token: &str, user: &User) -> Trailer {
        Trailer::new(token, format!("{}", user.identity()))
    }

    /// Parse an award as a trailer.
    fn parse_award_as_trailers(award: Award) -> Option<(Trailer, Option<User>)> {
        let name = &award.name;

        // Handle skin tone color variants as their base version.
        let base_name = if name[..name.len() - 1].ends_with("_tone") {
            &name[..name.len() - 6]
        } else {
            name
        };

        match base_name {
            "100" | "clap" | "tada" | "thumbsup" => {
                Some((Self::make_user_trailer("Acked-by", &award.author), Some(award.author)))
            },
            "no_good" | "thumbsdown" => {
                Some((Self::make_user_trailer("Rejected-by", &award.author), Some(award.author)))
            },
            _ => None,
        }
    }

    /// Parse a comment for trailers.
    fn parse_comment_for_trailers(service: &HostingService, comment: &Comment)
                                  -> Vec<(Trailer, Option<User>)> {
        let explicit_trailers = TrailerRef::extract(&comment.content)
            .into_iter()
            // Transform values based on a some shortcuts like user references and a `me` shortcut.
            .filter_map(|trailer| {
                if !trailer.token.ends_with("-by") {
                    // Only `-by` trailers go through the username search.
                    Some((trailer.into(), None))
                } else if trailer.value.starts_with('@') {
                    // Handle user references.
                    service.user(&trailer.value[1..])
                        // Just drop unknown user references.
                        .ok()
                        .map(|user| {
                            (Self::make_user_trailer(trailer.token, &user),
                             Some(user.clone()))
                        })
                } else if trailer.value == "me" {
                    // Handle the special value `me` to mean the comment author.
                    Some((Self::make_user_trailer(trailer.token, &comment.author),
                          Some(comment.author.clone())))
                } else {
                    // Use the trailer as-is.
                    Some((trailer.into(), None))
                }
            });
        // Gather the implicit trailers from things like `+2` lines and the like.
        let implicit_trailers = comment.content
            .lines()
            .filter_map(|l| {
                let line = l.trim();

                TRAILER_MARKERS.iter()
                    .filter_map(|&(token, needles)| {
                        needles.iter()
                            .find(|&needle| line.starts_with(needle))
                            .map(|_| {
                                (Self::make_user_trailer(token, &comment.author),
                                 Some(comment.author.clone()))
                            })
                    })
                    .next()
            });

        explicit_trailers.chain(implicit_trailers)
            .collect()
    }
}
