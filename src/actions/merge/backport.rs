// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Merging with backporting support.
//!
//! This action may be used when a merge request targets a branch, but also contains pieces which
//! need to be backported to other branches. It performs all of the relevant merges and pushes them
//! to the repository at once.

use crates::chrono::{DateTime, Utc};
use crates::either::{Left, Right};
use crates::git_workarea::{CommitId, GitContext, Identity};
use crates::itertools::Itertools;
use crates::topological_sort::TopologicalSort;

use actions::merge::prelude_impl::*;
use host::{HostedProject, MergeRequest};
use utils::mr;

use std::collections::hash_map::HashMap;
use std::collections::hash_set::HashSet;

/// A backport settings structure with the commit that should be merged.
pub struct MergeBackport<'a, P>
    where P: 'a,
{
    /// The settings to use for merging.
    settings: &'a MergeSettings<P>,
    /// The commit to use from the topic.
    commit: Option<CommitId>,
}

impl<'a, P> MergeBackport<'a, P>
    where P: 'a,
{
    /// Create a new backport description.
    pub fn new(settings: &'a MergeSettings<P>, commit: Option<CommitId>) -> Self {
        MergeBackport {
            settings: settings,
            commit: commit,
        }
    }
}

/// Merge a merge request into multiple target branches.
pub struct MergeMany {
    /// The context to use for Git actions.
    ctx: GitContext,
    /// The project of the target branches.
    project: HostedProject,
}

impl MergeMany {
    /// Create a new merge action.
    pub fn new(ctx: GitContext, project: HostedProject) -> Self {
        Self {
            ctx: ctx,
            project: project,
        }
    }

    /// Merge a merge request into multiple target branches.
    ///
    /// Information for the merge commit is gathered from the comment stream as well as the merge
    /// request itself. Comments from before the last update are ignored since they do not apply to
    /// the latest incarnation of the topic.
    pub fn merge_mr<'a, I, P>(&self, mr: &MergeRequest, who: &Identity, when: DateTime<Utc>,
                              with: I)
                              -> Result<MergeActionResult>
        where I: IntoIterator<Item = MergeBackport<'a, P>>,
              P: MergePolicy + 'a,
    {
        self.merge_mr_named(mr, &mr.source_branch, who, when, with)
    }

    /// Merge a merge request into the branch with a different name.
    pub fn merge_mr_named<'a, T, I, P>(&self, mr: &MergeRequest, topic_name: T, who: &Identity,
                                       when: DateTime<Utc>, with: I)
                                       -> Result<MergeActionResult>
        where T: AsRef<str>,
              I: IntoIterator<Item = MergeBackport<'a, P>>,
              P: MergePolicy + 'a,
    {
        let merger = Merger::new(&self.ctx, &self.project, mr)?;
        let info = MergeInformation {
            topic_name: topic_name.as_ref(),
            who: who,
            when: when,
        };

        if let Right(res) = merger.prep_mr()? {
            return Ok(res);
        }

        let mut refs = HashMap::new();
        let mut target_branches = HashSet::new();
        let mut sorter = TopologicalSort::new();

        for backport in with {
            let branch = backport.settings.branch().to_string();
            let commit = {
                if !target_branches.insert(backport.settings.branch()) {
                    bail!(ErrorKind::DuplicateTargetBranch(backport.settings.branch().to_string()));
                }

                let commit_id = backport.commit
                    .as_ref()
                    .unwrap_or(&mr.commit.id);

                // Ensure that the commit is part of the merge request.
                if !mr::contains_commit(&self.ctx,
                                        mr,
                                        commit_id,
                                        &CommitId::new(backport.settings.branch()))? {
                    bail!(ErrorKind::UnrelatedCommit(commit_id.clone()));
                }

                let merge_res = merger.create_merge(backport.settings, &info, commit_id)?;
                match merge_res {
                    Left(commit_id) => commit_id,
                    Right(res) => {
                        return Ok(res);
                    },
                }
            };

            backport.settings
                .into_branches()
                .iter()
                .foreach(|into_branch| {
                    sorter.add_dependency(branch.clone(), into_branch.name().to_string())
                });

            refs.insert(branch, (commit, backport.settings));
        }

        let quiet = refs.iter().all(|(_, &(_, settings))| settings.is_quiet());
        let refs_status = refs.iter()
            .map(|(branch, &(ref commit, settings))| {
                (branch.clone(), commit.clone(), settings.into_branches())
            });
        let push_refs = merger.perform_update_merges(sorter, refs_status, &info)?;

        merger.push_refs(quiet, push_refs)
    }
}
