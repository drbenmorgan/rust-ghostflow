// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

pub use actions::merge::{Error, ErrorKind, Result, ResultExt};

pub use actions::merge::policy::MergePolicy;

pub use actions::merge::settings::MergeActionResult;
pub use actions::merge::settings::MergeInformation;
pub use actions::merge::settings::MergeSettings;
pub use actions::merge::settings::Merger;
