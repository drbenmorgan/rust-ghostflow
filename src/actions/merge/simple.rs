// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Simple merging.
//!
//! This action may be used when a merge request targets a single branch.

use crates::chrono::{DateTime, Utc};
use crates::git_workarea::{GitContext, Identity};

use actions::merge::prelude_impl::*;
use host::{HostedProject, MergeRequest};

#[derive(Debug)]
/// Implementation of the `merge` action.
pub struct Merge<P> {
    /// The settings for the merge action.
    settings: MergeSettings<P>,
    /// The context to use for Git actions.
    ctx: GitContext,
    /// The project of the target branch.
    project: HostedProject,
}

impl<P> Merge<P> {
    /// Create a new merge action.
    pub fn new(ctx: GitContext, project: HostedProject, settings: MergeSettings<P>) -> Self {
        Self {
            settings: settings,
            ctx: ctx,
            project: project,
        }
    }

    /// The settings for the merge action.
    pub fn settings(&self) -> &MergeSettings<P> {
        &self.settings
    }
}

impl<P> Merge<P>
    where P: MergePolicy,
{
    /// Merge a merge request into the branch.
    ///
    /// Information for the merge commit is gathered from the comment stream as well as the merge
    /// request itself. Comments from before the last update are ignored since they do not apply to
    /// the latest incarnation of the topic.
    pub fn merge_mr(&self, mr: &MergeRequest, who: &Identity, when: DateTime<Utc>)
                    -> Result<MergeActionResult> {
        self.merge_mr_named(mr, &mr.source_branch, who, when)
    }

    /// Merge a merge request into the branch with a different name.
    pub fn merge_mr_named<B>(&self, mr: &MergeRequest, topic_name: B, who: &Identity,
                             when: DateTime<Utc>)
                             -> Result<MergeActionResult>
        where B: AsRef<str>,
    {
        let merger = Merger::new(&self.ctx, &self.project, mr)?;
        let info = MergeInformation {
            topic_name: topic_name.as_ref(),
            who: who,
            when: when,
        };
        merger.merge_mr(&self.settings, info)
    }
}
