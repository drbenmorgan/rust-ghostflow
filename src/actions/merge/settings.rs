// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::{DateTime, Utc};
use crates::either::{Either, Left, Right};
use crates::git_workarea::{CommitId, GitContext, Identity, MergeResult, MergeStatus};
use crates::itertools::Itertools;
use crates::topological_sort::TopologicalSort;

use actions::merge::*;
use actions::merge::trailers::ParseTrailers;
use host::{HostedProject, MergeRequest, User};
use utils::Trailer;

use std::collections::hash_map::HashMap;
use std::iter;

#[derive(Debug, Clone)]
/// Information about how to merge into a branch.
pub struct IntoBranch {
    /// The name of the target branch.
    name: String,
    /// Further branches to merge into.
    chain: Vec<IntoBranch>,
}

impl IntoBranch {
    /// Create a new description for an into branch.
    pub fn new<N>(name: N) -> Self
        where N: ToString,
    {
        Self {
            name: name.to_string(),
            chain: Vec::new(),
        }
    }

    /// Add branches for further merging.
    pub fn chain_into<I>(&mut self, branch: I) -> &mut Self
        where I: IntoIterator<Item = Self>,
    {
        self.chain.extend(branch.into_iter());
        self
    }

    /// The name of the branch.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// The branches to chain into.
    pub fn chain_branches(&self) -> &[Self] {
        &self.chain
    }

    fn add_topo_links(&self, sorter: &mut TopologicalSort<String>) {
        self.chain
            .iter()
            .foreach(|into_branch| {
                debug!("adding dep from {} -> {}",
                       self.name(),
                       into_branch.name());
                sorter.add_dependency(self.name(), into_branch.name());
                into_branch.add_topo_links(sorter);
            })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
/// The result of the merge action.
pub enum MergeActionResult {
    /// Everything worked fine.
    Success,
    /// The push failed.
    ///
    /// This likely means that the remote changed in some way and the merge will need to be
    /// restarted.
    PushFailed,
    /// The merge failed due to conflicts or otherwise unsuitable state of the merge request.
    ///
    /// Failures require user interaction before they may be attempted again.
    Failed,
}

#[derive(Debug)]
/// Settings for a merge action.
pub struct MergeSettings<P> {
    /// The branch that the merge action is responsible for targetting.
    branch: String,
    /// Branch name for merging.
    merge_branch_as: Option<String>,
    /// Branches that should always contain the target branch.
    into_branches: Vec<IntoBranch>,
    /// The merge policy.
    policy: P,
    /// Whether the action should create informational comments or not.
    ///
    /// Errors always create comments.
    quiet: bool,
    /// The number of commits to list in the merge commit message.
    ///
    /// A limit of `None` lists all commits in the message.
    log_limit: Option<usize>,
}

impl<P> MergeSettings<P> {
    /// Create a new merge action.
    pub fn new<B>(branch: B, policy: P) -> Self
        where B: ToString,
    {
        Self {
            branch: branch.to_string(),
            merge_branch_as: None,
            into_branches: Vec::new(),
            policy: policy,
            quiet: false,
            log_limit: None,
        }
    }

    /// Reduce the number of comments made by the merge action.
    ///
    /// The comments created by this action can be a bit much. This reduces the comments to those
    /// which are errors or are important.
    pub fn quiet(&mut self) -> &mut Self {
        self.quiet = true;
        self
    }

    /// Whether to merge silently or not.
    pub fn is_quiet(&self) -> bool {
        self.quiet
    }

    /// Limit the number of log entries in merge commit messages.
    ///
    /// Anything beyond this limit (if present) is elided.
    pub fn log_limit(&mut self, log_limit: Option<usize>) -> &mut Self {
        self.log_limit = log_limit;
        self
    }

    /// Set the name of the branch to use in merge commits.
    pub fn merge_branch_as<N>(&mut self, name: Option<N>) -> &mut Self
        where N: ToString,
    {
        self.merge_branch_as = name.map(|name| name.to_string());
        self
    }

    /// Add a set of branches which always have this branch merged in.
    pub fn add_into_branches<I>(&mut self, branches: I) -> &mut Self
        where I: IntoIterator<Item = IntoBranch>,
    {
        self.into_branches.extend(branches.into_iter());
        self
    }

    /// The name of the branch being merged into.
    pub fn branch(&self) -> &str {
        &self.branch
    }

    /// Branches which should be merged into when a topic is merged into this branch.
    pub fn into_branches(&self) -> &[IntoBranch] {
        &self.into_branches
    }

    fn add_topo_links(&self, sorter: &mut TopologicalSort<String>) {
        self.into_branches
            .iter()
            .foreach(|into_branch| {
                debug!("adding dep from {} -> {}",
                       self.branch(),
                       into_branch.name());
                sorter.add_dependency(self.branch(), into_branch.name());
                into_branch.add_topo_links(sorter);
            })
    }
}

/// Information required when performing a merge.
pub struct MergeInformation<'a> {
    /// The name of the topic that is being merged.
    pub topic_name: &'a str,
    /// Who is performing the merge.
    pub who: &'a Identity,
    /// When the merge was requested.
    pub when: DateTime<Utc>,
}

/// A structure which performs a merge of a merge request.
pub struct Merger<'a> {
    /// The git context to use for merging.
    ctx: &'a GitContext,
    /// The project that is being merged into.
    project: &'a HostedProject,
    /// The merge request which is being merged.
    mr: &'a MergeRequest,
    /// The trailers discovered from the merge request.
    trailers: Vec<(Trailer, Option<User>)>,
}

/// Alias used for nested results involved in merging a merge request.
type StepResult<T> = Result<Either<T, MergeActionResult>>;

impl<'a> Merger<'a> {
    /// Create a new merger object.
    pub fn new(ctx: &'a GitContext, project: &'a HostedProject, mr: &'a MergeRequest)
               -> Result<Self> {
        Ok(Merger {
            ctx: ctx,
            project: project,
            mr: mr,
            trailers: ParseTrailers::find(project, mr)?,
        })
    }

    /// Merge a merge request.
    pub fn merge_mr<'b, P>(self, settings: &MergeSettings<P>, info: MergeInformation<'b>)
                           -> Result<MergeActionResult>
        where P: MergePolicy,
    {
        if let Right(res) = self.prep_mr()? {
            return Ok(res);
        }

        let commit_id = match self.create_merge(settings, &info, &self.mr.commit.id)? {
            Left(commit_id) => commit_id,
            Right(res) => {
                return Ok(res);
            },
        };

        let mut sorter = TopologicalSort::new();
        settings.add_topo_links(&mut sorter);
        let refs = iter::once((settings.branch.clone(), commit_id, settings.into_branches()));
        let push_refs = self.perform_update_merges(sorter, refs, &info)?;
        self.push_refs(settings.quiet, push_refs)
    }

    /// Prepare to merge a merge request.
    ///
    /// This ensures the merge request is available locally and that it is not a work-in-progress.
    pub fn prep_mr(&self) -> StepResult<()> {
        info!(target: "ghostflow/merge",
              "preparing to merge {}",
              self.mr.url);

        if self.mr.work_in_progress {
            self.send_mr_comment("This merge request is marked as a Work in Progress and may not \
                                  be merged. Please remove the Work in Progress state first.");
            return Ok(Right(MergeActionResult::Failed));
        }

        // Fetch the commit into the merge's git context.
        self.project.service.fetch_mr(self.ctx, self.mr)?;

        Ok(Left(()))
    }

    /// Perform merges from updated branches into their "into" branches.
    ///
    /// This takes the sorted set of branches which have been merged into by a merge request and
    /// updates all of their "into" branches so that they are always synchronized.
    ///
    /// Returns a vector of commits which need to be pushed to the given branches on the remote.
    pub fn perform_update_merges<'b, I>(&self, mut sorter: TopologicalSort<String>, refs: I,
                                        info: &MergeInformation<'b>)
                                        -> Result<Vec<(CommitId, String)>>
        where I: IntoIterator<Item = (String, CommitId, &'b [IntoBranch])>,
    {
        // A map of branch -> Vec<branch> for knowing which branches to merge into this branch.
        let mut from_branches: HashMap<_, Vec<_>> = HashMap::new();
        // A map of branch -> (commit, [IntoBranch]) to know where branches should go.
        let mut push_refs = refs.into_iter()
            .map(|(branch, commit, into_branches)| (branch, (commit, into_branches)))
            .collect::<HashMap<_, _>>();

        // Look at a free branch.
        while let Some(target_branch) = sorter.pop() {
            let (target_commit, target_intos) = if let Some(target_info) = push_refs.get(&target_branch) {
                // Merge into the branch as it has been updated so far.
                target_info.clone()
            } else {
                // There is no record of what commit is supposed to be used for this ref, but the
                // end of this loop should always be keeping this up-to-date.
                bail!("Failed to track references to push: {}", target_branch);
            };

            // Find out if we have branches to merge into the branch.
            if let Some(source_branches) = from_branches.remove(target_branch.as_str()) {
                // Perform the merges.
                let new_commit = source_branches.into_iter()
                    .fold(Ok(target_commit), |target_commit, source_branch| {
                        let target_commit = target_commit?;
                        let &(ref source_commit, _) = push_refs.get(&source_branch)
                            .ok_or_else(|| ErrorKind::Msg("unimplemented!()".to_string()))?;

                        // Prepare for the merge of the branch.
                        let workarea = self.ctx.prepare(&target_commit)?;
                        let merge_result = workarea.setup_update_merge(&target_commit, source_commit)?;
                        let mut merge_command = if let MergeResult::Ready(command) = merge_result {
                            command
                        } else {
                            // Unmergeable; something went really wrong since we're not actually
                            // performing a merge, but just creating a new commit with the same
                            // tree and multiple parents.
                            bail!("Failed to create a merge using the `-s ours` strategy: {:?}",
                                  merge_result);
                        };

                        // Add authorship information to the commit message. Committer information is provided by
                        // the default git environment.
                        merge_command.author(info.who)
                            .author_date(&info.when);

                        // Skip the "into" part if merging into master.
                        let into_branch = if target_branch == "master" {
                            String::new()
                        } else {
                            format!(" into {}", target_branch)
                        };

                        let commit_message = format!("Merge branch '{}'{}",
                                                     source_branch,
                                                     into_branch);

                        info!(target: "ghostflow/merge",
                              "updating {} into {}",
                              source_branch,
                              target_branch);

                        // Fold using the newly created merge commit.
                        merge_command.commit(commit_message)
                            .chain_err(|| "failed to commit the merge")
                    })?;

                let push_ref = push_refs.entry(target_branch.clone())
                    .or_insert_with(|| (CommitId::new(String::new()), target_intos));
                push_ref.0 = new_commit;
            }

            // Queue up the "into" branches for this branch.
            target_intos.iter()
                .foreach(|target_into| {
                    from_branches.entry(target_into.name())
                        .or_insert_with(Vec::new)
                        .push(target_branch.clone());
                    push_refs.entry(target_into.name().to_string())
                        .or_insert((CommitId::new(target_into.name()), target_into.chain_branches()));
                });
        }

        // If there are still nodes remaining, we have detected a cycle.
        if !sorter.is_empty() {
            bail!(ErrorKind::CircularIntoBranches);
        }

        // If we still have information about branches here, we haven't merged everything required.
        // This should never happen (we'd have had circular dependencies).
        if !from_branches.is_empty() {
            bail!("Bookkeeping failed: remaining branches: {:?}", from_branches);
        }

        Ok(push_refs.into_iter()
            .map(|(branch, (commit, _))| (commit, branch))
            .collect())
    }

    /// Create a merge commit for the merge request into the branch.
    pub fn create_merge<'b, P>(&self, settings: &MergeSettings<P>, info: &MergeInformation<'b>,
                               commit_id: &CommitId)
                               -> StepResult<CommitId>
        where P: MergePolicy,
    {
        info!(target: "ghostflow/merge",
              "preparing to merge {} into {}",
              self.mr.url,
              settings.branch);

        let branch_id = CommitId::new(&settings.branch);

        // Determine if the topic is mergeable at all.
        let merge_status = self.ctx.mergeable(&branch_id, commit_id)?;
        let bases = if let MergeStatus::Mergeable(bases) = merge_status {
            bases
        } else {
            self.send_mr_comment(&unmerged_status_message(&settings.branch, &merge_status));
            return Ok(Right(MergeActionResult::Failed));
        };

        // Prepare a work area to perform the actual merge.
        let workarea = self.ctx.prepare(&branch_id)?;
        let merge_result = workarea.setup_merge(&bases, &branch_id, commit_id)?;
        let mut merge_command = match merge_result {
            MergeResult::Conflict(conflicts) => {
                let mut conflict_paths = conflicts.iter()
                    .map(|conflict| conflict.path().to_string_lossy())
                    .dedup();
                self.send_mr_comment(&format!("This merge request contains conflicts with `{}` in \
                                               the following paths:\n\n  - `{}`",
                                              settings.branch,
                                              conflict_paths.join("`\n  - `")));
                return Ok(Right(MergeActionResult::Failed));
            },
            MergeResult::Ready(command) => command,
            MergeResult::_Phantom(_) => unreachable!(),
        };

        // Add authorship information to the commit message. Committer information is provided by
        // the default git environment.
        merge_command.author(info.who)
            .author_date(&info.when);

        let mut mr_policy = settings.policy.for_mr(self.mr);

        self.trailers.iter()
            // Filter trailers through the policy.
            .foreach(|&(ref trailer, ref user_opt)| {
                mr_policy.process_trailer(trailer, user_opt.as_ref())
            });

        let trailers = match mr_policy.result() {
            Ok(trailers) => trailers.into_iter().unique(),
            Err(reasons) => {
                let reason = reasons.into_iter()
                    .join("  \n  - ");
                self.send_mr_comment(&format!("This merge request may not be merged into `{}` \
                                               because:\n\n  \
                                               - {}",
                                              settings.branch,
                                              reason));
                return Ok(Right(MergeActionResult::Failed));
            },
        };

        let commit_message =
            self.build_commit_message(settings, info.topic_name, commit_id, trailers)?;

        info!(target: "ghostflow/merge",
              "merging {} into {}",
              self.mr.url,
              settings.branch);

        Ok(Left(merge_command.commit(commit_message)?))
    }

    /// Push the results of a merge action to the remote repository.
    pub fn push_refs<R, B>(&self, quiet: bool, refs: R) -> Result<MergeActionResult>
        where R: IntoIterator<Item = (CommitId, B)>,
              B: AsRef<str>,
    {
        let push = self.ctx
            .git()
            .arg("push")
            .arg("--atomic")
            .arg("--porcelain")
            .arg("origin")
            .args(&refs.into_iter()
                .map(|(commit_id, branch)| {
                    format!("{}:{}", commit_id, branch.as_ref())
                })
                .collect::<Vec<_>>())
            .output()
            .chain_err(|| "failed to construct push command")?;
        if !push.status.success() {
            warn!(target: "ghostflow/merge",
                  "failed to push the merge of {} to the remote server: {}",
                  self.mr.url,
                  String::from_utf8_lossy(&push.stderr));

            self.send_info_mr_comment(quiet,
                                      "Automatic merge succeeded, but pushing to the remote \
                                       failed.");

            return Ok(MergeActionResult::PushFailed);
        }

        self.send_info_mr_comment(quiet, "Topic successfully merged and pushed.");

        Ok(MergeActionResult::Success)
    }

    /// Build a commit message for a merge request.
    fn build_commit_message<I, P>(&self, settings: &MergeSettings<P>, topic_name: &str,
                                  commit_id: &CommitId, trailers: I)
                                  -> Result<String>
        where I: IntoIterator<Item = Trailer>,
              P: MergePolicy,
    {
        let mut topic_summary = self.mr
            .description
            // Break it into lines.
            .lines()
            // Find the message block.
            .skip_while(|&line| line != "```message")
            // Skip the entry line.
            .skip(1)
            // Take it until the end of the block.
            .take_while(|&line| line != "```")
            // Add newlines.
            .map(|line| format!("{}\n", line))
            // Join the lines together.
            .join("");
        if !topic_summary.is_empty() {
            // Append a separator if we have a topic description.
            topic_summary.push('\n');
        }

        let mut log_command = self.ctx.git();
        log_command
            .arg("log")
            .arg("--date-order")
            .arg("--format=%h %s")
            .arg("--abbrev-commit");

        if let Some(limit) = settings.log_limit {
            // Get up to one more than the maximum. This is done so that we can detect that there
            // are more so that an elision indicator may be added.
            log_command.arg(format!("--max-count={}", limit + 1));
        }

        let log = log_command.arg(format!("{}..{}", settings.branch, commit_id))
            .output()
            .chain_err(|| "failed to construct log command")?;
        if !log.status.success() {
            bail!(ErrorKind::Git(format!("failed to get a log of commits on the topic: {}",
                                         String::from_utf8_lossy(&log.stderr))));
        }
        let log_output = String::from_utf8_lossy(&log.stdout);
        let mut log_lines = log_output.lines().collect::<Vec<_>>();
        // Elide the log if there are too many entries.
        if let Some(limit) = settings.log_limit {
            if limit == 0 {
                log_lines.clear()
            } else if log_lines.len() > limit {
                log_lines[limit] = "...";
            }
        }
        let mut log_summary = log_lines.into_iter()
            .map(|line| format!("{}\n", line))
            .join("");
        if !log_summary.is_empty() {
            // Append a separator if we have a log summary.
            log_summary.push('\n');
        }

        let trailer_summary =
            trailers.into_iter()
                .chain(iter::once(Trailer::new("Merge-request", &self.mr.reference)))
                .map(|trailer| format!("{}\n", trailer))
                .join("");

        let into_branch_raw = settings.merge_branch_as
            .as_ref()
            .unwrap_or(&settings.branch);
        let into_branch = if into_branch_raw == "master" {
            String::new()
        } else {
            format!(" into {}", into_branch_raw)
        };

        Ok(format!("Merge topic '{}'{}\n\
                    \n\
                    {}{}{}",
                   topic_name,
                   into_branch,
                   topic_summary,
                   log_summary,
                   trailer_summary))
    }

    /// Send a comment to a merge request.
    fn send_mr_comment(&self, content: &str) {
        if let Err(err) = self.project.service.post_mr_comment(self.mr, content) {
            error!(target: "ghostflow/merge",
                   "failed to post a comment to merge request: {}, {}: {:?}",
                   self.project.name,
                   self.mr.id,
                   err);
        }
    }

    /// Send an informational comment to a merge request.
    fn send_info_mr_comment(&self, quiet: bool, content: &str) {
        if !quiet {
            self.send_mr_comment(content)
        }
    }
}

/// The status message for a merge status.
fn unmerged_status_message(branch: &str, reason: &MergeStatus) -> String {
    let reason_message = match *reason {
        MergeStatus::NoCommonHistory => "there is no common history",
        MergeStatus::AlreadyMerged => "it has already been merged",
        MergeStatus::Mergeable(_) => "it is\u{2026}mergeable? Sorry, something went wrong",
    };

    format!("This merge request may not be merged into `{}` because {}.",
            branch,
            reason_message)
}
