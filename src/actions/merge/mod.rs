// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The `merge` action.
//!
//! This action performs the merge of a merge request topic into the target branch. It gathers
//! information from the merge request such as reviewers, testers, acceptance or rejection
//! messages, and more to determine the resulting merge commit message.
//!
//! There are multiple implementations of it which allow handling of more complicated merge
//! strategies.

use crates::git_workarea::{self, CommitId};

use host;
use utils::mr;

error_chain! {
    links {
        GitWorkarea(git_workarea::Error, git_workarea::ErrorKind)
            #[doc = "Errors from the git-workarea crate."];
        Host(host::Error, host::ErrorKind)
            #[doc = "Errors from the service host."];
        MrUtils(mr::Error, mr::ErrorKind)
            #[doc = "Errors from the merge request utilities."];
    }

    errors {
        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }

        /// A merge request has been requested to be merged into a branch twice.
        DuplicateTargetBranch(branch: String) {
            display("duplicate target branch name: {}", branch)
        }

        /// A commit requested to be merged is not part of the merge request.
        UnrelatedCommit(commit: CommitId) {
            display("commit unrelated to the merge request: {}", commit.as_str())
        }

        /// Configurations for "into branches" are a cycle.
        CircularIntoBranches {
            display("into branches form a cycle")
        }
    }
}

mod policy;
pub use self::policy::MergePolicy;
pub use self::policy::MergePolicyFilter;

mod settings;
pub use self::settings::IntoBranch;
pub use self::settings::MergeActionResult;
pub use self::settings::MergeSettings;

mod prelude_impl;
mod trailers;

mod simple;
pub use self::simple::Merge;

mod backport;
pub use self::backport::MergeBackport;
pub use self::backport::MergeMany;
