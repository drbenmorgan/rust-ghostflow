// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! This module contains the actions within ghostflow.
//!
//! The actions are intended to be triggered by explicit actions such as a comment on a merge
//! request or commit, a push to a branch, or a nightly trigger. Each action implements some part
//! of the workflow which may not be used by all projects. For example, a project might use the
//! `check` action to run commit hooks over merge requests, but skip the `merge` action for
//! managing bringing topics into the main branches.

pub mod check;
pub mod clone;
pub mod data;
pub mod follow;
pub mod merge;
pub mod reformat;
pub mod stage;
pub mod test;

#[cfg(test)]
mod tests;
